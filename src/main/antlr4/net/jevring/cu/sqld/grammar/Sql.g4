// @author Markus Jevring <markus@jevring.net>
grammar Sql;

// ATOMS

// Skip whitespace.
// AHA! This needs to go FIRST! Otherwise we get a fuck-load of complaints and shit never works!
Whitespace          : [ \t\r\n]+ -> skip;
Boolean             : 'true'
					| 'false'
					;					
Null                : 'null';					
value               : Null              #nullValue
					| Double            #doubleValue
					| Integer           #integerValue
					| QuotedValue       #stringValue
					;
// atoms, i.e. the leaves of the tree, need to start with an upper-case letter
// todo: we'll need to unquote this as well, when handling it. Create a test for this
Identifier          : Character (Character | Digit)* 
					| '`' Identifier '`'
					;
// This basically HAS to be a lexer rule. If we make it into a parser rule, 
// and extract the content inside the quotation marks, we end up losing all
// the whitespace characters, and every whitespace-separated "group" becomes
// a separate child
QuotedValue         : '\'' (~'\'' | '\'\'')* '\'';
Double              : '-'? Digit+ '.' Digit+ ('e' '-'? Digit+)?;
Integer             : '-'? Digit+;
Digit               : [0-9];
Character           : [a-zA-Z_$%];
NewLine             : '\r'
					| '\n'
					| '\r\n'
					;
								
// COMMON
where               : 'where' condition;
// todo: see how a lot of nesting works. like (((a = 1) and ((b = 2) or (c = 3))) etc.
// we *might* have to have some kind of depth counte
condition           : operator_condition
					| left=condition connective right=condition
					| '(' nested=condition ')'
//                    | column_reference nullness                    
                    // todo: do we really want these like this, rather than simple operators?
                    //| column_reference 'not'? 'in' '(' container ')'
                    //| column_reference 'not'? 'like' Value // todo: we'd like to force the quotes here, but that can't be done if Value is matched/defined first
                    | column_reference 'between' start=value 'and' finish=value
					;
//nullness            : 'is' 'not'? 'null';
value_list          : value (',' value)*;					
container           : value_list
					| select;
					
operator_condition  : left=operand operator right=operand;
operator            : '='
                    | '!='
                    | '<>'
                    | '<'
                    | '<='
                    | '>'
                    | '>='
                    | 'like'
                    | 'not like'
                    | 'in'
                    | 'not in'
                    ;
connective          : 'and'
					| 'or'
					;		

operand             : Boolean
					| column_reference
					| value
					; // todo: this will obviously need to be expanded to include expressions as well
column_reference    : column_name
					| table_name '.' column_name
					| schema_name '.' table_name '.' column_name
					;				

// todo: get comments to work
comment             : '#' ~(NewLine)* NewLine;

// DELETE
delete              : 'delete' 'from' table where?;								
					
// UPDATE					
// todo: support joining on things for the update condition
update              : 'update' table 'set' key_value_pairs where?;
key_value_pairs     : key_value_pair (',' key_value_pair)*;
key_value_pair      : key=column_reference '=' value;					

// INSERT
insert              : 'insert' 'into' table columns? what_to_insert;
columns             : '(' Identifier (',' Identifier)* ')';

what_to_insert      : 'values' value_tuples
					| select
					; 
value_tuples        : value_tuple (',' value_tuple)*;
value_tuple         : '(' value_list ')';				

// SELECT
select              : 'select' selection_columns 'from' tables joins where? limit? order_by? group_by? 
					| 'select' lastInsertId='last_insert_id()'                                                      
					;
selection_columns   : selection_column (',' selection_column)*;
// todo: investigate using labels and smaller visitors for more things.
selection_column    : star='*'                                                      #selectStar
					| column_name                                                   #selectColumn
					| column_name 'as' column_alias                                 #selectColumnAsAlias
					| table_name '.' star='*'                                       #selectTableStar
					| table_name '.' column_name                                    #selectTableAndColumn
					| table_name '.' column_name 'as' column_alias                  #selectTableAndColumnAsAlias
					| schema_name '.' table_name '.' star='*'                       #selectSchemaAndTableStar
					| schema_name '.' table_name '.' column_name                    #selectSchemaAndTableAndColumn
					| schema_name '.' table_name '.' column_name 'as' column_alias  #selectSchemaAndTableAndColumnAsAlias
					;
column_name         : Identifier;
column_alias        : Identifier;
table_name          : Identifier;
schema_name         : Identifier;

tables              : table (',' table)*;

table               : table_name
					| table_name table_alias
					| schema_name '.' table_name
					| schema_name '.' table_name table_alias
					;

table_alias         : Identifier;					

joins               : join*;

join                : join_type? 'join' table join_condition?;
join_type           : 'full' ('outer')?
					| 'left' ('outer')?
					| 'right' ('outer')?
					| 'inner'
					| 'cross'
					;

join_condition      : 'on' condition  #joinOn
					| 'using' columns #joinUsing
					;
limit               : 'limit' Digit+;
order_by            : 'order' 'by' column_and_direction (',' column_and_direction)*;
group_by            : 'group' 'by' column_name;
column_and_direction: column_reference sort_order;
sort_order          : ('asc'|'desc');
// CREATE
// https://dev.mysql.com/doc/refman/5.1/en/create-table.html
create              : 'create' 'table' full_table '(' create_table_entries ')' ('engine' engine=Identifier)?  #createTable
					| 'create' 'schema' schema_name                             #createSchema
					| 'create' 'database' schema_name                           #createDatabase
					;
full_table          : table_name
					| schema_name '.' table_name
					;
create_table_entries: create_table_entry (',' create_table_entry)*;
create_table_entry  : column_name column_type column_constraint*                                     
					| table_constraint                                                                                  
					;
single_size         : '(' size=Integer ')';
double_size         : '(' size=Integer ',' decimal=Integer ')';
// also supports types from other vendors
 // todo: support more type equivalences
// https://dev.mysql.com/doc/refman/5.6/en/other-vendor-data-types.html
column_type         : 'varchar' single_size                     #varchar
					| 'character' 'varying' single_size         #varchar
					| 'char' single_size                        #char
					| 'tinytext'                                #tinyText
					| 'text'                                    #text
					| 'clob'                                    #clob
					| 'mediumtext'                              #mediumText
					| 'long' 'varchar'                          #mediumText
					| 'long'                                    #mediumText
					| 'longtext'                                #longtext
					| 'mediumblob'                              #mediumBlob
					| 'long' 'varbinary'                        #mediumBlob
					| 'blob'                                    #blob
					| 'longblob'                                #longblob
					| 'bit'                                     #boolean
					| 'boolean'                                 #boolean
					| 'bool'                                    #boolean
					| 'set' value_tuple                         #set
					| 'enum' value_tuple                        #enum
					| 'date' ('(' ')')?                         #date       
					| 'datetime' ('(' ')')?                     #datetime
					| 'timestamp' ('(' ')')?                    #timestamp
					| 'time' ('(' ')')?                         #time
					| 'year' ('(' ')')?                         #year
					| 'tinyint' single_size?                    #tinyInt 
					| 'int1' single_size?                       #tinyInt 
					| 'smallint' single_size?                   #smallInt       
					| 'int2' single_size?                       #smallInt       
					| 'mediumint' single_size?                  #mediumInt
					| 'middleint' single_size?                  #mediumInt
					| 'int3' single_size?                       #mediumInt
					| 'int' single_size?                        #int
					| 'int4' single_size?                       #int
					| 'bigint' single_size?                     #bigInt
					| 'int8' single_size?                       #bigInt
					| 'float' double_size?                      #float
					| 'float4' double_size?                     #float
					| 'double' double_size?                     #double
					| 'float8' double_size?                     #double
					| 'decimal' double_size?                    #decimal
					| 'numeric' double_size?                    #decimal
					| 'fixed' double_size?                      #decimal
					;
column_constraint	: 'not' 'null'                              #notNull 
                    | 'unique'                                  #unique
                    | 'default' value                           #default
                    | 'primary' 'key'                           #primaryKey
					| 'foreign' 'key' 'references' 
						primary_key_reference                   #foreignKey
					| check_constraint                          #check
					| 'auto_increment' step=Integer?            #autoIncrement
               		| 'unsigned'                                #unsigned
					;
primary_key_reference : table_name columns; 					
constraint_name     : 'constraint' name=Identifier;					
table_constraint    : constraint_name? check_constraint         #tableCheckConstraint
					| constraint_name? 'unique' columns         #tableUnique
					| constraint_name? 'primary' 'key' columns  #tablePrimaryKey
					| constraint_name? 'foreign' 'key' columns  
						'references' primary_key_reference      #tableForeignKey
					;					
check_constraint    : 'check' '(' condition ')';					
// DROP
drop                : 'drop' 'table' full_table                 #dropTable
					| 'drop' 'schema' schema_name               #dropSchema
					| 'drop' 'database' schema_name             #dropDatabase
					;
// ALTER    
alter               : 'alter' 'table' full_table alter_operations;
alter_operations    : alter_operation (',' alter_operation)*; // todo: this means that we need to build up a list. Change it to support this.   
 
alter_operation     : 'add' 'column' create_table_entry         #alterAddColumn // this isn't strictly correct, but it's easier this way,a s we can re-use the code for the create_table_entry
					| 'add' create_table_entry                  #alterAddTableConstraint
 					| 'drop' 'column' column_name               #alterDropColumn
 					| 'drop' constraint_name                    #alterDropNamedConstraint
                    | 'drop' 'check' name=Identifier            #alterDropCheck        
                    | 'drop' 'foreign' 'key' name=Identifier    #alterDropForeignKey        
                    | 'drop' 'primary' 'key'                    #alterDropPrimaryKey        
                    | 'drop' 'unique' name=Identifier           #alterDropUnique        
 					| 'modify' 'column' create_table_entry      #alterDropColumn
 					| column_name 'set' 'default' value         #alterSetDefault
 					| column_name 'drop' 'default'              #alterDropDefault
 					;			

// START
// This is the starting point. 
// SqlParser parser = new SqlParser(...);
// parser.statement();
statement           : select
					| insert
					| update
					| delete
					| create
					| drop
					| alter
					; 


