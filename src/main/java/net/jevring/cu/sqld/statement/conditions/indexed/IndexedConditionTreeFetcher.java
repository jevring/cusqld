/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.conditions.indexed;

import net.jevring.cu.sqld.statement.conditions.ConditionTree;
import net.jevring.cu.sqld.storage.RowReference;
import net.jevring.cu.sqld.structure.Table;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Fetches rows based on the conditions and index keys present in the condition tree for the specified table.
 * For joining two tables, see {@link IndexedConditionTreeMatcher}.
 *
 * @author markus@jevring.net
 */
public class IndexedConditionTreeFetcher {
	private final IndexedConditionTree root;
	private final Table table;

	public IndexedConditionTreeFetcher(IndexedConditionTree root, Table table) {
		this.root = root;
		this.table = table;
	}

	public Set<RowReference> fetchRows() throws SQLException {
		return fetchRows(root);
	}

	private Set<RowReference> fetchRows(IndexedConditionTree conditionTree) throws SQLException {
		if (conditionTree == IndexedConditionTree.TRUE) {
			// simple fast way out of a leaf
			return table.storage().getRows();
		} else if (conditionTree.getCondition().isPresent()) {
			IndexedCondition condition = conditionTree.getCondition().get();
			if (condition instanceof IndexedColumnCondition) {
				// todo: this isn't a good solution. 
				// we need to filter partial results, rather than resort to a complete table scan here


				// todo: we must carry a set of matches rows with us from the parent (child, actually).
				// if we have rows, then apply the current conditions as a filter.
				// if there are no rows, fetch them from table storage.
				// we also need to track "depth" in the condition tree.
				// (I think) only things with depth 0 (or 1?) should be allowed to fetch things from the table storage.

				IndexedColumnCondition indexedColumnCondition = (IndexedColumnCondition) condition;
				Table table = indexedColumnCondition.getTable();
				return table.storage().getRows(conditionTree);
			} else if (condition instanceof IndexedJoinCondition) {
				throw new AssertionError(
						"We should never encounter a join condition when fetching rows. It should have been pruned.");
			} else if (condition instanceof IndexKeyCondition) {
				IndexKeyCondition indexKeyCondition = (IndexKeyCondition) condition;
				return indexKeyCondition.getTable().storage().getRows(indexKeyCondition.getIndex(), indexKeyCondition.getKey());
			} else if (condition instanceof IndexChoiceCondition) {
				IndexChoiceCondition indexChoiceCondition = (IndexChoiceCondition) condition;
				Set<RowReference> bestList = null;
				for (IndexKeyCondition indexKeyCondition : indexChoiceCondition.getChoices()) {
					Set<RowReference> rows = indexKeyCondition.getTable()
					                                          .storage()
					                                          .getRows(indexKeyCondition.getIndex(), indexKeyCondition.getKey());
					if (bestList == null || rows.size() < bestList.size()) {
						bestList = rows;
					}
				}
				return bestList;
			} else {
				// todo: when we enter here with IndexedConditionTree.TRUE, we need to fetch all rows from all tables, which needs to be indicated.
				// since we're basically always checking before calling in here, we should do the same above.
				// but that's actually quite ugly. We should be able to handle ALL tree business in here!
				throw new IllegalArgumentException("Condition type not yet supported: " + condition);
			}
		} else {
			Set<RowReference> leftRows = fetchRows(conditionTree.getLeft().get());
			Set<RowReference> rightRows = fetchRows(conditionTree.getRight().get());
			if (conditionTree.getConnective().get() == ConditionTree.Connective.And) {
				return intersection(leftRows, rightRows);
			} else {
				return union(leftRows, rightRows);
			}
		}
	}

	private Set<RowReference> union(Set<RowReference> leftRows, Set<RowReference> rightRows) {
		Set<RowReference> union = new HashSet<>();
		union.addAll(leftRows);
		union.addAll(rightRows);
		return union;
	}

	private Set<RowReference> intersection(Set<RowReference> leftRows, Set<RowReference> rightRows) {
		Set<RowReference> shortest;
		Set<RowReference> longest;

		// we do this by taking the SMALLEST of the two, and saying "keep every row of mine that also exists in the other".
		// we take the smallest, as we're O(n) of the thing we're iterating and O(1) for the lookups.
		// thus, if we can make n as small as possible, we've gained something.
		// the prime example is when one is the whole table and the other is just a single row...

		if (leftRows.size() < rightRows.size()) {
			shortest = leftRows;
			longest = rightRows;
		} else {
			shortest = rightRows;
			longest = leftRows;
		}
		return shortest.parallelStream().filter(longest::contains).collect(Collectors.toSet());
	}
}
