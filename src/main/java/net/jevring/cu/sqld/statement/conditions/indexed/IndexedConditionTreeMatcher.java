/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.conditions.indexed;

import net.jevring.cu.sqld.statement.conditions.ConditionTree;
import net.jevring.cu.sqld.storage.Row;

import java.sql.SQLException;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Walks a condition tree to determine if a row matches the conditions.
 *
 * @author markus@jevring.net
 */
public class IndexedConditionTreeMatcher {
	private final IndexedConditionTree root;

	public IndexedConditionTreeMatcher(IndexedConditionTree root) {
		this.root = root;
	}

	public boolean matches(Row row) throws SQLException {
		return matches(row, root);
	}

	private boolean matches(Row row, IndexedConditionTree conditionTree) throws SQLException {
		if (conditionTree == IndexedConditionTree.TRUE) {
			// simple fast way out of a leaf
			return true;
		} else {
			if (conditionTree.getCondition().isPresent()) {
				IndexedCondition condition = conditionTree.getCondition().get();
				if (condition instanceof IndexedColumnCondition) {
					IndexedColumnCondition cc = (IndexedColumnCondition) condition;
					int column = cc.getColumn();
					// todo: we're going to have to ensure that this works for things that are not strings!

					// todo: things like < still have to work for strings, of course, so we'll have to resolve this in different ways

					switch (cc.getOperator()) {
						case Equals:
							return row.getValueAt(column).equals(cc.getValue());
						case NotEquals:
							return !row.getValueAt(column).equals(cc.getValue());
						case LessThan:
							return row.getAsDoubleAt(column) < asDouble(cc.getValue());
						case LessThanOrEquals:
							return row.getAsDoubleAt(column) <= asDouble(cc.getValue());
						case GreaterThan:
							return row.getAsDoubleAt(column) > asDouble(cc.getValue());
						case GreaterThanOrEquals:
							return row.getAsDoubleAt(column) >= asDouble(cc.getValue());
						case Like:
							// todo: compile the pattern when creating the IndexedConditionTree
							String value = row.getStringAt(column);
							String conditionalValueAsString = (String) cc.getValue();
							Pattern p = Pattern.compile(conditionalValueAsString.replace("%", ".*"));
							return p.matcher(value).matches();
						case NotLike:
							// todo: just flip Like, but do it later, so we don't have to mess around with variable duplication.
							// also, the current implementation isn't great.
							return false;
						default:
							throw new IllegalArgumentException("not yet supported: " + cc.getOperator());
					}
				} else if (condition instanceof IndexedJoinCondition) {
					IndexedJoinCondition jc = (IndexedJoinCondition) condition;

					Object left = row.getValueAt(jc.getLeft());
					Object right = row.getValueAt(jc.getRight());
					// todo: null doesn't equal null in sql, so this will have to be modified
					// todo: join conditionTree must also support operators like != and 'like' etc.

					// todo: extract the values and then perform the operation the same for all conditionTree

					return Objects.equals(left, right);
				} else if (condition instanceof IndexKeyCondition) {
					IndexKeyCondition indexKeyCondition = (IndexKeyCondition) condition;
					// todo: handle this using the indexes
					return matches(row, indexKeyCondition.getCorrespondingCondition());
				} else if (condition instanceof IndexChoiceCondition) {
					IndexChoiceCondition indexChoiceCondition = (IndexChoiceCondition) condition;
					// todo: handle this using the indexes	
					for (IndexKeyCondition indexKeyCondition : indexChoiceCondition.getChoices()) {
						if (matches(row, indexKeyCondition.getCorrespondingCondition())) {
							return true;
						}
					}
					return false;
				} else {
					throw new IllegalArgumentException("not yet supported: " + condition);
				}
			} else {
				boolean leftOk = matches(row, conditionTree.getLeft().get());
				// the reason we don't resolve rightOk here is that it's not necessary to resolve
				// it until the left hand of the expression has been resolved.
				if (conditionTree.getConnective().get() == ConditionTree.Connective.And) {
					return leftOk && matches(row, conditionTree.getRight().get());
				} else {
					return leftOk || matches(row, conditionTree.getRight().get());
				}
			}
		}
	}

	private double asDouble(Object value) throws SQLException {
		if (value instanceof Number) {
			return ((Number) value).doubleValue();
		} else {
			throw new SQLException(String.format("Value (%s) is not a number", value));
		}
	}
}
