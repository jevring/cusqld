/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.parser.antlr4;

import net.jevring.cu.sqld.grammar.SqlBaseVisitor;
import net.jevring.cu.sqld.grammar.SqlParser;
import net.jevring.cu.sqld.statement.conditions.ConditionTree;
import net.jevring.cu.sqld.statement.references.ColumnReference;
import net.jevring.cu.sqld.structure.CaseInsensitiveId;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.structure.TableId;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.column.constraints.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Visits columns constraints.
 *
 * @author markus@jevring.net
 */
class ColumnConstraintVisitor extends SqlBaseVisitor<ColumnConstraint> {
	private final SqlBaseVisitor<Object> valueVisitor = new ValueVisitor();
	private final ColumnId id;

	public ColumnConstraintVisitor(ColumnId id) {
		this.id = id;
	}

	@Override
	public AutoIncrement visitAutoIncrement(SqlParser.AutoIncrementContext ctx) {
		return new AutoIncrement(Integer.parseInt(ctx.step.getText()));
	}

	@Override
	public NotNull visitNotNull(SqlParser.NotNullContext ctx) {
		return new NotNull();
	}

	@Override
	public Unique visitUnique(SqlParser.UniqueContext ctx) {
		return new Unique(new CaseInsensitiveId("UNIQUE(" + id + ")"), Collections.singletonList(id));
	}

	@Override
	public PrimaryKey visitPrimaryKey(SqlParser.PrimaryKeyContext ctx) {
		return new PrimaryKey(new CaseInsensitiveId("PRIMARY_KEY(" + id + ")"), Collections.singletonList(id));
	}

	@Override
	public ForeignKey visitForeignKey(SqlParser.ForeignKeyContext ctx) {
		List<ColumnReference> primaryKeyReference = new ArrayList<>();
		TableId tableId = new TableId(ctx.primary_key_reference().table_name().getText());
		ctx.primary_key_reference()
		   .columns()
		   .Identifier()
		   .stream()
		   .map(terminalNode -> new ColumnId(terminalNode.getText()))
		   .forEach(columnId -> primaryKeyReference.add(new ColumnReference(Optional.<SchemaId>empty(),
		                                                                    Optional.of(tableId),
		                                                                    columnId,
		                                                                    Optional.<ColumnId>empty())));

		return new ForeignKey(new CaseInsensitiveId(ctx.getText()), Collections.singletonList(id), primaryKeyReference);
	}

	@Override
	public Unsigned visitUnsigned(SqlParser.UnsignedContext ctx) {
		return new Unsigned();
	}

	@Override
	public Check visitCheck(SqlParser.CheckContext ctx) {
		ConditionTree conditionTree = ParserTools.createConditionTree(ctx.check_constraint().condition());
		return new Check(new CaseInsensitiveId(ctx.getText()), conditionTree);
	}

	@Override
	public DefaultValue visitDefault(SqlParser.DefaultContext ctx) {
		// todo: is this perhaps a better pattern with which to use the visitors?
		return new DefaultValue(new CaseInsensitiveId(ctx.getText()), ctx.value().accept(valueVisitor));
	}
}

