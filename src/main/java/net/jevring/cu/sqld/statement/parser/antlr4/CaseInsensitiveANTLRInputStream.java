/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.parser.antlr4;

import org.antlr.v4.runtime.ANTLRInputStream;

/**
 * An input stream that makes the look-ahead part of the parser case insensitive,
 * but that returns original case for the actual tokens.
 * <p>
 * A simplification of <a href="https://gist.github.com/sharwell/9424666">this</a>.
 *
 * @author markus@jevring.net
 */
public class CaseInsensitiveANTLRInputStream extends ANTLRInputStream {
	private final char[] lowercase;

	public CaseInsensitiveANTLRInputStream(String input) {
		super(input);
		// todo: it would be nice if we could do this without the recreation of a bunch of char[] and Strings.
		// however, since toLowerCase() is pretty complex, and might possibly be intrinsified at some point, 
		// if it isn't already, perhaps we simply stick with it.
		lowercase = input.toLowerCase().toCharArray();
	}

	@Override
	public int LA(int i) {
		int s = super.LA(i);
		if (s <= 0) {
			// this is validation code from the parent
			return s;
		} else {
			// we're past the validation. Grab the corresponding character from the lowercase array.
			// this 'magic', (p + i - 1) means "look ahead i steps from p (where we currently are), but adjust for a 0-based array".
			// or something along those lines, anyway. This is identical to the index used in the parent.

			// The reason we're not simply passing a lowercase string to the parent is that
			// we only want the lexer to behave in a case-insensitive manner. We want the strings
			// we *actually* get out of it to be cased in whatever way they were when they were parsed.
			return lowercase[p + i - 1];

		}
	}
}
