/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.parser.antlr4;

import net.jevring.cu.sqld.grammar.SqlBaseVisitor;
import net.jevring.cu.sqld.grammar.SqlParser;
import net.jevring.cu.sqld.statement.alterations.*;
import net.jevring.cu.sqld.structure.CaseInsensitiveId;
import net.jevring.cu.sqld.structure.column.Column;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.column.constraints.ColumnConstraint;
import org.antlr.v4.runtime.misc.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
class TableAlterationVisitor extends SqlBaseVisitor<TableAlteration> {
	private final SqlBaseVisitor<Object> valueVisitor = new ValueVisitor();

	@Override
	public AddColumn visitAlterAddColumn(@NotNull SqlParser.AlterAddColumnContext ctx) {
		List<Column<?>> columns = new ArrayList<>();
		List<ColumnConstraint> constraints = new ArrayList<>(); // we expect to not write to this
		ParserTools.handleCreateTableEntry(columns, constraints, ctx.create_table_entry());
		return new AddColumn(columns.get(0));
	}

	@Override
	public TableAlteration visitAlterAddTableConstraint(@NotNull SqlParser.AlterAddTableConstraintContext ctx) {
		List<Column<?>> columns = new ArrayList<>(); // we expect to not write to this
		List<ColumnConstraint> constraints = new ArrayList<>();
		ParserTools.handleCreateTableEntry(columns, constraints, ctx.create_table_entry());
		return new AddConstraint(constraints.get(0));
	}

	@Override
	public TableAlteration visitAlterDropColumn(@NotNull SqlParser.AlterDropColumnContext ctx) {
		// todo: why do we have a ctx.create_table_entry() in this context? 

		ColumnId columnId = new ColumnId(ctx.column_name().Identifier().getText());
		return new DropColumn(columnId);
	}

	@Override
	public TableAlteration visitAlterDropNamedConstraint(@NotNull SqlParser.AlterDropNamedConstraintContext ctx) {
		return new DropNamedConstraint(new CaseInsensitiveId(ctx.constraint_name().name.getText()));
	}

	@Override
	public TableAlteration visitAlterDropCheck(@NotNull SqlParser.AlterDropCheckContext ctx) {
		// todo: how do you drop a check that isn't named?
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public TableAlteration visitAlterDropForeignKey(@NotNull SqlParser.AlterDropForeignKeyContext ctx) {
		// todo: how do you drop a foreign key that isn't named?
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public TableAlteration visitAlterDropPrimaryKey(@NotNull SqlParser.AlterDropPrimaryKeyContext ctx) {
		return new DropPrimaryKey();
	}

	@Override
	public TableAlteration visitAlterDropUnique(@NotNull SqlParser.AlterDropUniqueContext ctx) {
		// todo: how do you drop a unique constraint that isn't named?
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public TableAlteration visitAlterSetDefault(@NotNull SqlParser.AlterSetDefaultContext ctx) {
		ColumnId columnId = new ColumnId(ctx.column_name().getText());
		Object value = ctx.value().accept(valueVisitor);
		return new SetDefaultValue(columnId, value);
	}

	@Override
	public TableAlteration visitAlterDropDefault(@NotNull SqlParser.AlterDropDefaultContext ctx) {
		ColumnId columnId = new ColumnId(ctx.column_name().getText());
		return new DropDefaultValue(columnId);
	}
}
