/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.parser.antlr4;

import net.jevring.cu.sqld.grammar.SqlBaseVisitor;
import net.jevring.cu.sqld.grammar.SqlParser;
import net.jevring.cu.sqld.statement.conditions.*;
import net.jevring.cu.sqld.statement.references.ColumnReference;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.structure.TableId;
import net.jevring.cu.sqld.structure.column.Column;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.column.constraints.AutoIncrement;
import net.jevring.cu.sqld.structure.column.constraints.ColumnConstraint;
import net.jevring.cu.sqld.structure.column.constraints.DefaultValue;
import net.jevring.cu.sqld.structure.column.constraints.Unsigned;
import net.jevring.cu.sqld.structure.column.types.ColumnType;
import org.antlr.v4.runtime.ParserRuleContext;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
class ParserTools {
	private static final SqlBaseVisitor<ColumnType<?>> columnTypeVisitor = new ColumnTypeVisitor();
	private static final SqlBaseVisitor<ColumnConstraint> tableConstraintVisitor = new TableConstraintVisitor();
	private static final SqlBaseVisitor<Object> valueVisitor = new ValueVisitor();

	public static <T, C extends ParserRuleContext> Optional<T> ofNullable(C ctx, Function<C, T> function) {
		if (ctx == null) {
			return Optional.empty();
		} else {
			return Optional.of(function.apply(ctx));
		}
	}

	public static ColumnReference createColumnReference(SqlParser.Column_referenceContext columnContext) {
		Optional<SchemaId> schemaId = ofNullable(columnContext.schema_name(), context -> new SchemaId(context.getText()));
		Optional<TableId> tableId = ofNullable(columnContext.table_name(), context -> new TableId(context.getText()));
		ColumnId columnId = new ColumnId(columnContext.column_name().getText());
		return new ColumnReference(schemaId, tableId, columnId, Optional.<ColumnId>empty());
	}

	public static Condition createCondition(SqlParser.Operator_conditionContext condition) {
		ColumnReference leftColumnReference = null;
		Object leftValue = null;
		ColumnReference rightColumnReference = null;
		Object rightValue = null;
		if (condition.left.column_reference() != null) {
			leftColumnReference = createColumnReference(condition.left.column_reference());
		} else {
			leftValue = valueVisitor.visit(condition.left.value());
		}
		if (condition.right.column_reference() != null) {
			rightColumnReference = createColumnReference(condition.right.column_reference());
		} else {
			rightValue = valueVisitor.visit(condition.right.value());
		}
		String operatorAsString = condition.operator().getText();
		Condition.Operator operator = Condition.Operator.getBySqlValue(operatorAsString);


		if (leftColumnReference != null && rightColumnReference != null) {
			return new JoinCondition(leftColumnReference, rightColumnReference, operator);
		} else if (leftColumnReference != null) {
			return new ColumnCondition(leftColumnReference, operator, rightValue);
		} else if (rightColumnReference != null) {
			return new ColumnCondition(rightColumnReference, operator, leftValue);
		} else {
			// this is value = value, which seems nonsensical, but should be supported.
			return new StaticCondition(leftValue, rightValue, operator);
		}
	}

	public static ConditionTree createConditionTree(SqlParser.ConditionContext conditionContext) {
		SqlParser.Operator_conditionContext operatorCondition = conditionContext.operator_condition();
		if (operatorCondition != null) {
			Condition c = createCondition(operatorCondition);
			return new ConditionTree(c);
		} else if (conditionContext.nested != null) {
			// in this case we had something like: '(' condition ')', 
			// in which case we need to dig in to the condition itself
			return createConditionTree(conditionContext.nested);
		} else {
			ConditionTree.Connective connective =
					ConditionTree.Connective.caseInsensitiveValueOf(conditionContext.connective().getText());
			ConditionTree left = createConditionTree(conditionContext.left);
			ConditionTree right = createConditionTree(conditionContext.right);

			return new ConditionTree(left, connective, right);
		}
	}

	public static List<ColumnId> createColumnIds(SqlParser.ColumnsContext columnsContext) {
		List<ColumnId> columns;
		if (columnsContext != null) {
			columns = columnsContext.Identifier()
			                        .stream()
			                        .map(terminalNode -> new ColumnId(terminalNode.getText()))
			                        .collect(Collectors.toList());
		} else {
			columns = Collections.emptyList();
		}
		return columns;
	}

	public static Object[] createValues(SqlParser.Value_tupleContext valueTuple) {
		List<SqlParser.ValueContext> valueNodes = valueTuple.value_list().value();
		int length = valueNodes.size();
		Object[] values = new Object[length];
		for (int i = 0; i < length; i++) {
			SqlParser.ValueContext valueNode = valueNodes.get(i);
			values[i] = valueVisitor.visit(valueNode);
		}
		return values;
	}

	public static void handleCreateTableEntry(List<Column<?>> columns,
	                                          List<ColumnConstraint> constraints,
	                                          SqlParser.Create_table_entryContext createTableEntryContext) {
		ColumnType<?> columnType = columnTypeVisitor.visit(createTableEntryContext);
		if (columnType != null) {
			ColumnId id = new ColumnId(createTableEntryContext.column_name().getText());
			SqlBaseVisitor<ColumnConstraint> columnConstraintVisitor = new ColumnConstraintVisitor(id);

			boolean notNull = false;
			Object defaultValue = null;
			int autoIncrementStep = 0;
			boolean unsigned = false;


			for (SqlParser.Column_constraintContext columnConstraintContext : createTableEntryContext.column_constraint()) {
				ColumnConstraint constraint = columnConstraintVisitor.visit(columnConstraintContext);
				if (constraint instanceof AutoIncrement) {
					autoIncrementStep = ((AutoIncrement) constraint).getStep();
				} else if (constraint instanceof Unsigned) {
					unsigned = true;
				} else if (constraint instanceof net.jevring.cu.sqld.structure.column.constraints.NotNull) {
					notNull = true;
				} else if (constraint instanceof DefaultValue) {
					defaultValue = ((DefaultValue) constraint).getValue();
				} else {
					constraints.add(constraint);
				}
			}

			// todo: generics here and for all columns everywhere!
			columns.add(new Column(id, columnType, notNull, defaultValue, autoIncrementStep, unsigned));

		} else {
			ColumnConstraint tableConstraint = tableConstraintVisitor.visit(createTableEntryContext);
			if (tableConstraint != null) {
				constraints.add(tableConstraint);
			}
		}
	}


	public static String unquoteIdentifier(String identifier) {
		// todo: it would be nice if we could get the parser to do this for us, but I don't think it'll work
		// todo: see if we can't do here what we're doing with the value, so there's one less branch
		if (identifier.charAt(0) == '`') {
			return identifier.substring(1, identifier.length() - 1);
		} else {
			return identifier;
		}

	}
}
