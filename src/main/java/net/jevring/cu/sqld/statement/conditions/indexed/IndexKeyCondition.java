/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.conditions.indexed;

import net.jevring.cu.sqld.structure.Table;
import net.jevring.cu.sqld.structure.index.Index;
import net.jevring.cu.sqld.structure.index.IndexKey;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class IndexKeyCondition implements IndexedCondition {
	private final Table table;
	private final Index index;
	private final IndexKey key;
	private final IndexedConditionTree correspondingCondition;
	// todo: should we have the representative condition here as well, so we can, in theory, turn the indexes off?

	public IndexKeyCondition(Table table, Index index, IndexKey key, IndexedConditionTree correspondingCondition) {
		this.table = table;
		this.index = index;
		this.key = key;
		this.correspondingCondition = correspondingCondition;
	}

	public Table getTable() {
		return table;
	}

	public Index getIndex() {
		return index;
	}

	public IndexKey getKey() {
		return key;
	}

	public IndexedConditionTree getCorrespondingCondition() {
		return correspondingCondition;
	}

	@Override
	public String toString() {
		return "indexLookup(" + index.getId() + ", " + key + ")";
		/*
		return "IndexKeyCondition{" +
				"table=" + table.getId() +
				", index=" + index.getId() +
				", key=" + key +
				'}';
				*/
	}
}
