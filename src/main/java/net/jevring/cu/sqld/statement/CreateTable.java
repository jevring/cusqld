/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement;

import net.jevring.cu.sqld.statement.references.TableReference;
import net.jevring.cu.sqld.storage.TableStorage;
import net.jevring.cu.sqld.structure.column.Column;
import net.jevring.cu.sqld.structure.column.constraints.ColumnConstraint;

import java.util.List;

/**
 * Represents an {@code CREATE TABLE} statement.
 *
 * @author markus@jevring.net
 */
public class CreateTable implements StructureStatement {
	private final TableReference table;
	private final List<Column<?>> columns;
	private final List<ColumnConstraint> constraints;
	private final TableStorage.Type storageType;

	public CreateTable(TableReference table,
	                   List<Column<?>> columns,
	                   List<ColumnConstraint> constraints,
	                   TableStorage.Type storageType) {
		this.table = table;
		this.columns = columns;
		this.constraints = constraints;
		this.storageType = storageType;
	}

	public TableReference getTable() {
		return table;
	}

	public List<Column<?>> getColumns() {
		return columns;
	}

	public List<ColumnConstraint> getConstraints() {
		return constraints;
	}

	public TableStorage.Type getStorageType() {
		return storageType;
	}
}
