/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.parser.antlr4;

import net.jevring.cu.sqld.grammar.SqlBaseVisitor;
import net.jevring.cu.sqld.grammar.SqlParser;
import net.jevring.cu.sqld.statement.conditions.ConditionTree;
import net.jevring.cu.sqld.statement.references.ColumnReference;
import net.jevring.cu.sqld.structure.CaseInsensitiveId;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.structure.TableId;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.column.constraints.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Turns parser results into table constraints.
 *
 * @author markus@jevring.net
 */
class TableConstraintVisitor extends SqlBaseVisitor<ColumnConstraint> {
	@Override
	public ColumnConstraint visitTableCheckConstraint(SqlParser.TableCheckConstraintContext ctx) {
		ConditionTree conditionTree = ParserTools.createConditionTree(ctx.check_constraint().condition());
		CaseInsensitiveId name;
		if (ctx.constraint_name() != null) {
			name = new CaseInsensitiveId(ctx.constraint_name().getText());
		} else {
			name = new CaseInsensitiveId(ctx.getText());
		}
		return new Check(name, conditionTree);
	}

	@Override
	public ColumnConstraint visitTableUnique(SqlParser.TableUniqueContext ctx) {
		CaseInsensitiveId name;
		if (ctx.constraint_name() != null) {
			name = new CaseInsensitiveId(ctx.constraint_name().getText());
		} else {
			name = new CaseInsensitiveId(ctx.getText());
		}
		return new Unique(name, ParserTools.createColumnIds(ctx.columns()));
	}

	@Override
	public ColumnConstraint visitTablePrimaryKey(SqlParser.TablePrimaryKeyContext ctx) {
		CaseInsensitiveId name;
		if (ctx.constraint_name() != null) {
			name = new CaseInsensitiveId(ctx.constraint_name().getText());
		} else {
			name = new CaseInsensitiveId(ctx.getText());
		}
		return new PrimaryKey(name, ParserTools.createColumnIds(ctx.columns()));
	}

	@Override
	public ColumnConstraint visitTableForeignKey(SqlParser.TableForeignKeyContext ctx) {
		List<ColumnReference> primaryKeyReference = new ArrayList<>();
		TableId tableId = new TableId(ctx.primary_key_reference().table_name().getText());
		ctx.primary_key_reference()
		   .columns()
		   .Identifier()
		   .stream()
		   .map(terminalNode -> new ColumnId(terminalNode.getText()))
		   .forEach(columnId -> primaryKeyReference.add(new ColumnReference(Optional.<SchemaId>empty(),
		                                                                    Optional.of(tableId),
		                                                                    columnId,
		                                                                    Optional.<ColumnId>empty())));
		CaseInsensitiveId name;
		if (ctx.constraint_name() != null) {
			name = new CaseInsensitiveId(ctx.constraint_name().getText());
		} else {
			name = new CaseInsensitiveId(ctx.getText());
		}
		return new ForeignKey(name, ParserTools.createColumnIds(ctx.columns()), primaryKeyReference);
	}
}

