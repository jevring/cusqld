/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.parser.antlr4;

import net.jevring.cu.sqld.grammar.SqlBaseVisitor;
import net.jevring.cu.sqld.grammar.SqlParser;
import net.jevring.cu.sqld.statement.references.ColumnReference;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.structure.TableId;
import net.jevring.cu.sqld.structure.column.ColumnId;
import org.antlr.v4.runtime.misc.NotNull;

import java.util.Optional;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
class ColumnReferenceVisitor extends SqlBaseVisitor<ColumnReference> {
	private static final Optional<SchemaId> NO_SCHEMA = Optional.<SchemaId>empty();
	private static final Optional<TableId> NO_TABLE = Optional.<TableId>empty();
	private static final Optional<ColumnId> NO_ALIAS = Optional.<ColumnId>empty();

	@Override
	public ColumnReference visitSelectStar(@NotNull SqlParser.SelectStarContext ctx) {
		return new ColumnReference(NO_SCHEMA, NO_TABLE, ColumnId.STAR, NO_ALIAS);
	}

	@Override
	public ColumnReference visitSelectColumn(@NotNull SqlParser.SelectColumnContext ctx) {
		return new ColumnReference(NO_SCHEMA, NO_TABLE, column(ctx.column_name()), NO_ALIAS);
	}

	@Override
	public ColumnReference visitSelectColumnAsAlias(@NotNull SqlParser.SelectColumnAsAliasContext ctx) {
		return new ColumnReference(NO_SCHEMA, NO_TABLE, column(ctx.column_name()), alias(ctx.column_alias()));
	}

	@Override
	public ColumnReference visitSelectTableStar(@NotNull SqlParser.SelectTableStarContext ctx) {
		return new ColumnReference(NO_SCHEMA, table(ctx.table_name()), ColumnId.STAR, NO_ALIAS);
	}

	@Override
	public ColumnReference visitSelectTableAndColumn(@NotNull SqlParser.SelectTableAndColumnContext ctx) {
		return new ColumnReference(NO_SCHEMA, table(ctx.table_name()), column(ctx.column_name()), NO_ALIAS);
	}

	@Override
	public ColumnReference visitSelectTableAndColumnAsAlias(@NotNull SqlParser.SelectTableAndColumnAsAliasContext ctx) {
		return new ColumnReference(NO_SCHEMA, table(ctx.table_name()), column(ctx.column_name()), alias(ctx.column_alias()));
	}

	@Override
	public ColumnReference visitSelectSchemaAndTableStar(@NotNull SqlParser.SelectSchemaAndTableStarContext ctx) {
		return new ColumnReference(schema(ctx.schema_name()), table(ctx.table_name()), ColumnId.STAR, NO_ALIAS);
	}

	@Override
	public ColumnReference visitSelectSchemaAndTableAndColumn(@NotNull SqlParser.SelectSchemaAndTableAndColumnContext ctx) {
		return new ColumnReference(schema(ctx.schema_name()), table(ctx.table_name()), column(ctx.column_name()), NO_ALIAS);
	}

	@Override
	public ColumnReference visitSelectSchemaAndTableAndColumnAsAlias(
			@NotNull SqlParser.SelectSchemaAndTableAndColumnAsAliasContext ctx) {
		return new ColumnReference(schema(ctx.schema_name()),
		                           table(ctx.table_name()),
		                           column(ctx.column_name()),
		                           alias(ctx.column_alias()));
	}

	private Optional<SchemaId> schema(SqlParser.Schema_nameContext schema_nameContext) {
		return Optional.of(new SchemaId(schema_nameContext.getText()));
	}

	private Optional<TableId> table(SqlParser.Table_nameContext table_nameContext) {
		return Optional.of(new TableId(table_nameContext.getText()));
	}

	private ColumnId column(SqlParser.Column_nameContext column_nameContext) {
		return new ColumnId(column_nameContext.getText());
	}


	private Optional<ColumnId> alias(SqlParser.Column_aliasContext column_aliasContext) {
		return Optional.of(new ColumnId(column_aliasContext.getText()));
	}

}
