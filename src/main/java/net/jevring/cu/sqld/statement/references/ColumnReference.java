/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.references;

import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.structure.TableId;
import net.jevring.cu.sqld.structure.column.ColumnId;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A reference to a column [in a table[in a schema]].
 *
 * @author markus@jevring.net
 * @see ResolvedColumnReference
 * @see net.jevring.cu.sqld.statement.ColumnResolver#resolve(ColumnReference)
 */
public class ColumnReference {
	private static final Pattern columnReference =
			Pattern.compile("(?:(\\w+)\\.)?(?:(\\w+)\\.)?(\\w+?)(?:\\s+AS\\s+(\\w+))?", Pattern.CASE_INSENSITIVE);
	private final Optional<SchemaId> schemaId;
	private final Optional<TableId> tableId;
	private final ColumnId columnId;
	private final Optional<ColumnId> aliasId;

	public ColumnReference(Optional<SchemaId> schemaId,
	                       Optional<TableId> tableId,
	                       ColumnId columnId,
	                       Optional<ColumnId> aliasId) {
		this.schemaId = schemaId;
		this.tableId = tableId;
		this.columnId = columnId;
		this.aliasId = aliasId;
	}

	public static ColumnReference parse(String fieldExpression) {
		Matcher matcher = columnReference.matcher(fieldExpression);
		if (matcher.matches()) {
			Optional<SchemaId> schemaId = Optional.empty();
			String potentialSchema = matcher.group(1);
			if (potentialSchema != null) {
				schemaId = Optional.of(new SchemaId(potentialSchema));
			}
			Optional<TableId> tableId = Optional.empty();
			String potentialTable = matcher.group(2);
			if (potentialTable != null) {
				tableId = Optional.of(new TableId(potentialTable));
			} else {
				// this is a special case. If there was no schema, we've matched the table as the schema.
				if (schemaId.isPresent()) {
					schemaId = Optional.empty();
					tableId = Optional.of(new TableId(matcher.group(1)));
				}
			}

			ColumnId columnId = new ColumnId(matcher.group(3));

			Optional<ColumnId> aliasId = Optional.empty();
			String potentialAlias = matcher.group(4);
			if (potentialAlias != null) {
				aliasId = Optional.of(new ColumnId(potentialAlias));
			}

			return new ColumnReference(schemaId, tableId, columnId, aliasId);
		} else {
			throw new IllegalArgumentException("Can't parse table reference: " + fieldExpression);
		}
	}

	public Optional<SchemaId> getSchemaId() {
		return schemaId;
	}

	public Optional<TableId> getTableId() {
		return tableId;
	}

	public ColumnId getColumnId() {
		return columnId;
	}

	public Optional<ColumnId> getAliasId() {
		return aliasId;
	}

	public boolean isStar() {
		return "*".equals(columnId.toString());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof ColumnReference)) {
			return false;
		}
		ColumnReference that = (ColumnReference) o;
		return Objects.equals(schemaId, that.schemaId) &&
				Objects.equals(tableId, that.tableId) &&
				Objects.equals(columnId, that.columnId) &&
				Objects.equals(aliasId, that.aliasId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(schemaId, tableId, columnId, aliasId);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (schemaId.isPresent()) {
			sb.append(schemaId.get()).append('.');
		}
		if (tableId.isPresent()) {
			sb.append(tableId.get()).append('.');
		}
		sb.append(columnId);
		if (aliasId.isPresent()) {
			sb.append(" as ").append(aliasId.get());
		}
		return sb.toString();
	}
}
