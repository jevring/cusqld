/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.parser.antlr4;

import net.jevring.cu.sqld.grammar.SqlBaseVisitor;
import net.jevring.cu.sqld.grammar.SqlParser;
import net.jevring.cu.sqld.structure.column.types.*;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.RuleNode;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Converts parser tokens into column types.
 *
 * @author markus@jevring.net
 */
class ColumnTypeVisitor extends SqlBaseVisitor<ColumnType<?>> {
	// todo: dig out some real values for these defaults
	private final int defaultCharLength = 10;
	private final int defaultVarCharLength = 50;
	private final int defaultDoubleTotalLength = 10;
	private final int defaultDoubleDecimalLength = 3;
	private final int defaultIntLength = 10;


	@Override
	protected boolean shouldVisitNextChild(RuleNode node, ColumnType<?> currentResult) {
		// without this, the parser keeps going to the constraints even after it's found the types.
		// as the constraints are handed by another visitor, we need to stop here
		return currentResult == null;
	}

	@Override
	public ColumnType<?> visitVarchar(SqlParser.VarcharContext ctx) {
		int size = sizeOrDefault(ctx.single_size(), defaultVarCharLength);
		if (size > 250) {
			return new TextColumnType();
		}
		return new VarCharColumnType(size);
	}

	@Override
	public BigDecimalColumnType visitDecimal(@NotNull SqlParser.DecimalContext ctx) {
		int size = sizeOrDefault(ctx.double_size(), defaultDoubleTotalLength);
		int decimal = decimalOrDefault(ctx.double_size(), defaultDoubleDecimalLength);
		return new BigDecimalColumnType(size, decimal);
	}

	@Override
	public IntegerColumnType visitInt(SqlParser.IntContext ctx) {
		int size = sizeOrDefault(ctx.single_size(), defaultIntLength);
		return new IntegerColumnType(size);
	}

	@Override
	public CharColumnType visitChar(@NotNull SqlParser.CharContext ctx) {
		int size = sizeOrDefault(ctx.single_size(), defaultCharLength);
		return new CharColumnType(size);
	}

	@Override
	public ColumnType<?> visitTinyText(@NotNull SqlParser.TinyTextContext ctx) {
		// todo: provide size
		return new TextColumnType();
	}

	@Override
	public TextColumnType visitText(@NotNull SqlParser.TextContext ctx) {
		// todo: no size
		return new TextColumnType();
	}

	@Override
	public BlobColumnType visitBlob(@NotNull SqlParser.BlobContext ctx) {
		return new BlobColumnType();
	}

	@Override
	public TextColumnType visitClob(@NotNull SqlParser.ClobContext ctx) {
		// todo: no size
		return new TextColumnType();
	}

	@Override
	public ColumnType<?> visitMediumText(@NotNull SqlParser.MediumTextContext ctx) {
		// todo: provide size
		return new TextColumnType();
	}

	@Override
	public ColumnType<?> visitMediumBlob(@NotNull SqlParser.MediumBlobContext ctx) {
		// todo: provide size
		return new BlobColumnType();
	}

	@Override
	public ColumnType<?> visitLongtext(@NotNull SqlParser.LongtextContext ctx) {
		// todo: provide size
		return new TextColumnType();
	}

	@Override
	public ColumnType<?> visitLongblob(@NotNull SqlParser.LongblobContext ctx) {
		// todo: provide size
		return new BlobColumnType();
	}

	@Override
	public ColumnType<?> visitSet(@NotNull SqlParser.SetContext ctx) {
		Object[] values = ParserTools.createValues(ctx.value_tuple());
		Set<Object> allowedValues = new HashSet<>();
		Collections.addAll(allowedValues, values);
		return new SetColumnType(allowedValues);
	}

	@Override
	public BooleanColumnType visitBoolean(@NotNull SqlParser.BooleanContext ctx) {
		return new BooleanColumnType();
	}

	@Override
	public ColumnType<?> visitEnum(@NotNull SqlParser.EnumContext ctx) {
		Object[] values = ParserTools.createValues(ctx.value_tuple());
		Set<Object> allowedValues = new HashSet<>();
		Collections.addAll(allowedValues, values);
		return new EnumColumnType(allowedValues);
	}

	@Override
	public ColumnType<?> visitDate(@NotNull SqlParser.DateContext ctx) {
		return new DateColumnType();
	}

	@Override
	public ColumnType<?> visitDatetime(@NotNull SqlParser.DatetimeContext ctx) {
		return new DateTimeColumnType();
	}

	@Override
	public ColumnType<?> visitTimestamp(@NotNull SqlParser.TimestampContext ctx) {
		return new TimestampColumnType();
	}

	@Override
	public ColumnType<?> visitTime(@NotNull SqlParser.TimeContext ctx) {
		return new TimeColumnType();
	}

	@Override
	public ColumnType<?> visitYear(@NotNull SqlParser.YearContext ctx) {
		return new YearColumnType();
	}

	@Override
	public ByteColumnType visitTinyInt(@NotNull SqlParser.TinyIntContext ctx) {
		int size = sizeOrDefault(ctx.single_size(), defaultIntLength);
		return new ByteColumnType(size);
	}

	@Override
	public ShortColumnType visitSmallInt(@NotNull SqlParser.SmallIntContext ctx) {
		int size = sizeOrDefault(ctx.single_size(), defaultIntLength);
		return new ShortColumnType(size);
	}

	@Override
	public IntegerColumnType visitMediumInt(@NotNull SqlParser.MediumIntContext ctx) {
		int size = sizeOrDefault(ctx.single_size(), defaultIntLength);
		return new IntegerColumnType(size);
	}

	@Override
	public LongColumnType visitBigInt(@NotNull SqlParser.BigIntContext ctx) {
		int size = sizeOrDefault(ctx.single_size(), defaultIntLength);
		return new LongColumnType(size);
	}

	@Override
	public FloatColumnType visitFloat(@NotNull SqlParser.FloatContext ctx) {
		int size = sizeOrDefault(ctx.double_size(), defaultDoubleTotalLength);
		int decimal = decimalOrDefault(ctx.double_size(), defaultDoubleDecimalLength);
		return new FloatColumnType(size, decimal);
	}

	@Override
	public DoubleColumnType visitDouble(@NotNull SqlParser.DoubleContext ctx) {
		int size = sizeOrDefault(ctx.double_size(), defaultDoubleTotalLength);
		int decimal = decimalOrDefault(ctx.double_size(), defaultDoubleDecimalLength);
		return new DoubleColumnType(size, decimal);
	}

	private int tokenOrDefault(Token potentialSize, int defaultSize) {
		if (potentialSize == null) {
			return defaultSize;
		} else {
			return Integer.valueOf(potentialSize.getText());
		}
	}

	private int sizeOrDefault(SqlParser.Single_sizeContext sizeContext, int defaultSize) {
		if (sizeContext == null) {
			return defaultSize;
		} else {
			return tokenOrDefault(sizeContext.size, defaultSize);
		}
	}

	private int sizeOrDefault(SqlParser.Double_sizeContext sizeContext, int defaultSize) {
		if (sizeContext == null) {
			return defaultSize;
		} else {
			return tokenOrDefault(sizeContext.size, defaultSize);
		}
	}

	private int decimalOrDefault(SqlParser.Double_sizeContext sizeContext, int defaultSize) {
		if (sizeContext == null) {
			return defaultSize;
		} else {
			return tokenOrDefault(sizeContext.decimal, defaultSize);
		}
	}
}

