/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.parser.antlr4;

import net.jevring.cu.sqld.grammar.SqlBaseVisitor;
import net.jevring.cu.sqld.grammar.SqlParser;
import net.jevring.cu.sqld.statement.*;
import net.jevring.cu.sqld.statement.alterations.TableAlteration;
import net.jevring.cu.sqld.statement.conditions.ConditionTree;
import net.jevring.cu.sqld.statement.references.ColumnReference;
import net.jevring.cu.sqld.statement.references.OrderBy;
import net.jevring.cu.sqld.statement.references.TableReference;
import net.jevring.cu.sqld.storage.TableStorage;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.structure.TableId;
import net.jevring.cu.sqld.structure.column.Column;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.column.constraints.ColumnConstraint;
import org.antlr.v4.runtime.misc.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * A visitor for all the root statement types.
 *
 * @author markus@jevring.net
 */
class StatementVisitor extends SqlBaseVisitor<Statement> {

	private final SqlBaseVisitor<Object> valueVisitor = new ValueVisitor();
	private final SqlBaseVisitor<ColumnReference> columnReferenceVisitor = new ColumnReferenceVisitor();
	private final SqlBaseVisitor<TableAlteration> columnAlterationVisitor = new TableAlterationVisitor();

	@Override
	public CreateTable visitCreateTable(SqlParser.CreateTableContext ctx) {
		List<Column<?>> columns = new ArrayList<>();
		List<ColumnConstraint> constraints = new ArrayList<>();
		if (ctx.create_table_entries() != null) {
			for (SqlParser.Create_table_entryContext createTableEntryContext : ctx.create_table_entries().create_table_entry()) {
				// it can be either a column or a constraint
				ParserTools.handleCreateTableEntry(columns, constraints, createTableEntryContext);
			}
		}
		// todo: this is not going to work in reality. We can't have the memory storage be the default one. it has to be disk
		TableStorage.Type storageType = TableStorage.Type.Memory;
		if (ctx.engine != null) {
			storageType = TableStorage.Type.valueOf(ctx.engine.getText());
		}
		return new CreateTable(createTableReference(ctx.full_table()), columns, constraints, storageType);
	}

	@Override
	public CreateSchema visitCreateSchema(SqlParser.CreateSchemaContext ctx) {
		return new CreateSchema(new SchemaId(ctx.schema_name().getText()));
	}

	@Override
	public CreateSchema visitCreateDatabase(SqlParser.CreateDatabaseContext ctx) {
		return new CreateSchema(new SchemaId(ctx.schema_name().getText()));
	}

	@Override
	public AlterTable visitAlter(@NotNull SqlParser.AlterContext ctx) {
		List<TableAlteration> alterations = new ArrayList<>();
		for (SqlParser.Alter_operationContext alterOperationContext : ctx.alter_operations().alter_operation()) {
			TableAlteration tableAlteration = columnAlterationVisitor.visit(alterOperationContext);
			alterations.add(tableAlteration);
		}

		return new AlterTable(createTableReference(ctx.full_table()), alterations);
	}

	@Override
	public DropTable visitDropTable(SqlParser.DropTableContext ctx) {
		return new DropTable(createTableReference(ctx.full_table()));
	}

	@Override
	public DropSchema visitDropSchema(SqlParser.DropSchemaContext ctx) {
		return new DropSchema(new SchemaId(ctx.schema_name().getText()));
	}

	@Override
	public DropSchema visitDropDatabase(SqlParser.DropDatabaseContext ctx) {
		return new DropSchema(new SchemaId(ctx.schema_name().getText()));
	}

	@Override
	public Select visitSelect(@NotNull SqlParser.SelectContext ctx) {
		if (ctx.lastInsertId != null) {
			return new SelectLastInsertId();
		}
		TableReference table;
		List<Join> joins = new ArrayList<>();

		List<TableReference> tables = createTables(ctx.tables());
		table = tables.get(0);
		for (int i = 1; i < tables.size(); i++) {
			TableReference tableReference = tables.get(i);
			joins.add(new Join(tableReference, Join.Type.Cross));
		}

		List<ColumnReference> columns = new ArrayList<>();
		for (SqlParser.Selection_columnContext columnContext : ctx.selection_columns().selection_column()) {
			ColumnReference columnReference = columnReferenceVisitor.visit(columnContext);
			columns.add(columnReference);
		}

		List<OrderBy> ordering = new ArrayList<>();
		if (ctx.order_by() != null) {
			for (SqlParser.Column_and_directionContext columnAndDirectionContext : ctx.order_by().column_and_direction()) {
				ColumnReference columnReference = ParserTools.createColumnReference(columnAndDirectionContext.column_reference());

				// todo: default this to ASC, but detect it in a nice way
				OrderBy.Direction direction =
						OrderBy.Direction.valueOf(columnAndDirectionContext.sort_order().getText().toUpperCase());
				ordering.add(new OrderBy(columnReference, direction));
			}
		}
		Optional<Integer> limit = Optional.empty();
		if (ctx.limit() != null) {
			limit = Optional.of(Integer.parseInt(ctx.limit().getText()));
		}

		Optional<ConditionTree> conditions =
				ParserTools.ofNullable(ctx.where(), whereContext -> ParserTools.createConditionTree(ctx.where().condition()));

		if (ctx.joins() != null) {
			for (SqlParser.JoinContext joinContext : ctx.joins().join()) {
				Join.Type joinType = Join.Type.Inner;
				if (joinContext.join_type() != null) {
					// only the first child here will be useful.
					// the second will occasionally contain the word 'outer' which is superfluous
					joinType = Join.Type.caseInsensitiveValueOf(joinContext.join_type().getChild(0).getText());
				}
				TableReference tableReference = createTableReference(joinContext.table());
				SqlBaseVisitor<Optional<ConditionTree>> joinConditionVisitor = new SqlBaseVisitor<Optional<ConditionTree>>() {
					@Override
					public Optional<ConditionTree> visitJoinOn(@NotNull SqlParser.JoinOnContext ctx) {
						return Optional.of(ParserTools.createConditionTree(ctx.condition()));
					}

					@Override
					public Optional<ConditionTree> visitJoinUsing(@NotNull SqlParser.JoinUsingContext ctx) {
						throw new UnsupportedOperationException("'join using' not yet implemented");
					}

					@Override
					protected Optional<ConditionTree> defaultResult() {
						return Optional.empty();
					}
				};
				Optional<ConditionTree> conditionTree = joinConditionVisitor.visit(joinContext);

				// todo: validate whether there's a difference between the ON and the WHERE statement.
				// for instance, if conditions in the ON *only* apply to that join, etc, 
				// or if it's just a convenient and clear way of writing.
				conditions = join(conditions, conditionTree);
				Join join = new Join(tableReference, joinType);
				joins.add(join);
			}
		}

		return new Select(table, columns, joins, ordering, limit, conditions);
	}

	private Optional<ConditionTree> join(Optional<ConditionTree> existing, Optional<ConditionTree> toAdd) {
		if (existing.isPresent()) {
			if (toAdd.isPresent()) {
				// actually join things together
				return Optional.of(new ConditionTree(existing.get(), ConditionTree.Connective.And, toAdd.get()));
			} else {
				return existing;
			}
		} else {
			return toAdd;
		}
	}

	@Override
	public Insert visitInsert(@NotNull SqlParser.InsertContext ctx) {
		TableReference tableReference = createTableReference(ctx.table());
		SqlParser.ColumnsContext columnsContext = ctx.columns();
		List<ColumnId> columns = ParserTools.createColumnIds(columnsContext);
		// todo: ensure that we can handle the case where these are of different length (when processing them, that is. here we're already fine, I think)
		List<Object[]> values;
		Optional<Select> select;

		SqlParser.What_to_insertContext whatToInsert = ctx.what_to_insert();
		if (whatToInsert.select() != null) {
			select = Optional.of(visitSelect(whatToInsert.select()));
			values = Collections.emptyList();
		} else {
			select = Optional.empty();
			values = new ArrayList<>();
			for (SqlParser.Value_tupleContext valueTuple : whatToInsert.value_tuples().value_tuple()) {
				values.add(ParserTools.createValues(valueTuple));
			}
		}
		return new Insert(tableReference, columns, values, select);
	}

	@Override
	public Update visitUpdate(@NotNull SqlParser.UpdateContext ctx) {
		TableReference tableReference = createTableReference(ctx.table());
		Optional<ConditionTree> conditions =
				ParserTools.ofNullable(ctx.where(), whereContext -> ParserTools.createConditionTree(ctx.where().condition()));
		List<FieldUpdate> updates = new ArrayList<>();
		for (SqlParser.Key_value_pairContext keyValuePair : ctx.key_value_pairs().key_value_pair()) {
			ColumnReference columnReference = ParserTools.createColumnReference(keyValuePair.key);
			Object value = valueVisitor.visit(keyValuePair.value());
			FieldUpdate fieldUpdate = new FieldUpdate(columnReference, value);
			updates.add(fieldUpdate);
		}
		return new Update(tableReference, updates, conditions);
	}

	@Override
	public Delete visitDelete(@NotNull SqlParser.DeleteContext ctx) {
		TableReference tableReference = createTableReference(ctx.table());
		Optional<ConditionTree> conditions =
				ParserTools.ofNullable(ctx.where(), whereContext -> ParserTools.createConditionTree(ctx.where().condition()));
		return new Delete(tableReference, conditions);
	}

	private List<TableReference> createTables(SqlParser.TablesContext tablesContext) {
		List<TableReference> tables = new ArrayList<>();
		for (SqlParser.TableContext tableContext : tablesContext.table()) {
			TableReference tableReference = createTableReference(tableContext);
			tables.add(tableReference);
		}
		return tables;
	}

	private TableReference createTableReference(SqlParser.Full_tableContext fullTableContext) {
		Optional<SchemaId> schemaId = Optional.empty();
		if (fullTableContext.schema_name() != null) {
			schemaId = Optional.of(new SchemaId(ParserTools.unquoteIdentifier(fullTableContext.schema_name().getText())));
		}
		TableId tableId = new TableId(ParserTools.unquoteIdentifier(fullTableContext.table_name().getText()));
		return new TableReference(schemaId, tableId, Optional.<TableId>empty());
	}

	private TableReference createTableReference(SqlParser.TableContext tableContext) {
		Optional<SchemaId> schemaId =
				ParserTools.ofNullable(tableContext.schema_name(), context -> new SchemaId(context.getText()));
		TableId tableId = new TableId(tableContext.table_name().getText());
		Optional<TableId> tableAlias =
				ParserTools.ofNullable(tableContext.table_alias(), context -> new TableId(context.getText()));
		return new TableReference(schemaId, tableId, tableAlias);
	}

}
