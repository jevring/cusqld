/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement;

import net.jevring.cu.sqld.statement.conditions.ConditionTree;
import net.jevring.cu.sqld.statement.references.ColumnReference;
import net.jevring.cu.sqld.statement.references.OrderBy;
import net.jevring.cu.sqld.statement.references.TableReference;

import java.util.List;
import java.util.Optional;

/**
 * Represents the {@code SELECT} statement.
 *
 * @author markus@jevring.net
 */
public class Select implements Statement {
	private final TableReference table;
	private final List<ColumnReference> columns;
	private final List<Join> joins;
	private final List<OrderBy> ordering;
	private final Optional<Integer> limit;
	private final Optional<ConditionTree> conditions;

	public Select(TableReference table,
	              List<ColumnReference> columns,
	              List<Join> joins,
	              List<OrderBy> ordering,
	              Optional<Integer> limit,
	              Optional<ConditionTree> conditions) {
		this.table = table;
		this.columns = columns;
		this.joins = joins;
		this.ordering = ordering;
		this.limit = limit;
		this.conditions = conditions;
	}

	public TableReference getTable() {
		return table;
	}

	public List<ColumnReference> getColumns() {
		return columns;
	}

	public List<Join> getJoins() {
		return joins;
	}

	public List<OrderBy> getOrdering() {
		return ordering;
	}

	public Optional<Integer> getLimit() {
		return limit;
	}

	public Optional<ConditionTree> getConditions() {
		return conditions;
	}
}
