/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.references;

import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.structure.TableId;

import java.util.Objects;
import java.util.Optional;

/**
 * A table reference contains a canonical address to a table in a schema,
 * and an alias.
 *
 * @author markus@jevring.net
 */
public class TableReference {
	private final Optional<SchemaId> schemaId;
	private final TableId tableId;
	private final Optional<TableId> aliasId;

	public TableReference(Optional<SchemaId> schemaId, TableId tableId, Optional<TableId> aliasId) {
		this.schemaId = schemaId;
		this.tableId = tableId;
		this.aliasId = aliasId;
	}

	public Optional<SchemaId> getSchemaId() {
		return schemaId;
	}

	public TableId getTableId() {
		return tableId;
	}

	public Optional<TableId> getAliasId() {
		return aliasId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof TableReference)) {
			return false;
		}
		TableReference that = (TableReference) o;
		return Objects.equals(schemaId, that.schemaId) &&
				Objects.equals(tableId, that.tableId) &&
				Objects.equals(aliasId, that.aliasId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(schemaId, tableId, aliasId);
	}

	@Override
	public String toString() {
		return "TableReference{" +
				"schemaId=" + schemaId +
				", tableId=" + tableId +
				", aliasId=" + aliasId +
				'}';
	}
}
