/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement;

import net.jevring.cu.sqld.statement.references.TableReference;

/**
 * Represents a {@code JOIN} statement fragment.
 *
 * @author markus@jevring.net
 */
public class Join {
	private final TableReference tableReference;
	private final Type type;

	public Join(TableReference tableReference, Type type) {
		this.tableReference = tableReference;
		this.type = type;
	}

	public TableReference getTableReference() {
		return tableReference;
	}

	public Type getType() {
		return type;
	}

	// todo: document which join behaves in which way

	// todo: so, according to https://en.wikipedia.org/wiki/Join_%28SQL%29, an INNER JOIN is effectively a cross join with parameters.
	// That's what I thought a cross join was. 
	// am I to understand that a cross join takes no parameters?
	public enum Type {
		Inner,
		/**
		 * A cross join is a cartesian product of all involved tables.
		 * This is the same behavior as specifying multiple tables
		 * in the {@code FROM} clause.
		 */
		Cross,
		Right,
		Left,
		Full;

		public static Type caseInsensitiveValueOf(String value) {
			for (Type type : values()) {
				if (type.name().equalsIgnoreCase(value)) {
					return type;
				}
			}
			throw new IllegalArgumentException("Unknown join type: " + value);
		}
	}
}
