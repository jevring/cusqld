/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.conditions.indexed;

import net.jevring.cu.sqld.statement.ColumnResolver;
import net.jevring.cu.sqld.statement.conditions.ColumnCondition;
import net.jevring.cu.sqld.statement.conditions.Condition;
import net.jevring.cu.sqld.statement.conditions.ConditionTree;
import net.jevring.cu.sqld.statement.conditions.JoinCondition;
import net.jevring.cu.sqld.statement.references.ColumnReference;
import net.jevring.cu.sqld.statement.references.IndexReference;
import net.jevring.cu.sqld.statement.references.ResolvedColumnReference;
import net.jevring.cu.sqld.statement.references.ResolvedTableReference;
import net.jevring.cu.sqld.structure.Table;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.index.Index;
import net.jevring.cu.sqld.structure.index.IndexKey;

import java.sql.SQLException;
import java.util.*;

/**
 * Builds a condition tree with a combination of index look-ups
 * and normal column-index based value comparisons.
 *
 * @author markus@jevring.net
 */
public class IndexedConditionTreeBuilder {
	private final Map<ResolvedColumnReference, Integer> columnsToIndexes;
	private final Optional<ConditionTree> rootConditionTree;
	private final ColumnResolver columnResolver;

	public IndexedConditionTreeBuilder(Map<ResolvedColumnReference, Integer> columnsToIndexes,
	                                   Optional<ConditionTree> rootConditionTree,
	                                   ColumnResolver columnResolver) {
		this.columnsToIndexes = columnsToIndexes;
		this.rootConditionTree = rootConditionTree;
		this.columnResolver = columnResolver;
	}

	public IndexedConditionTreeFetcher createFetcher(Table table) throws SQLException {
		TreeNode tree =
				createIndexedConditionTree(rootConditionTree, true, Collections.singletonMap(table.getReference(), table));
		// todo: see if we can't order things within one "level". Ideally we want index lookups first, rather than simple comparison operations
		System.out.println("tree = " + tree);
		return new IndexedConditionTreeFetcher(tree.getIndexedConditionTree(), table);
	}

	public IndexedConditionTreeMatcher createJoinConditionMatcher(Set<Table> tables) throws SQLException {
		Map<ResolvedTableReference, Table> applicableTables = new HashMap<>();
		for (Table table : tables) {
			applicableTables.put(table.getReference(), table);
		}
		TreeNode tree = createIndexedConditionTree(rootConditionTree, false, applicableTables);
		System.out.println("tree = " + tree);
		return new IndexedConditionTreeMatcher(tree.getIndexedConditionTree());
	}

	private IndexReference newIndexReference(Table table, Index index) {
		return new IndexReference(table.getReference(), index.getId());
	}

	private TreeNode createIndexedConditionTree(Optional<ConditionTree> conditions,
	                                            boolean skipJoinConditions,
	                                            Map<ResolvedTableReference, Table> applicableTables) throws SQLException {
		if (conditions.isPresent()) {
			ConditionTree conditionTree = conditions.get();
			if (conditionTree.getCondition().isPresent()) {
				Condition condition = conditionTree.getCondition().get();
				if (condition instanceof ColumnCondition) {
					ColumnCondition cc = (ColumnCondition) condition;
					ColumnReference column = cc.getColumn();
					ResolvedColumnReference resolvedColumnReference = columnResolver.resolve(column);
					Table applicableTable = applicableTables.get(resolvedColumnReference.getTableReference());
					if (applicableTable == null) {
						// if this column isn't applicable to the tables we're currently 
						// creating the indexed condition tree for, there's no point in
						// including them.
						return TreeNode.TRUE;
					}
					int columnIndex = columnsToIndexes.get(resolvedColumnReference);
					Object value = cc.getValue();
					ColumnId columnId = resolvedColumnReference.getColumnId();
					IndexedColumnCondition indexedColumnCondition =
							new IndexedColumnCondition(columnIndex, cc.getOperator(), value, applicableTable);

					List<IndexKeyCondition> indexKeyConditions = new ArrayList<>();
					Map<IndexReference, PartialIndexKey> partialIndexKeys = new HashMap<>();
					if (cc.getOperator() == Condition.Operator.Equals) { // todo: later, when we have ordered indexes, we will need to expand this to deal with <, >, etc
						// see what full and/or partial index keys we can make here

						// while we only have a single table, it's possible that this column is part of multiple indexes, 
						// so we still have to carry the potential indexes out, alongside the complete indexes

						for (Index index : applicableTable.getIndexes()) {
							IndexReference indexReference = newIndexReference(applicableTable, index);
							if (index.getColumns().contains(columnId)) {
								int indexSize = index.getColumns().size();
								if (indexSize == 1) {
									// complete index key
									indexKeyConditions.add(new IndexKeyCondition(applicableTable,
									                                             index,
									                                             new IndexKey(value),
									                                             new IndexedConditionTree(indexedColumnCondition)));
								} else {
									// partial index key
									PartialIndexKey partialIndexKey = new PartialIndexKey(index, indexedColumnCondition);
									int indexIndex = index.getMapping().toIndexIndex(columnIndex);
									if (indexIndex >= 0) {
										// todo: when would this ever be < 0 ???
										partialIndexKey.set(indexIndex, value);
									}
									partialIndexKeys.put(indexReference, partialIndexKey);
								}
							}
						}
						if (!indexKeyConditions.isEmpty()) {
							if (indexKeyConditions.size() == 1) {
								return new TreeNode(partialIndexKeys, new IndexedConditionTree(indexKeyConditions.get(0)));
							} else {
								return new TreeNode(partialIndexKeys,
								                    new IndexedConditionTree(new IndexChoiceCondition(indexKeyConditions)));
							}
						}
					}
					return new TreeNode(partialIndexKeys, new IndexedConditionTree(indexedColumnCondition));
				} else if (condition instanceof JoinCondition) {
					JoinCondition jc = (JoinCondition) condition;
					if (skipJoinConditions) {
						return TreeNode.TRUE;
					}

					ResolvedColumnReference leftResolved = columnResolver.resolve(jc.getLeft());
					ResolvedColumnReference rightResolved = columnResolver.resolve(jc.getRight());

					Table leftApplicableTable = applicableTables.get(leftResolved.getTableReference());
					Table rightApplicableTable = applicableTables.get(rightResolved.getTableReference());
					if (leftApplicableTable == null || rightApplicableTable == null) {
						// we OR these together as BOTH columns in a join condition
						// must be applicable, otherwise it will NEVER evaluate to true.
						// remember de morgan. We could have !(a && b)
						return TreeNode.TRUE;
					}

					int leftIndex = columnsToIndexes.get(leftResolved);
					int rightIndex = columnsToIndexes.get(rightResolved);
					// todo: join conditions also need to support index lookups
					return new TreeNode(new IndexedConditionTree(new IndexedJoinCondition(leftIndex,
					                                                                      rightIndex,
					                                                                      jc.getOperator())));
				} else {
					throw new IllegalArgumentException("Unsupported condition type: " + condition);
				}
			} else {
				TreeNode leftBranch = createIndexedConditionTree(conditionTree.getLeft(), skipJoinConditions, applicableTables);
				TreeNode rightBranch = createIndexedConditionTree(conditionTree.getRight(), skipJoinConditions, applicableTables);
				ConditionTree.Connective connective = conditionTree.getConnective().get();
				if (leftBranch == TreeNode.TRUE && rightBranch == TreeNode.TRUE) {
					// we do these micro-optimization checks to simply reduce the number recursive calls we have to do.
					// while this might not seem like much, keep in mind that we're in this method considerably less
					// than we are in the method that compares things for each row...
					return TreeNode.TRUE;
				} else {
					IndexedConditionTree indexedConditionTree = new IndexedConditionTree(leftBranch.getIndexedConditionTree(),
					                                                                     connective,
					                                                                     rightBranch.getIndexedConditionTree());
					if (connective == ConditionTree.Connective.Or) {
						// there's nothing we can do at this stage.
						// only partial keys on either side of an AND condition can be combined.
						return new TreeNode(indexedConditionTree);

						// todo: see about turning (a && (b || c)) into (a && b) || (a && c)
						// note: this might entail more than two columns, so this will be a bit tricky
						// this might be a query planning issue. See which is faster based on some metric
						// Actually, let's completely skip that for now, as it's apparently hard enough to get normal indexes to work...
						// this also applies to (a && (b && c)), as this could, given the right index, 
						// be written like (a && b) && (a && c)
						// it's basic multiplication, really

					} else {
						// todo. there's actually another point we have to consider: how can we use indexes when performing joins?
						// given that, in that case, we'll need to create index keys on the fly.
						// ideally we'd like to use the same code for this.

						Map<IndexReference, PartialIndexKey> leftPartialIndexKeys = new HashMap<>(); // populated
						Map<IndexReference, PartialIndexKey> rightPartialIndexKeys =
								new HashMap<>(rightBranch.getPartialIndexKeys()); // removed from and repopulated when there's no match
						for (Map.Entry<IndexReference, PartialIndexKey> leftEntry : leftBranch.getPartialIndexKeys().entrySet()) {
							IndexReference indexReference = leftEntry.getKey();
							PartialIndexKey leftPartialIndexKey = leftEntry.getValue();

							PartialIndexKey rightPartialIndexKey = rightPartialIndexKeys.remove(indexReference);
							if (rightPartialIndexKey != null) {
								PartialIndexKey combinedKey = leftPartialIndexKey.tryCombine(rightPartialIndexKey);
								if (combinedKey != null) {
									if (combinedKey.isComplete()) {
										Table table = applicableTables.get(indexReference.getTableReference());
										List<IndexedColumnCondition> correspondingConditions = new ArrayList<>();
										correspondingConditions.addAll(leftPartialIndexKey.conditions);
										correspondingConditions.addAll(rightPartialIndexKey.conditions);
										IndexKeyCondition indexKeyCondition = new IndexKeyCondition(table,
										                                                            combinedKey.getIndex(),
										                                                            combinedKey.getIndexKey(),
										                                                            combineWithAnd(
												                                                            correspondingConditions));


										indexedConditionTree = remove(indexedConditionTree, correspondingConditions);
										if (indexedConditionTree == null) {
											// we've removed EVERYTHING in the tree. Only the index remains.
											indexedConditionTree = new IndexedConditionTree(indexKeyCondition);
										} else {
											indexedConditionTree =
													new IndexedConditionTree(new IndexedConditionTree(indexKeyCondition),
													                         ConditionTree.Connective.And,
													                         indexedConditionTree);
										}
									} else {
										leftPartialIndexKeys.put(indexReference, combinedKey);
									}
								} else {
									// we couldn't do anything. Put it back where it came from
									rightPartialIndexKeys.put(indexReference, leftPartialIndexKey);
									// todo: this ONLY happens if the partial index keys are for the same column.
									// in this case, in reality, supporting only equals, and the values not being the same, we'd get an empty set. always.
									// int he future, when we support ordered indexes, we might have something like
									// "a > 5 && a <= 15", meaning that a is inside a range.
									// this must, of course, be allowed.
									// in this case we'd end up with 2 partial (but now even more filled than before) keys from the original 2
								}
							} else {
								leftPartialIndexKeys.put(indexReference, leftPartialIndexKey);
							}
						}
						// this loop takes care of the things that match both sides.
						// whatever else either ends up in the left side again or remains in the right side.
						// partial keys that become complete index keys seize to be partial and become
						// complete for this whole branch.

						// at this point there was no overlap between left and right that hasn't already been processed, so we can join them.
						leftPartialIndexKeys.putAll(rightPartialIndexKeys);
						return new TreeNode(leftPartialIndexKeys, indexedConditionTree);
					}
				}
			}
		} else {
			// if there are no conditions, the condition is trivially true
			return TreeNode.TRUE;
		}

	}

	private IndexedConditionTree combineWithAnd(List<IndexedColumnCondition> correspondingConditions) {
		IndexedConditionTree tree = new IndexedConditionTree(correspondingConditions.get(0));
		for (int i = 1; i < correspondingConditions.size(); i++) {
			IndexedColumnCondition indexedColumnCondition = correspondingConditions.get(i);
			tree = new IndexedConditionTree(tree, ConditionTree.Connective.And, new IndexedConditionTree(indexedColumnCondition));
		}
		return tree;
	}

	private IndexedConditionTree remove(IndexedConditionTree from, List<IndexedColumnCondition> toRemove) {
		if (from.getConnective().isPresent()) {
			IndexedConditionTree left = remove(from.getLeft().get(), toRemove);
			IndexedConditionTree right = remove(from.getRight().get(), toRemove);
			if (left == null) {
				if (right == null) {
					return null;
				} else {
					return right;
				}
			} else {
				if (right == null) {
					return left;
				} else {
					return new IndexedConditionTree(left, from.getConnective().get(), right);
				}
			}
		} else {
			IndexedCondition potential = from.getCondition().get();
			// todo: doesn't this mean they should be sets? for faster removal

			// the assumption is that it can't be in BOTH left and right.

			if (toRemove.contains(potential)) {
				return null;
			} else {
				// no change. return this object as it is
				return from;
			}
		}
	}

	private static final class TreeNode {
		/**
		 * This is used to prune the tree for branches that are not applicable.
		 */
		private static final TreeNode TRUE = new TreeNode(IndexedConditionTree.TRUE);
		private final Map<IndexReference, PartialIndexKey> partialIndexKeys;
		private final IndexedConditionTree indexedConditionTree;

		public TreeNode(Map<IndexReference, PartialIndexKey> partialIndexKeys, IndexedConditionTree indexedConditionTree) {
			this.partialIndexKeys = partialIndexKeys;
			this.indexedConditionTree = indexedConditionTree;
		}

		public TreeNode(IndexedConditionTree indexedConditionTree) {
			this.partialIndexKeys = Collections.emptyMap();
			this.indexedConditionTree = indexedConditionTree;
		}

		public Map<IndexReference, PartialIndexKey> getPartialIndexKeys() {
			return partialIndexKeys;
		}

		public IndexedConditionTree getIndexedConditionTree() {
			return indexedConditionTree;
		}

		@Override
		public String toString() {
			return "TreeNode{" +
					"\npartialIndexKeys=" + partialIndexKeys +
					"\nindexedConditionTree=" + indexedConditionTree +
					"\n}";
		}
	}

	private static final class PartialIndexKey {
		private final List<IndexedColumnCondition> conditions = new ArrayList<>();
		private final Object[] values;
		private final Index index;

		private PartialIndexKey(Index index, IndexedColumnCondition condition) {
			this.conditions.add(condition);
			this.values = new Object[index.getColumns().size()];
			this.index = index;
		}

		private PartialIndexKey(Index index, List<IndexedColumnCondition> conditions) {
			this.conditions.addAll(conditions);
			this.values = new Object[index.getColumns().size()];
			this.index = index;
		}

		public Index getIndex() {
			return index;
		}

		public void set(int i, Object value) {
			this.values[i] = value;
		}

		public boolean isComplete() {
			for (Object value : values) {
				if (value == null) {
					// note: this doesn't allow null values in the key of the index. That might be an issue
					return false;
				}
			}
			return true;
		}

		public IndexKey getIndexKey() {
			if (!isComplete()) {
				throw new IllegalStateException("Index key is not complete");
			}
			return new IndexKey(values);
		}

		public PartialIndexKey tryCombine(PartialIndexKey other) {
			List<IndexedColumnCondition> combinedConditions = new ArrayList<>();
			combinedConditions.addAll(conditions);
			combinedConditions.addAll(other.conditions);
			PartialIndexKey potential = new PartialIndexKey(index, combinedConditions);
			for (int i = 0; i < values.length; i++) {
				Object mine = values[i];
				Object theirs = other.values[i];
				if (mine == null && theirs != null) {
					potential.set(i, theirs);
				} else if (theirs == null && mine != null) {
					potential.set(i, mine);
				} else {
					// they're both not null.
					// that means we have values for the same field.
					// we can do nothing at this point. combination failed.
					return null;
				}

			}
			return potential;
		}

		@Override
		public String toString() {
			return "PartialIndexKey{" + Arrays.toString(values) + ", " + conditions + '}';
		}
	}
}
