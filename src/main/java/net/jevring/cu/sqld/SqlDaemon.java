/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld;

import net.jevring.cu.sqld.connection.ConnectionContext;
import net.jevring.cu.sqld.statement.*;
import net.jevring.cu.sqld.statement.alterations.TableAlteration;
import net.jevring.cu.sqld.statement.conditions.ColumnCondition;
import net.jevring.cu.sqld.statement.conditions.Condition;
import net.jevring.cu.sqld.statement.conditions.ConditionTree;
import net.jevring.cu.sqld.statement.conditions.JoinCondition;
import net.jevring.cu.sqld.statement.conditions.indexed.IndexedConditionTreeBuilder;
import net.jevring.cu.sqld.statement.conditions.indexed.IndexedConditionTreeFetcher;
import net.jevring.cu.sqld.statement.conditions.indexed.IndexedConditionTreeMatcher;
import net.jevring.cu.sqld.statement.parser.Parser;
import net.jevring.cu.sqld.statement.parser.antlr4.AntlrSqlParserWrapper;
import net.jevring.cu.sqld.statement.references.*;
import net.jevring.cu.sqld.storage.*;
import net.jevring.cu.sqld.storage.disk.variable.VariableWidthTablePerFileTableStorage;
import net.jevring.cu.sqld.storage.memory.MemoryRow;
import net.jevring.cu.sqld.storage.memory.MemoryTableStorage;
import net.jevring.cu.sqld.structure.*;
import net.jevring.cu.sqld.structure.column.Column;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.column.constraints.ColumnConstraint;
import net.jevring.cu.sqld.structure.column.types.ColumnType;
import net.jevring.cu.sqld.structure.column.types.DoubleColumnType;
import net.jevring.cu.sqld.structure.column.types.IntegerColumnType;
import net.jevring.cu.sqld.structure.index.Index;
import net.jevring.cu.sqld.util.Util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * An sql daemon that stores and retrieves data based on statements.
 * Statements can be acquired using a parser.
 *
 * @author markus@jevring.net
 * @see Statement
 * @see net.jevring.cu.sqld.statement.parser.Parser
 */
public class SqlDaemon implements StatementExecutor {
	private static final Charset UTF_8 = Charset.forName("UTF-8");
	private static final Charset US_ASCII = Charset.forName("US-ASCII");
	private static final Charset ISO_8859_1 = Charset.forName("ISO-8859-1");
	private static final Charset CP1252 = Charset.forName("CP1252");
	private final Map<SchemaId, Schema> schemata = new HashMap<>();
	private final File dataDirectory;

	public SqlDaemon(File dataDirectory) {
		this.dataDirectory = dataDirectory;
	}

	public void readSchemasAndTablesFromDisk() throws SQLException {
		File[] schemas = dataDirectory.listFiles();
		if (schemas != null) {
			Parser parser = new AntlrSqlParserWrapper();
			for (File schema : schemas) {
				SchemaId schemaId = new SchemaId(schema.getName());
				addSchema(schemaId);
				File[] tables = schema.listFiles();
				if (tables != null) {
					for (File table : tables) {
						try {
							File sqlFile = new File(table, "table.sql");
							String sql = new String(Files.readAllBytes(sqlFile.toPath()));
							CreateTable createTable = parser.parse(sql);
							createTable(createTable, schemaId, false);
						} catch (IOException e) {
							throw new SQLException("Could not load stored sql from disk for table " + table.getAbsolutePath(), e);
						}
					}
				}
			}
		}
	}

	@Override
	public void executeStructureChange(DropTable dropTable, ConnectionContext connectionContext) throws SQLException {
		TableReference table = dropTable.getTableReference();
		SchemaId schemaId = table.getSchemaId().orElse(connectionContext.getSchema());
		Schema schema = safeGetSchema(schemaId);
		Table removedTable = schema.removeTable(table.getTableId());
		if (removedTable == null) {
			throw new SQLException("Unknown table: " + table.getTableId());
		}
		removedTable.storage().dispose();
		File tableStructureDirectory = new File(new File(dataDirectory, schemaId.toString()), table.getTableId().toString());
		Util.recursivelyWipeDirectory(tableStructureDirectory);
	}

	@Override
	public void executeStructureChange(DropSchema dropSchema, ConnectionContext connectionContext) throws SQLException {
		safeGetSchema(dropSchema.getSchemaId()); // validate that the schema exists
		Schema removedSchema = schemata.remove(dropSchema.getSchemaId());
		removedSchema.getTables().values().forEach(table -> table.storage().dispose());
		File schemaDirectory = new File(dataDirectory, dropSchema.getSchemaId().toString());
		Util.recursivelyWipeDirectory(schemaDirectory);
	}

	@Override
	public void executeStructureChange(AlterTable alterTable, ConnectionContext connectionContext) throws SQLException {
		SchemaId schemaId = alterTable.getTableReference().getSchemaId().orElse(connectionContext.getSchema());
		TableId tableId = alterTable.getTableReference().getTableId();
		Table table = getTable(schemaId, tableId);
		for (TableAlteration tableAlteration : alterTable.getTableAlterations()) {
			// todo: validate all alterations first, including things like seeing if changing a whole table works, etc really? is the entire ALTER statement atomic? Can't be!
			tableAlteration.apply(table);
		}
		storeTableStructure(table);
	}

	private void storeTableStructure(Table table) throws SQLException {
		File file = new File(new File(new File(dataDirectory, table.getSchemaId().toString()), table.getId().toString()),
		                     "table.sql");
		StringBuilder sb = new StringBuilder();
		sb.append("create table `")
		  .append(table.getSchemaId().toString())
		  .append("`.`")
		  .append(table.getId().toString())
		  .append("` (\n");
		for (int i = 0; i < table.getColumnsInOrder().size(); i++) {
			ColumnId columnId = table.getColumnsInOrder().get(i);
			Column<?> column = table.getColumn(columnId);
			sb.append("\t");
			sb.append(column.getId()).append(' ').append(column.getType().getSqlToken());
			if (column.isNotNull()) {
				sb.append(" NOT NULL");
				if (column.getDefaultValue() != null) {
					sb.append(" DEFAULT ").append(column.getDefaultValue());
				}
			} else {
				sb.append(" DEFAULT ").append(column.getDefaultValue());
			}
			if (column.getAutoIncrementStep() > 0) {
				sb.append(" AUTO_INCREMENT ").append(column.getAutoIncrementStep());
			}
			if (column.isUnsigned()) {
				sb.append(" UNSIGNED");
			}
			if (i < table.getNumberOfColumns() - 1) {
				sb.append(", \n");
			}
		}
		for (int i = 0; i < table.getIndexes().size(); i++) {
			sb.append(", \n\t");
			Index index = table.getIndexes().get(i);
			sb.append(index.getType()).append("(");
			for (int j = 0; j < index.getColumns().size(); j++) {
				ColumnId columnId = index.getColumns().get(j);
				sb.append(columnId.toString());
				if (j < index.getColumns().size() - 1) {
					sb.append(", ");
				}
			}
			sb.append(")");
		}
		// todo: add table constraints

		sb.append("\n)");
		sb.append(" ENGINE ").append(table.storage() instanceof MemoryTableStorage ? "Memory" : "TablePerFile");
		try {
			if (file.getParentFile().exists() || file.getParentFile().mkdirs()) {
				Files.write(file.toPath(),
				            sb.toString().getBytes(),
				            StandardOpenOption.WRITE,
				            StandardOpenOption.CREATE,
				            StandardOpenOption.TRUNCATE_EXISTING);
			} else {
				throw new SQLException("Could not create directories: " + file.getParentFile().getAbsolutePath());
			}
		} catch (IOException e) {
			// todo: this is effectively unrecoverable. We should probably shut down in this case.
			throw new SQLException("Could not store file structure on disk", e);
		}
	}

	@Override
	public void executeStructureChange(CreateTable createTable, ConnectionContext connectionContext) throws SQLException {
		createTable(createTable, connectionContext.getSchema(), true);
	}

	private void createTable(CreateTable createTable, SchemaId defaultSchema, boolean writeToDisk) throws SQLException {
		// validate first, so we don't end up with some half-accepted state:
		for (Column<?> column : createTable.getColumns()) {
			ColumnType<?> columnType = column.getType();
			if (column.getAutoIncrementStep() > 0 && (!(columnType instanceof DoubleColumnType) && !(columnType instanceof IntegerColumnType))) {
				throw new SQLException("'auto_increment' only allowed for numeric columns");
			}
		}

		TableReference table = createTable.getTable();

		SchemaId schemaId = table.getSchemaId().orElse(defaultSchema);
		Schema schema = safeGetSchema(schemaId);

		// todo: validate all the constraints BEFORE adding them (to avoid adding some but not others)
		// todo: this is actually required for EVERYTHING, ALWAYS.
		// do first, then effectively commit the change.
		// it's not just for transactions, but it'll help us with them later

		// create the structure
		// NOTE: none of these things below here are allowed to throw, as then we'll end up with things half-added!
		TableStorage tableStorage;
		if (createTable.getStorageType() == TableStorage.Type.Memory) {
			tableStorage = new MemoryTableStorage();
		} else if (createTable.getStorageType() == TableStorage.Type.TablePerFile) {
			// todo. we have to either provide the table right away, or read the rows after we're set the table, to avoid a NullPointerException
			tableStorage = new VariableWidthTablePerFileTableStorage(dataDirectory, schemaId, table.getTableId(), UTF_8);
		} else {
			throw new SQLException("Unknown storage engine: " + createTable.getStorageType());
		}
		Table tableInSchema = schema.addTable(table.getTableId(), tableStorage);
		createTable.getColumns().forEach(tableInSchema::addColumn);
		for (ColumnConstraint columnConstraint : createTable.getConstraints()) {
			tableInSchema.addConstraint(columnConstraint);
		}
		tableStorage.setTable(tableInSchema);
		tableStorage.start();
		if (writeToDisk) {
			storeTableStructure(tableInSchema);
		}
	}

	@Override
	public void executeStructureChange(CreateSchema createSchema, ConnectionContext connectionContext) throws SQLException {
		File schemaDirectory = new File(dataDirectory, createSchema.getSchemaId().toString());
		if (schemaDirectory.exists()) {
			throw new SQLException("Schema directory already exists on disk: " + schemaDirectory.getAbsolutePath());
		}
		if (schemaDirectory.mkdirs()) {
			addSchema(createSchema.getSchemaId());
		} else {
			throw new SQLException("Could not create schema directory on disk: " + schemaDirectory.getAbsolutePath());
		}
	}

	// todo: do things like validate max length of fields in DDL.

	@Override
	public int executeUpdate(Insert insert, ConnectionContext connectionContext) throws SQLException {
		SchemaId schemaId = insert.getTableReference().getSchemaId().orElse(connectionContext.getSchema());
		TableId tableId = insert.getTableReference().getTableId();

		Table table = getTable(schemaId, tableId);

		List<ColumnId> remainingColumns;
		List<ColumnId> statementColumns;


		if (!insert.getColumns().isEmpty()) {
			statementColumns = insert.getColumns();
			remainingColumns = new ArrayList<>(table.getColumns().keySet());
			remainingColumns.removeAll(statementColumns);
		} else {
			// todo: validate that we have the correct number of values to insert
			statementColumns = new ArrayList<>(table.getColumnsInOrder());
			remainingColumns = new ArrayList<>();
			// skip auto_increment columns here in the statementColumns
			// todo: this is super-hacky. Make this nicer and more elegant!
			for (Column<?> column : table.getColumns().values()) {
				if (column.getAutoIncrementStep() > 0) {
					statementColumns.remove(column.getId());
					remainingColumns.add(column.getId());
				}
			}
		}


		Column<?>[] statementColumnsInDataOrder = new Column<?>[statementColumns.size()];
		int[] statementFinalRowIndexesInDataOrder = new int[statementColumns.size()];
		for (int i = 0; i < statementColumns.size(); i++) {
			ColumnId columnId = statementColumns.get(i);
			statementFinalRowIndexesInDataOrder[i] = table.indexOf(columnId);
			Column<?> column = table.getColumn(columnId);
			statementColumnsInDataOrder[i] = column;

			if (column.getAutoIncrementStep() > 0) {
				throw new SQLException("Can't explicitly insert values into column with AUTO_INCREMENT. Offending column: " + column
						.getId());
			}
		}


		Column<?>[] remainingColumnsInDataOrder = new Column<?>[remainingColumns.size()];
		int[] remainingFinalRowIndexesInDataOrder = new int[remainingColumns.size()];
		for (int i = 0; i < remainingColumns.size(); i++) {
			ColumnId columnId = remainingColumns.get(i);
			remainingFinalRowIndexesInDataOrder[i] = table.indexOf(columnId);
			remainingColumnsInDataOrder[i] = table.getColumn(columnId);
		}

		// todo: support CHECK conditions
		// todo: support FOREIGN KEY conditions
		// todo: support combined PRIMARY KEY constraints (i.e when PRIMARY KEY is defined on multiple columns, this must be joined to a single multi-column key)
		// todo: support combined UNIQUE constraints (i.e when UNIQUE is defined on multiple columns, this must be joined to a single multi-column key)

		List<Row> rows = new ArrayList<>();
		// validate and potentially convert values.
		// this needs to be done BEFORE we add things to the table storage, 
		// so that we don't end up with partial updates
		long lastGeneratedId = 0;
		for (Object[] objects : insert.getValues()) {
			Object[] row = new Object[table.getNumberOfColumns()];

			if (objects.length != statementColumns.size()) {
				// todo: add unit test for this condition
				throw new SQLException(String.format(
						"INSERT tuple %s doesn't contain the correct number of values. Should contain %d but contains %d",
						Arrays.toString(objects),
						statementColumns.size(),
						objects.length));
			}
			for (int i = 0; i < objects.length; i++) {
				Object value = objects[i];

				Column<?> column = statementColumnsInDataOrder[i];
				value = convertIfNecessary(value, column);

				int rowIndex = statementFinalRowIndexesInDataOrder[i];
				row[rowIndex] = value;
			}
			for (int i = 0; i < remainingColumns.size(); i++) {
				Column<?> column = remainingColumnsInDataOrder[i];
				Object value = column.getDefaultValue();
				if (column.getAutoIncrementStep() > 0) {
					lastGeneratedId = table.storage().autoIncrement(column.getAutoIncrementStep());
					// since the generated id is a long, we'll have to modify it to fit.
					value = convertIfNecessary(lastGeneratedId, column);
				}
				int rowIndex = remainingFinalRowIndexesInDataOrder[i];
				row[rowIndex] = value;
			}
			Row memoryRow = new MemoryRow(row);
			rows.add(memoryRow);
		}
		connectionContext.setLastGeneratedId(lastGeneratedId);
		// only once we reach here do we know that all is well, and the validation has passed, 
		// and we can safely add the rows 

		table.storage().addRows(rows);
		return rows.size();
	}

	/**
	 * Checks if the values needs to be converted. If not, returns the object.
	 * If the object needs to be converted, it'll be converted if possible.
	 * If the object can not be converted, an exception is thrown.
	 *
	 * @param value  the value
	 * @param column the column specifies how, if necessary, the value should be converted
	 * @return a value that fits the column
	 * @throws SQLException if the value could not be converted
	 */
	private <T> T convertIfNecessary(Object value, Column<T> column) throws SQLException {
		return column.getType().convertAndAdjust(value);
	}

	@Override
	public int executeUpdate(Update update, ConnectionContext connectionContext) throws SQLException {
		SchemaId schemaId = update.getTableReference().getSchemaId().orElse(connectionContext.getSchema());
		TableId tableId = update.getTableReference().getTableId();

		ColumnResolver columnResolver = new SingleTableColumnResolver(connectionContext.getSchema(), tableId);
		Table table = getTable(schemaId, tableId);
		IndexedConditionTreeBuilder treeBuilder =
				new IndexedConditionTreeBuilder(table.getColumnsToIndexes(), update.getConditions(), columnResolver);
		IndexedConditionTreeFetcher fetcher = treeBuilder.createFetcher(table);
		Set<RowReference> rows = fetcher.fetchRows();

		List<Row> changedRows = new ArrayList<>();
		List<RowReference> changedRowIndexes = new ArrayList<>();
		for (RowReference rowReference : rows) {
			Row row = rowReference.getRow();
			// todo: is this the most efficient way?
			Row clonedRow = new MemoryRow(Arrays.copyOf(row.getValues(), row.getValues().length));
			changedRows.add(clonedRow);
			changedRowIndexes.add(rowReference);
			for (FieldUpdate fieldUpdate : update.getUpdates()) {
				clonedRow.setValue(table.indexOf(fieldUpdate.getColumnReference().getColumnId()), fieldUpdate.getValue());
			}
		}
		table.storage().updateRows(changedRows, changedRowIndexes);
		return changedRows.size();
	}

	@Override
	public int executeUpdate(Delete delete, ConnectionContext connectionContext) throws SQLException {
		SchemaId schemaId = delete.getTableReference().getSchemaId().orElse(connectionContext.getSchema());
		TableId tableId = delete.getTableReference().getTableId();

		ColumnResolver columnResolver = new SingleTableColumnResolver(connectionContext.getSchema(), tableId);
		Table table = getTable(schemaId, tableId);
		IndexedConditionTreeBuilder treeBuilder =
				new IndexedConditionTreeBuilder(table.getColumnsToIndexes(), delete.getConditions(), columnResolver);
		IndexedConditionTreeFetcher fetcher = treeBuilder.createFetcher(table);
		Set<RowReference> rows = fetcher.fetchRows();
		table.storage().deleteRows(rows);
		return rows.size();
	}

	@Override
	public ResultSet executeQuery(Select select, ConnectionContext connectionContext) throws SQLException {
		if (select instanceof SelectLastInsertId) {
			ResolvedColumnReference resolvedColumnReference = new ResolvedColumnReference(null, null);
			return new RowResultSet(Collections.singletonMap(resolvedColumnReference, 0), new ColumnResolver() {
				@Override
				public ResolvedColumnReference resolve(String column) throws SQLException {
					return resolvedColumnReference;
				}

				@Override
				public ResolvedColumnReference resolve(ColumnReference columnReference) {
					return resolvedColumnReference;
				}
			}, Collections.singletonList(new MemoryRow(new Object[]{connectionContext.getLastGeneratedId()})));
		}
		final SchemaId defaultSchemaId = connectionContext.getSchema();

		LinkedHashMap<ResolvedColumnReference, Integer> columnsToIndexes = new LinkedHashMap<>();
		// resolve all tables and their aliases
		List<Table> tablesInQuery = new ArrayList<>();
		// note that the ALIASES are simply names that REFERENCE tables.
		// thus, as long as they are unique to the QUERY, we're fine
		final Map<TableId, Table> tableAliases = new HashMap<>();
		resolveTable(defaultSchemaId, tablesInQuery, tableAliases, select.getTable());
		List<Join.Type> joinTypes = new ArrayList<>();
		for (Join join : select.getJoins()) {
			resolveTable(defaultSchemaId, tablesInQuery, tableAliases, join.getTableReference());
			joinTypes.add(join.getType());
		}

		// resolve all output columns
		List<ResolvedColumnReference> outputColumns = new ArrayList<>();
		Map<ColumnId, ResolvedColumnReference> outputColumnAliases = new HashMap<>();
		Map<ColumnReference, ResolvedColumnReference> resolvedColumnReferences = new HashMap<>();
		for (ColumnReference columnReference : select.getColumns()) {
			if (columnReference.isStar()) {
				if (columnReference.getTableId().isPresent()) {
					// create resolved columns for all columns in this table
					SchemaId schemaId = columnReference.getSchemaId().orElse(defaultSchemaId);
					TableId tableId = columnReference.getTableId().get();
					Table table = getTable(schemaId, tableId);
					for (ColumnId columnId : table.getColumnsInOrder()) {
						ResolvedColumnReference resolvedColumnReference =
								new ResolvedColumnReference(new ResolvedTableReference(defaultSchemaId, table.getId()), columnId);
						outputColumns.add(resolvedColumnReference);
						columnsToIndexes.putIfAbsent(resolvedColumnReference, columnsToIndexes.size());
					}
				} else {
					// create resolved columns for all columns in ALL tables
					for (Table table : tablesInQuery) {
						for (ColumnId columnId : table.getColumnsInOrder()) {
							ResolvedColumnReference resolvedColumnReference =
									new ResolvedColumnReference(new ResolvedTableReference(table.getSchemaId(), table.getId()),
									                            columnId);
							outputColumns.add(resolvedColumnReference);
							columnsToIndexes.putIfAbsent(resolvedColumnReference, columnsToIndexes.size());
						}
					}
				}
			} else {
				ResolvedColumnReference resolvedColumnReference = resolveColumnAndAlias(columnsToIndexes,
				                                                                        Optional.of(outputColumnAliases),
				                                                                        columnReference,
				                                                                        tablesInQuery,
				                                                                        resolvedColumnReferences,
				                                                                        tableAliases);
				outputColumns.add(resolvedColumnReference);
			}
		}

		// resolve all condition columns
		Optional<ConditionTree> conditions = select.getConditions();
		if (conditions.isPresent()) {
			resolveConditionColumns(conditions.get(), columnsToIndexes, tablesInQuery, resolvedColumnReferences, tableAliases);
		}
		for (OrderBy orderBy : select.getOrdering()) {
			resolveColumnAndAlias(columnsToIndexes,
			                      Optional.of(outputColumnAliases),
			                      orderBy.getColumn(),
			                      tablesInQuery,
			                      resolvedColumnReferences,
			                      tableAliases);
		}

		List<Row> matchingRows;
		ColumnResolver columnResolver = new ColumnResolver() {
			@Override
			public ResolvedColumnReference resolve(String column) throws SQLException {
				ColumnReference columnReference = ColumnReference.parse(column);
				if (columnReference.getAliasId().isPresent()) {
					// they can REFERENCE aliases, but they can't HAVE aliases.
					// you can't, for instance, have something like "where schema.table.column as name = 12".
					throw new IllegalArgumentException("Columns used for resolution must not have aliases.");
				}
				ResolvedColumnReference resolvedColumnReference = resolvedColumnReferences.get(columnReference);
				if (resolvedColumnReference == null) {
					resolvedColumnReference = outputColumnAliases.get(new ColumnId(column));
					if (resolvedColumnReference == null) {
						// we're not referencing something that was part of the query.
						// select * from a
						// rs.getString("Name") <- this won't match anything we've resolved!
						resolvedColumnReference = resolveColumn(columnReference, tablesInQuery, tableAliases);
					}
				}
				return resolvedColumnReference;
			}

			@Override
			public ResolvedColumnReference resolve(ColumnReference columnReference) {
				ResolvedColumnReference resolvedColumnReference = resolvedColumnReferences.get(columnReference);
				if (resolvedColumnReference == null) {
					// todo: this fails if we reference a column (in order-by, but likely everywhere) when we've selected *
					// this should never happen as this method is only used for internal things
					throw new IllegalStateException("Encountered column that should have been resolved! " + columnReference);
				}
				return resolvedColumnReference;
			}
		};

		if (select.getJoins().isEmpty()) {
			TableReference tableReference = select.getTable();
			SchemaId schemaId = tableReference.getSchemaId().orElse(defaultSchemaId);
			TableId tableId = tableReference.getTableId();

			Table table = getTable(schemaId, tableId);
			IndexedConditionTreeBuilder treeBuilder =
					new IndexedConditionTreeBuilder(table.getColumnsToIndexes(), select.getConditions(), columnResolver);
			IndexedConditionTreeFetcher fetcher = treeBuilder.createFetcher(table);
			Set<RowReference> rows = fetcher.fetchRows();

			matchingRows = rows.stream().map(RowReference::getRow).collect(Collectors.toList());
			sort(columnResolver, select.getOrdering(), matchingRows, table.getColumnsToIndexes());
			if (select.getLimit().isPresent()) {
				matchingRows = matchingRows.stream().limit(select.getLimit().get()).collect(Collectors.toList());
			}
		} else {
			int totalWidth = columnsToIndexes.size();

			matchingRows = new ArrayList<>();
			Set<Table> applicableTables = new HashSet<>(); // todo: how will this handle self joins?

			for (int i = 0; i < tablesInQuery.size(); i++) {
				Table table = tablesInQuery.get(i);
				// adding to the applicable tables means that more of the condition tree is taken into account
				applicableTables.add(table);

				// filter the rows from the table first so that we get less data to work with during the join
				List<Row> filteredRows = filter(conditions, columnResolver, table, columnsToIndexes);
				if (i > 0) {
					// there will be 1 less join types than there will be tables, as we're not joining 
					// the first table with anything
					Join.Type joinType = joinTypes.get(i - 1);
					matchingRows = join(matchingRows,
					                    filteredRows,
					                    conditions,
					                    applicableTables,
					                    joinType,
					                    columnResolver,
					                    totalWidth,
					                    columnsToIndexes);
				} else {
					// add the rows from the base table to the result
					matchingRows.addAll(filteredRows);
				}
			}
			sort(columnResolver, select.getOrdering(), matchingRows, columnsToIndexes);
			if (select.getLimit().isPresent()) {
				matchingRows = matchingRows.stream().limit(select.getLimit().get()).collect(Collectors.toList());
			}
		}

		// todo: trim the result set width. Only use the |outputColumn| first values.
		// todo: also add tests to ensure that we throw an exception when we go out of bounds

		return new RowResultSet(columnsToIndexes, columnResolver, matchingRows);

	}

	private IndexedOrderByComparator createOrderByComparator(LinkedHashMap<ResolvedColumnReference, Integer> columnsToIndexes,
	                                                         ColumnResolver columnResolver,
	                                                         List<OrderBy> originalOrdering) throws SQLException {
		List<IndexedOrderBy> ordering = new ArrayList<>();
		for (OrderBy orderBy : originalOrdering) {
			ResolvedColumnReference resolvedColumnReference = columnResolver.resolve(orderBy.getColumn());

			// this should be safe, as we've already gotten something that is resolved
			Integer index = columnsToIndexes.get(resolvedColumnReference);
			ordering.add(new IndexedOrderBy(index, orderBy.getDirection()));
		}
		return new IndexedOrderByComparator(ordering);
	}

	private List<Row> join(List<Row> leftRows,
	                       List<Row> rightRows,
	                       Optional<ConditionTree> conditions,
	                       Set<Table> applicableTables,
	                       Join.Type joinType,
	                       ColumnResolver columnResolver,
	                       int totalWidth,
	                       LinkedHashMap<ResolvedColumnReference, Integer> columnsToIndexes) throws SQLException {
		// NOTE: DON'T use the fetcher here.
		// The fetcher can NOT be used with join conditions.

		IndexedConditionTreeBuilder treeBuilder = new IndexedConditionTreeBuilder(columnsToIndexes, conditions, columnResolver);
		IndexedConditionTreeMatcher matcher = treeBuilder.createJoinConditionMatcher(applicableTables);
		// todo: in the future, see if we can't use indexes to speed up join-matching as well

		List<Row> mergedRows = new ArrayList<>();
		boolean[] matchedRightRows = new boolean[rightRows.size()];
		for (Row leftRow : leftRows) {
			boolean matched = false;
			for (int rightRowIndex = 0; rightRowIndex < rightRows.size(); rightRowIndex++) {
				Row rightRow = rightRows.get(rightRowIndex);
				Object[] rowData = new Object[totalWidth];

				for (int i = 0; i < totalWidth; i++) {
					Object leftValue = leftRow.getValues()[i];
					Object rightValue = rightRow.getValues()[i];

					if (leftValue == null) {
						if (rightValue == null) {
							rowData[i] = null;
						} else {
							rowData[i] = rightValue;
						}
					} else {
						rowData[i] = leftValue;
					}
				}

				MemoryRow memoryRow = new MemoryRow(rowData);
				if (matcher.matches(memoryRow)) {
					matched = true;
					mergedRows.add(memoryRow);
					matchedRightRows[rightRowIndex] = true;
				}
			}
			if (!matched && (joinType == Join.Type.Left || joinType == Join.Type.Full)) {
				// in a left join, the data from the left table row should be included regardless
				// of if there was a join match or not
				MemoryRow partialRow = new MemoryRow(leftRow.getValues());
				mergedRows.add(partialRow);
			}
		}
		if (joinType == Join.Type.Right || joinType == Join.Type.Full) {
			for (int i = 0; i < matchedRightRows.length; i++) {
				boolean rowWasMatched = matchedRightRows[i];
				if (!rowWasMatched) {
					// in a right join, the data from the right table row should be included regardless
					// of if there was a join match or not
					MemoryRow partialRow = new MemoryRow(rightRows.get(i).getValues());
					mergedRows.add(partialRow);
				}
			}
		}


		return mergedRows;
	}

	private List<Row> filter(Optional<ConditionTree> conditions,
	                         ColumnResolver columnResolver,
	                         Table table,
	                         LinkedHashMap<ResolvedColumnReference, Integer> columnsToIndexes) throws SQLException {
		IndexedConditionTreeBuilder treeBuilder = new IndexedConditionTreeBuilder(columnsToIndexes, conditions, columnResolver);
		IndexedConditionTreeFetcher fetcher = treeBuilder.createFetcher(table);
		Set<RowReference> rows = fetcher.fetchRows();

		List<Row> filteredRows = new ArrayList<>();
		for (RowReference rowReference : rows) {

			// filter the data based on columnsToIndexes, create a new row, and add THAT to the list.
			// if we don't create a new row, and just return the input row, we can't be sure of the
			// offsets and the indexes in the final result will be wrong

			Object[] rowData = new Object[columnsToIndexes.size()];
			// todo: see if we can't make some kind of "mapper" out of this so we don't have to resolve things for each row.
			for (Map.Entry<ResolvedColumnReference, Integer> entry : columnsToIndexes.entrySet()) {
				ResolvedColumnReference resolvedColumnReference = entry.getKey();
				int index = entry.getValue();
				if (table.containsColumn(resolvedColumnReference)) {
					// only add data for columns that belong to this particular table
					rowData[index] = rowReference.getRow().getValueAt(table.indexOf(resolvedColumnReference.getColumnId()));
				}
			}
			MemoryRow memoryRow = new MemoryRow(rowData);
			filteredRows.add(memoryRow);
		}
		return filteredRows;
	}

	private void resolveConditionColumns(ConditionTree conditions,
	                                     LinkedHashMap<ResolvedColumnReference, Integer> columnsToIndexes,
	                                     List<Table> tablesInQuery,
	                                     Map<ColumnReference, ResolvedColumnReference> columnReferencesMap,
	                                     Map<TableId, Table> tableAliases) throws SQLException {
		if (conditions.getCondition().isPresent()) {
			Condition condition = conditions.getCondition().get();
			if (condition instanceof JoinCondition) {
				JoinCondition jc = (JoinCondition) condition;
				resolveColumnAndAlias(columnsToIndexes,
				                      Optional.<Map<ColumnId, ResolvedColumnReference>>empty(),
				                      jc.getLeft(),
				                      tablesInQuery,
				                      columnReferencesMap,
				                      tableAliases);
				resolveColumnAndAlias(columnsToIndexes,
				                      Optional.<Map<ColumnId, ResolvedColumnReference>>empty(),
				                      jc.getRight(),
				                      tablesInQuery,
				                      columnReferencesMap,
				                      tableAliases);
			} else if (condition instanceof ColumnCondition) {
				ColumnCondition cc = (ColumnCondition) condition;
				resolveColumnAndAlias(columnsToIndexes,
				                      Optional.<Map<ColumnId, ResolvedColumnReference>>empty(),
				                      cc.getColumn(),
				                      tablesInQuery,
				                      columnReferencesMap,
				                      tableAliases);

			} // else it's something static which doesn't require column resolution
		} else {
			// if it's not a condition, we know it's a tree node
			resolveConditionColumns(conditions.getLeft().get(),
			                        columnsToIndexes,
			                        tablesInQuery,
			                        columnReferencesMap,
			                        tableAliases);
			resolveConditionColumns(conditions.getRight().get(),
			                        columnsToIndexes,
			                        tablesInQuery,
			                        columnReferencesMap,
			                        tableAliases);
		}

	}

	private void resolveTable(SchemaId defaultSchemaId,
	                          List<Table> tables,
	                          Map<TableId, Table> aliases,
	                          TableReference tableReference) throws SQLException {
		SchemaId schemaId = tableReference.getSchemaId().orElse(defaultSchemaId);
		TableId tableId = tableReference.getTableId();
		Table table = getTable(schemaId, tableId);
		tables.add(table);

		Optional<TableId> potentialAliasId = tableReference.getAliasId();
		if (potentialAliasId.isPresent()) {
			TableId aliasId = potentialAliasId.get();
			Table existingValueForAlias = aliases.put(aliasId, table);
			if (existingValueForAlias != null) {
				throw new SQLException(String.format("Table alias '%s' is not unique in the query.", aliasId));
			}
		}
	}

	private ResolvedColumnReference resolveColumnAndAlias(LinkedHashMap<ResolvedColumnReference, Integer> columnsToIndexes,
	                                                      Optional<Map<ColumnId, ResolvedColumnReference>> aliases,
	                                                      ColumnReference columnReference,
	                                                      List<Table> tablesInQuery,
	                                                      Map<ColumnReference, ResolvedColumnReference> columnReferencesMap,
	                                                      Map<TableId, Table> tableAliases) throws SQLException {
		ResolvedColumnReference resolvedColumnReference = resolveColumn(columnReference, tablesInQuery, tableAliases);
		columnsToIndexes.putIfAbsent(resolvedColumnReference, columnsToIndexes.size());

		columnReferencesMap.put(columnReference, resolvedColumnReference);

		if (aliases.isPresent()) {
			Map<ColumnId, ResolvedColumnReference> aliasMap = aliases.get();
			Optional<ColumnId> potentialAliasId = columnReference.getAliasId();
			if (potentialAliasId.isPresent()) {
				ColumnId aliasId = potentialAliasId.get();
				ResolvedColumnReference existingValueForAlias = aliasMap.put(aliasId, resolvedColumnReference);
				if (existingValueForAlias != null) {
					throw new SQLException(String.format("Column alias '%s' is not unique in the query.", aliasId));
				}
			}
		}
		return resolvedColumnReference;
	}

	private ResolvedColumnReference resolveColumn(ColumnReference columnReference,
	                                              List<Table> tablesInQuery,
	                                              Map<TableId, Table> tableAliases) throws SQLException {
		final ColumnId columnId = columnReference.getColumnId();


		Table table = null;
		Optional<TableId> potentialTableId = columnReference.getTableId();
		if (potentialTableId.isPresent()) {
			Table potentialTable;
			potentialTable = tableAliases.get(potentialTableId.get());
			if (potentialTable != null) {
				table = potentialTable;
			} else {
				for (Table tableInQuery : tablesInQuery) {
					// todo: this could be done using a map. That said, this list will only ever be a handful of items long, so it's perhaps not the end of the world if we use a list.
					if (potentialTableId.get().equals(tableInQuery.getId())) {
						table = tableInQuery;
						break;
					}
				}
			}
			if (table == null) {
				throw new SQLException(String.format("Could not resolve token '%s' to a table. Tried both table and alias",
				                                     potentialTableId.get()));
			}
			Column<?> column = table.getColumn(columnId);
			if (column == null) {
				throw new SQLException(String.format("Column reference '%s' doesn't correspond to a column in table '%s'.",
				                                     columnReference,
				                                     table.getId()));
			}
		} else {
			// find the table this column belongs to
			for (Table tableInQuery : tablesInQuery) {
				Column<?> column = tableInQuery.getColumn(columnId);
				if (column != null) {
					table = tableInQuery;
					break;
				}
			}
			if (table == null) {
				throw new SQLException(String.format(
						"Column reference '%s' doesn't correspond to a column in any of the tables participating in the query.",
						columnReference));
			}

		}
		TableId tableId = table.getId();
		SchemaId schemaId = table.getSchemaId();


		return new ResolvedColumnReference(new ResolvedTableReference(schemaId, tableId), columnId);
	}

	private void sort(ColumnResolver columnResolver,
	                  List<OrderBy> ordering,
	                  List<Row> rows,
	                  LinkedHashMap<ResolvedColumnReference, Integer> columnsToIndexes) throws SQLException {
		if (!ordering.isEmpty()) {
			Collections.sort(rows, createOrderByComparator(columnsToIndexes, columnResolver, ordering));
		}
	}

	public Table getTable(SchemaId schemaId, TableId tableId) throws SQLException {
		Table table = safeGetSchema(schemaId).getTables().get(tableId);
		if (table == null) {
			throw new SQLException(String.format("Unknown table '%s' in schema '%s'", tableId, schemaId));
		}
		return table;
	}

	private Schema addSchema(SchemaId schemaId) throws SQLException {
		Schema schema = new DefaultSchema(schemaId);
		if (schemata.put(schemaId, schema) != null) {
			throw new SQLException("Schema already exists: " + schemaId);
		}
		return schema;
	}

	public Set<SchemaId> getSchemas() {
		return schemata.keySet();
	}

	public Set<TableId> getTables(SchemaId schemaId) throws SQLException {
		Schema schema = safeGetSchema(schemaId);
		return schema.getTables().keySet();
	}

	private Schema safeGetSchema(SchemaId schemaId) throws SQLException {
		Schema schema = schemata.get(schemaId);
		if (schema == null) {
			throw new SQLException("No such schema: " + schemaId);
		}
		return schema;
	}

	private static final class IndexedOrderByComparator implements Comparator<Row> {
		private final List<IndexedOrderBy> ordering;

		private IndexedOrderByComparator(List<IndexedOrderBy> ordering) {
			this.ordering = ordering;
		}

		// we know that we can cast to Comparable here, as we have validated the type when creating this comparator
		@SuppressWarnings("unchecked")
		@Override
		public int compare(Row o1, Row o2) {
			int comparison = Integer.MIN_VALUE;
			try {
				for (IndexedOrderBy orderBy : ordering) {
					int column = orderBy.getColumn();
					Object v1 = o1.getValueAt(column);
					Object v2 = o2.getValueAt(column);

					Comparable<Object> c1 = (Comparable<Object>) v1;
					Comparable<Object> c2 = (Comparable<Object>) v2;
					if (orderBy.getDirection() == OrderBy.Direction.ASC) {
						comparison = nullCompatibleCompare(c1, c2);
					} else {
						comparison = nullCompatibleCompare(c2, c1);
					}
					if (comparison != 0) {
						// only when there is a match do we need to check the next column 
						break;
					}
				}
				return comparison;
			} catch (SQLException e) {
				// this should never happen as we've pre-verified the columns
				throw new IllegalArgumentException(e);
			}
		}

		private int nullCompatibleCompare(Comparable<Object> c1, Comparable<Object> c2) {
			// https://dev.mysql.com/doc/refman/5.0/en/working-with-null.html
			int comparison;
			if (c1 == null) {
				if (c2 == null) {
					comparison = 0;
				} else {
					comparison = -1;
				}
			} else if (c2 == null) {
				comparison = 1;
			} else {
				comparison = c1.compareTo(c2);
			}
			return comparison;
		}
	}

	private static final class SingleTableColumnResolver implements ColumnResolver {
		private final SchemaId schemaId;
		private final TableId tableId;

		public SingleTableColumnResolver(SchemaId schemaId, TableId tableId) {
			this.schemaId = schemaId;
			this.tableId = tableId;
		}

		@Override
		public ResolvedColumnReference resolve(String column) {
			ColumnReference columnReference = ColumnReference.parse(column);
			if (columnReference.getAliasId().isPresent()) {
				// they can REFERENCE aliases, but they can't HAVE aliases.
				// you can't, for instance, have something like "where schema.table.column as name = 12".
				throw new IllegalArgumentException("Columns used for resolution must not have aliases.");
			}
			return resolve(columnReference);
		}

		@Override
		public ResolvedColumnReference resolve(ColumnReference columnReference) {
			// we don't need to do any special magic here, 
			// as we don't have to deal with multiple tables and column aliases etc.
			return new ResolvedColumnReference(new ResolvedTableReference(columnReference.getSchemaId().orElse(schemaId),
			                                                              tableId), columnReference.getColumnId());
		}
	}
}
