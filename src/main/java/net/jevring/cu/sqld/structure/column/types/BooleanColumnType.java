/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.structure.column.types;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Represents boolean type columns.
 *
 * @author markus@jevring.net
 */
public class BooleanColumnType extends ColumnType<Boolean> {
	@Override
	public Boolean convertAndAdjust(Object value) throws SQLException {
		if (value == null) {
			return null;
		}
		if (value instanceof Boolean) {
			return (Boolean) value;
		} else if (value instanceof Number) {
			return ((Number) value).intValue() == 1;
		} else if (value instanceof String) {
			return Boolean.parseBoolean((String) value);
		} else {
			throw new SQLException(String.format("Can't convert value '%s' of type %s into a boolean", value, value.getClass()));
		}
	}

	@Override
	public Boolean readDataFromStorage(StorageReader storageReader) throws IOException, SQLException {
		return storageReader.readBoolean(this);
	}

	@Override
	public void writeDataToStorage(StorageWriter storageWriter, Boolean value) throws IOException, SQLException {
		storageWriter.writeBoolean(value);
	}

	@Override
	public String getSqlToken() {
		return "BOOLEAN";
	}
}
