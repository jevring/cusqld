/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.structure.index;

import net.jevring.cu.sqld.storage.Row;

/**
 * This maps row values to index values.
 * This is effectively a map that indicates which columns in a row are part
 * of the index key in question.
 *
 * @author markus@jevring.net
 * @implNote It's effectively an int array. If the value for column index {@code i}
 * is < 0, the value does not participate in the index key. Otherwise the value indicates
 * which position in the index key the column value has. For instance, {@code new int[] {-1, 0, -1, 1}}
 * indicates that, for a table with 4 columns, 2 of them participate in the index,
 * and these two are the second and the fourth.
 */
public class IndexMapping {
	private final int[] columns;
	private final int size;

	public IndexMapping(int[] columns) {
		this.columns = columns;
		int size = 0;
		for (int column : columns) {
			if (column >= 0) {
				size++;
			}
		}
		this.size = size;
	}

	public IndexKey convertToIndexKey(Row row) {
		Object[] rowData = row.getValues();
		if (rowData.length != columns.length) {
			throw new IllegalArgumentException(String.format(
					"Incompatible row provided. Expected a row with %d columns but for %d",
					columns.length,
					rowData.length));
		}
		Object[] key = new Object[size];
		for (int i = 0; i < columns.length; i++) {
			int column = columns[i];
			if (column >= 0) {
				key[column] = rowData[i];
			}
		}
		return new IndexKey(key);
	}

	public int toIndexIndex(int realIndex) { // todo: better name
		return columns[realIndex];
	}
}
