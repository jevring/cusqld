/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.structure.column.constraints;

import net.jevring.cu.sqld.statement.references.ColumnReference;
import net.jevring.cu.sqld.structure.CaseInsensitiveId;
import net.jevring.cu.sqld.structure.column.ColumnId;

import java.util.List;

/**
 * Specifies the column(s) that make up the primary key for a table.
 * If specifies individually for a column, it means that that column
 * participates in the primary key. As it can be specified on multiple
 * columns, this means that all those columns make up the primary key together.
 *
 * @author markus@jevring.net
 */
public class ForeignKey extends ColumnConstraint {
	private final List<ColumnId> columns;
	private final List<ColumnReference> primaryKeyReference;

	public ForeignKey(CaseInsensitiveId name, List<ColumnId> columns, List<ColumnReference> primaryKeyReference) {
		super(name);
		this.columns = columns;
		this.primaryKeyReference = primaryKeyReference;
	}

	public List<ColumnId> getColumns() {
		return columns;
	}

	public List<ColumnReference> getPrimaryKeyReference() {
		return primaryKeyReference;
	}
}
