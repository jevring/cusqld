/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.structure.column;

import net.jevring.cu.sqld.structure.column.types.ColumnType;

/**
 * A column in the database.
 *
 * @author markus@jevring.net
 */
public class Column<T> {
	private final ColumnId id; // todo: should this really be here?
	private final ColumnType<T> type;
	private final boolean notNull;
	private final T defaultValue;
	private final int autoIncrementStep;
	private final boolean unsigned;

	public Column(ColumnId id, ColumnType<T> type, boolean notNull, T defaultValue, int autoIncrementStep, boolean unsigned) {
		this.id = id;
		this.type = type;
		this.notNull = notNull;
		this.defaultValue = defaultValue;
		this.autoIncrementStep = autoIncrementStep;
		this.unsigned = unsigned;
	}

	public ColumnId getId() {
		return id;
	}

	public ColumnType<T> getType() {
		return type;
	}

	public boolean isNotNull() {
		return notNull;
	}

	public T getDefaultValue() {
		return defaultValue;
	}

	public int getAutoIncrementStep() {
		return autoIncrementStep;
	}

	public boolean isUnsigned() {
		return unsigned;
	}

	@Override
	public String toString() {
		return "Column{" +
				"id=" + id +
				", type=" + type +
				", notNull=" + notNull +
				", defaultValue=" + defaultValue +
				", autoIncrementStep=" + autoIncrementStep +
				", unsigned=" + unsigned +
				'}';
	}
}
