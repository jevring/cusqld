/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.structure;

import net.jevring.cu.sqld.statement.conditions.ConditionTree;
import net.jevring.cu.sqld.statement.references.ResolvedColumnReference;
import net.jevring.cu.sqld.statement.references.ResolvedTableReference;
import net.jevring.cu.sqld.storage.TableStorage;
import net.jevring.cu.sqld.structure.column.Column;
import net.jevring.cu.sqld.structure.column.ColumnChange;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.column.constraints.*;
import net.jevring.cu.sqld.structure.column.types.BooleanColumnType;
import net.jevring.cu.sqld.structure.index.Index;
import net.jevring.cu.sqld.structure.index.IndexId;
import net.jevring.cu.sqld.structure.index.IndexMapping;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Common base class for all {@link Table}s.
 *
 * @author markus@jevring.net
 */
public class DefaultTable implements Table {
	private final Map<CaseInsensitiveId, ColumnConstraint> namedConstraints = new HashMap<>();
	private final List<Index> indexes = new ArrayList<>();
	private final Map<ColumnId, Integer> columnIndexes = new HashMap<>();
	private final Map<ColumnId, Column<?>> columns = new HashMap<>();
	private final List<ColumnId> columnsInOrder = new ArrayList<>();
	private final TableId id;
	private final Schema schema;
	private final TableStorage tableStorage;
	private Optional<ConditionTree> conditions = Optional.empty();

	public DefaultTable(TableId id, Schema schema, TableStorage tableStorage) {
		this.id = id;
		this.schema = schema;
		this.tableStorage = tableStorage;
	}

	@Override
	public SchemaId getSchemaId() {
		return schema.getId();
	}

	@Override
	public void addConstraint(ColumnConstraint columnConstraint) throws SQLException {
		// todo: actually DO something about these constraints as well
		if (columnConstraint instanceof PrimaryKey) {
			PrimaryKey pk = (PrimaryKey) columnConstraint;
			Index index = createIndex(pk.getName(), pk.getColumns(), Index.Type.PrimaryKey);
			indexes.add(index);
			storage().addIndex(index);
		} else if (columnConstraint instanceof Check) {
			Check check = (Check) columnConstraint;
			// todo: check if this is even possible given the values that are in there today. if not, reject the change
			if (conditions.isPresent()) {
				this.conditions =
						Optional.of(new ConditionTree(conditions.get(), ConditionTree.Connective.And, check.getConditions()));
			} else {
				conditions = Optional.of(check.getConditions());
			}
		} else if (columnConstraint instanceof Unique) {
			Unique unique = (Unique) columnConstraint;
			Index index = createIndex(unique.getName(), unique.getColumns(), Index.Type.Unique);
			// since we don't change the rows by adding an index, we can try to add it, and only if it completely works
			// do we really register it. That way we don't have to perform the operation twice
			storage().addIndex(index);
			indexes.add(index);
		} else if (columnConstraint instanceof ForeignKey) {
			ForeignKey fk = (ForeignKey) columnConstraint;
			// todo: handle FK constraints
		} else {
			throw new IllegalArgumentException(String.format("Constraint type %s must be applied to a column, not to a table",
			                                                 columnConstraint.getClass()));
		}
		namedConstraints.put(columnConstraint.getName(), columnConstraint);
	}

	private Index createIndex(CaseInsensitiveId name, List<ColumnId> columns, Index.Type type) {
		int[] indexColumns = new int[getNumberOfColumns()];
		Arrays.fill(indexColumns, -1);
		int indexInIndex = 0;
		for (ColumnId columnId : columns) {
			int indexInTable = columnIndexes.get(columnId);
			indexColumns[indexInTable] = indexInIndex++;
		}
		IndexMapping mapping = new IndexMapping(indexColumns);
		return new Index(new IndexId(name.toString()), mapping, columns, type);
	}

	@Override
	public List<Index> getIndexes() {
		return indexes;
	}

	@Override
	public void replaceColumn(ColumnId columnId, ColumnChange columnChange) {
		columns.put(columnId, columnChange.alterColumn(columns.get(columnId)));
		// todo: remember to change the structure of all the data when this happens
	}

	// todo: when changing or drop+add a column that takes up space that was previously taken up by other data, this data needs to be cleaned!
	// for instance, dropping a boolean leaves the boolean data in the boolean field.
	// adding a new boolean would then "inherit" this data if we didn't overwrite it with the default value

	@Override
	public void dropPrimaryKey() {
		// todo: remove the index from the list.
		// todo: remove the index from the table storage
		throw new UnsupportedOperationException("Not yet implemented");
	}

	@Override
	public void dropNamedConstraint(CaseInsensitiveId constraintId) throws SQLException {
		ColumnConstraint removedConstraint = namedConstraints.remove(constraintId);
		if (removedConstraint == null) {
			throw new SQLException("Unknown constraint " + constraintId);
		}
		// todo: do we need to do anything else here?
	}

	@Override
	public void addColumn(Column<?> column) {
		ColumnId id = column.getId();
		if (columns.put(id, column) != null) {
			// todo: validate outside so that we don't end up with a half-modified table
			throw new IllegalArgumentException("Column " + id + " already exists in table " + id);
		}
		columnsInOrder.add(id);
		columnIndexes.put(id, columnIndexes.size());
		// todo: remember to change the structure of all the data when this happens
	}

	@Override
	public void removeColumn(ColumnId columnId) throws SQLException {
		if (columns.remove(columnId) == null) {
			throw new SQLException("Table " + id + " does not contain column " + columnId);
		}
		columnsInOrder.remove(columnId);
		columnIndexes.remove(columnId);
		// todo: remember to change the structure of all the data when this happens
	}

	@Override
	public boolean containsColumn(ResolvedColumnReference resolvedColumnReference) {
		boolean tableOk = getReference().equals(resolvedColumnReference.getTableReference());
		boolean columnOk = columns.containsKey(resolvedColumnReference.getColumnId());
		return tableOk && columnOk;
	}

	@Override
	public TableStorage storage() {
		return tableStorage;
	}

	@Override
	public ResolvedTableReference getReference() {
		return new ResolvedTableReference(getSchemaId(), getId());
	}

	@Override
	public List<ColumnId> getNullableColumns() {
		// todo: don't calculate this for each invocation (fix this if everything works)
		return columnsInOrder.stream().filter(columnId -> !columns.get(columnId).isNotNull()).collect(Collectors.toList());
	}

	@Override
	public List<ColumnId> getBooleanColumns() {
		// todo: don't calculate this for each invocation (fix this if everything works)
		return columnsInOrder.stream()
		                     .filter(columnId -> columns.get(columnId).getType() instanceof BooleanColumnType)
		                     .collect(Collectors.toList());
	}

	@Override
	public Map<ColumnId, Column<?>> getColumns() {
		return Collections.unmodifiableMap(columns);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> Column<T> getColumn(ColumnId columnId) {
		// we suppress warnings as this is a pattern that can never work without casting
		return (Column<T>) columns.get(columnId);
	}

	@Override
	public TableId getId() {
		return id;
	}

	@Override
	public List<ColumnId> getColumnsInOrder() {
		return columnsInOrder;
	}

	@Override
	public int indexOf(ColumnId columnId) {
		return columnIndexes.getOrDefault(columnId, -1);
	}

	@Override
	public int getNumberOfColumns() {
		return columns.size();
	}

	@Override
	public LinkedHashMap<ResolvedColumnReference, Integer> getColumnsToIndexes() {
		// todo: this isn't super elegant. We should probably do this better
		// do it whenever we update the table, rather than whenever we need to call this.
		// this data rarely changes...
		LinkedHashMap<ResolvedColumnReference, Integer> columnsToIndexes = new LinkedHashMap<>();
		for (Map.Entry<ColumnId, Integer> entry : columnIndexes.entrySet()) {
			ColumnId columnId = entry.getKey();
			Integer index = entry.getValue();
			columnsToIndexes.put(new ResolvedColumnReference(new ResolvedTableReference(getSchemaId(), id), columnId), index);
		}
		return columnsToIndexes;
	}

	@Override
	public String toString() {
		return "Table{" + id + '}';
	}
}
