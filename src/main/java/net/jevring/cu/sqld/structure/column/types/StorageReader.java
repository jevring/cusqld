/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.structure.column.types;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public interface StorageReader {
	// todo: make these for all types, then use it via the column type.
	public Double readDouble(DoubleColumnType columnType) throws IOException, SQLException;

	public Float readFloat(FloatColumnType columnType) throws IOException, SQLException;

	public Byte readByte(ByteColumnType columnType) throws IOException, SQLException;

	public Short readShort(ShortColumnType columnType) throws IOException, SQLException;

	public Integer readInteger(IntegerColumnType columnType) throws IOException, SQLException;

	public Long readLong(LongColumnType columnType) throws IOException, SQLException;

	public BigDecimal readBigDecimal(BigDecimalColumnType columnType) throws IOException, SQLException;

	public String readChar(CharColumnType columnType) throws IOException, SQLException;

	public String readVarChar(VarCharColumnType columnType) throws IOException, SQLException;

	public Boolean readBoolean(BooleanColumnType columnType) throws IOException, SQLException;

	public String readText(TextColumnType columnType) throws IOException, SQLException;
}
