/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.structure.column.types;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class DateColumnType extends ColumnType<LocalDate> {
	@Override
	public LocalDate convertAndAdjust(Object value) throws SQLException {
		if (value == null) {
			return null;
		}
		if (value instanceof LocalDate) {
			return (LocalDate) value;
		} else if (value instanceof String) {
			try {
				// todo: which formats do we support?
				return LocalDate.parse((String) value, DateTimeFormatter.BASIC_ISO_DATE);
			} catch (NumberFormatException e) {
				throw new SQLException(String.format("Value '%s' could not be converted to a LocalDate", value));
			}
		} else {
			throw new SQLException(String.format("Value '%s' of type '%s' not compatible with fields of type LocalDate",
			                                     value,
			                                     value.getClass()));
		}
	}

	@Override
	public LocalDate readDataFromStorage(StorageReader storageReader) throws IOException, SQLException {
		// todo: implement DateColumnType.readDataFromStorage()
		throw new UnsupportedOperationException("DateColumnType.readDataFromStorage() not yet implemented");
	}

	@Override
	public void writeDataToStorage(StorageWriter storageWriter, LocalDate value) throws IOException, SQLException {
		// todo: implement DateColumnType.writeDataToStorage()
		throw new UnsupportedOperationException("DateColumnType.writeDataToStorage() not yet implemented");
	}

	@Override
	public String getSqlToken() {
		return "DATE()";
	}
}
