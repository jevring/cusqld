/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.structure;

import net.jevring.cu.sqld.statement.references.ResolvedColumnReference;
import net.jevring.cu.sqld.statement.references.ResolvedTableReference;
import net.jevring.cu.sqld.storage.TableStorage;
import net.jevring.cu.sqld.structure.column.Column;
import net.jevring.cu.sqld.structure.column.ColumnChange;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.column.constraints.ColumnConstraint;
import net.jevring.cu.sqld.structure.index.Index;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * A table contains {@link Column}s.
 *
 * @author markus@jevring.net
 */
public interface Table {
	void addColumn(Column<?> column);

	void removeColumn(ColumnId columnId) throws SQLException;

	Map<ColumnId, Column<?>> getColumns();

	public <T> Column<T> getColumn(ColumnId columnId);

	TableId getId();

	List<ColumnId> getColumnsInOrder();

	int indexOf(ColumnId columnId);

	int getNumberOfColumns();

	SchemaId getSchemaId();

	LinkedHashMap<ResolvedColumnReference, Integer> getColumnsToIndexes();

	void addConstraint(ColumnConstraint columnConstraint) throws SQLException;

	List<Index> getIndexes();

	void replaceColumn(ColumnId columnId, ColumnChange columnChange);

	void dropPrimaryKey();

	void dropNamedConstraint(CaseInsensitiveId constraintId) throws SQLException;

	boolean containsColumn(ResolvedColumnReference resolvedColumnReference);

	TableStorage storage();

	ResolvedTableReference getReference();

	List<ColumnId> getNullableColumns();

	List<ColumnId> getBooleanColumns();
}
