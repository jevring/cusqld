/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.structure.column.types;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Set;

/**
 * <a href="https://dev.mysql.com/doc/refman/5.0/en/set.html">https://dev.mysql.com/doc/refman/5.0/en/set.html</a>
 *
 * @author markus@jevring.net
 */
public class SetColumnType extends ColumnType<Object> {
	private final Set<Object> allowedValues;

	public SetColumnType(Set<Object> allowedValues) {
		super();
		this.allowedValues = allowedValues;
	}

	public Set<Object> getAllowedValues() {
		return allowedValues;
	}

	@Override
	public Object convertAndAdjust(Object value) throws SQLException {
		// todo: fix this when we know more
		throw new SQLException("Sets aren't really supported yet. Check back later");
	}

	@Override
	public Object readDataFromStorage(StorageReader storageReader) throws IOException {
		// todo: implement SetColumnType.readDataFromStorage()
		throw new UnsupportedOperationException("SetColumnType.readDataFromStorage() not yet implemented");
	}

	@Override
	public void writeDataToStorage(StorageWriter storageWriter, Object value) throws IOException, SQLException {
		// todo: implement SetColumnType.writeDataToStorage()
		throw new UnsupportedOperationException("SetColumnType.writeDataToStorage() not yet implemented");
	}

	@Override
	public String getSqlToken() {
		StringBuilder sb = new StringBuilder();
		sb.append("SET(");
		Iterator<Object> iterator = allowedValues.iterator();
		while (iterator.hasNext()) {
			Object o = iterator.next();
			// todo: quotation marks around these if they are strings.
			sb.append(o);
			if (iterator.hasNext()) {
				sb.append(", ");
			}
		}
		sb.append(")");
		return sb.toString();
	}
}
