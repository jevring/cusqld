/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.structure;

import net.jevring.cu.sqld.storage.TableStorage;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Common base class for all {@link Schema}s
 *
 * @author markus@jevring.net
 */
public class DefaultSchema implements Schema {
	private final Map<TableId, Table> tables = new HashMap<>();
	private final SchemaId id;

	public DefaultSchema(SchemaId id) {
		this.id = id;
	}

	@Override
	public Table addTable(TableId tableId, TableStorage tableStorage) {
		Table table = new DefaultTable(tableId, this, tableStorage);
		tables.put(tableId, table);
		return table;
	}

	@Override
	public Table removeTable(TableId tableId) {
		return tables.remove(tableId);
	}

	@Override
	public Map<TableId, Table> getTables() {
		return Collections.unmodifiableMap(tables);
	}

	@Override
	public SchemaId getId() {
		return id;
	}

	@Override
	public String toString() {
		return "AbstractSchema{" +
				"id=" + id +
				'}';
	}
}
