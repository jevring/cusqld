/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.structure.column.types;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Represents a decimal value column.
 *
 * @author markus@jevring.net
 */
public class DoubleColumnType extends ColumnType<Double> {
	private final int totalLength;
	private final int decimalLength;

	public DoubleColumnType(int totalLength, int decimalLength) {
		this.totalLength = totalLength;
		this.decimalLength = decimalLength;
	}

	public int getTotalLength() {
		return totalLength;
	}

	public int getDecimalLength() {
		return decimalLength;
	}

	@Override
	public Double convertAndAdjust(Object value) throws SQLException {
		if (value == null) {
			return null;
		}

		Number n;
		if (value instanceof Number) {
			n = (Number) value;
		} else if (value instanceof String) {
			try {
				n = Double.parseDouble((String) value);
			} catch (NumberFormatException e) {
				throw new SQLException(String.format("Value '%s' could not be converted to a Double", value));
			}
		} else {
			throw new SQLException(String.format("Value '%s' of type '%s' not compatible with fields of type Double",
			                                     value,
			                                     value.getClass()));
		}
		return n.doubleValue();
	}

	private Number treat(Number number) {
		// todo: this also needs to handle things like DECIMAL(5,9) which adjusts the data we actually store.
		// not the space is takes, but the value.
		// it's not the STORAGE that is being described, but the TREATMENT
		// see "numeric datatypes" here: https://docs.oracle.com/cd/B28359_01/server.111/b28318/datatype.htm#CNCPT1834

		return number;
	}

	@Override
	public Double readDataFromStorage(StorageReader storageReader) throws IOException, SQLException {
		return storageReader.readDouble(this);
	}

	@Override
	public void writeDataToStorage(StorageWriter storageWriter, Double value) throws IOException, SQLException {
		storageWriter.writeDouble(value);
	}

	@Override
	public String getSqlToken() {
		return String.format("DOUBLE(%d,%d)", totalLength, decimalLength);
	}
}
