/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.storage.index;

import net.jevring.cu.sqld.storage.Row;
import net.jevring.cu.sqld.storage.RowReference;
import net.jevring.cu.sqld.structure.index.Index;
import net.jevring.cu.sqld.structure.index.IndexId;
import net.jevring.cu.sqld.structure.index.IndexKey;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This stores all indexes and index data for a single table.
 *
 * @author markus@jevring.net
 */
public class TableIndexes {
	private final Map<IndexId, IndexData> indexData = new HashMap<>();

	public void addIndex(Index index, Set<RowReference> rows) throws SQLException {
		// todo: the type of index should determine the index data etc in the future (like how it's stored, hash vs tree, etc)
		IndexData indexData;
		if (index.getType() == Index.Type.Unique) {
			indexData = new UniqueIndexData(index.getMapping());
		} else {
			indexData = new MultipleRowHashIndexData(index.getMapping());
		}
		for (RowReference row : rows) {
			indexData.add(row.getRow(), row);
		}
		// only add it once we've added all the rows and we know that the index can be created.
		// this is only really an issue for unique indexes
		this.indexData.put(index.getId(), indexData);
	}

	public void addRow(Row row, RowReference rowReference) throws SQLException {
		for (IndexData index : indexData.values()) {
			index.add(row, rowReference);
		}
	}

	public void updateRows(Row updatedRow,
	                       RowReference updatedRowReference,
	                       Row originalRow,
	                       RowReference originalRowReference) throws SQLException {
		for (IndexData index : indexData.values()) {
			index.delete(originalRow, originalRowReference);
			index.add(updatedRow, updatedRowReference);
		}
	}

	public void deleteRow(RowReference row, Row removedRow) {
		for (IndexData index : indexData.values()) {
			index.delete(removedRow, row);
		}
	}

	public Set<RowReference> getRows(Index index, IndexKey key) {
		return indexData.get(index.getId()).get(key);
	}
}
