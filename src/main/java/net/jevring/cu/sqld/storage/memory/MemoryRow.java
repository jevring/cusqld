/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.storage.memory;

import net.jevring.cu.sqld.storage.Row;

import java.sql.SQLException;
import java.util.Arrays;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class MemoryRow implements Row {
	private final Object[] values;

	public MemoryRow(Object[] values) {
		this.values = values;
	}

	@Override
	public <T> T getValueAt(int column) throws SQLException {
		if (column < 0 || column >= values.length) {
			throw new SQLException(String.format("Column index out of bounds. Got %d but expected a value <= %d",
			                                     column,
			                                     values.length));
		}
		return (T) values[column];
	}

	@Override
	public String getStringAt(int column) throws SQLException {
		return (String) getValueAt(column);
	}

	@Override
	public Double getAsDoubleAt(int column) throws SQLException {
		Object value = getValueAt(column);
		if (value instanceof Number) {
			return ((Number) value).doubleValue();
		} else {
			try {
				// todo: for now it'll always be a string, but perhaps we can do something more elegant in the parser later
				return Double.parseDouble((String) value);
			} catch (Exception e) {
				throw new SQLException(String.format("Value in field %s (%s) is not a number", column, value));
			}

		}
	}

	@Override
	public void setValue(int index, Object value) {
		values[index] = value;
	}

	@Override
	public Object[] getValues() {
		return values;
	}

	@Override
	public String toString() {
		return "MemoryRow{" +
				"values=" + Arrays.toString(values) +
				'}';
	}
}
