/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.storage.index;

import net.jevring.cu.sqld.storage.Row;
import net.jevring.cu.sqld.storage.RowReference;
import net.jevring.cu.sqld.structure.index.IndexKey;
import net.jevring.cu.sqld.structure.index.IndexMapping;

import java.util.*;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
class MultipleRowHashIndexData implements IndexData {
	private final Map<IndexKey, Set<RowReference>> rows = new HashMap<>();
	private final IndexMapping mapping;

	public MultipleRowHashIndexData(IndexMapping mapping) {
		this.mapping = mapping;
	}

	@Override
	public void add(Row row, RowReference rowReference) {
		IndexKey indexKey = mapping.convertToIndexKey(row);
		Set<RowReference> rowReferences = rows.get(indexKey);
		if (rowReferences == null) {
			rowReferences = new HashSet<>();
			rows.put(indexKey, rowReferences);
		}
		rowReferences.add(rowReference);
	}

	@Override
	public void delete(Row row, RowReference rowReference) {
		IndexKey key = mapping.convertToIndexKey(row);
		Set<RowReference> rowReferences = rows.get(key);
		if (rowReferences == null) {
			throw new AssertionError(String.format("Expected to find references for the key %s for row %s but found nothing",
			                                       key,
			                                       row));
		}
		boolean removed = rowReferences.remove(rowReference);
		if (!removed) {
			throw new AssertionError(String.format("Expected to find references for the key %s for row %s but found nothing",
			                                       key,
			                                       row));
		}
	}

	@Override
	public Set<RowReference> get(IndexKey key) {
		Set<RowReference> rowReferences = rows.get(key);
		if (rowReferences == null) {
			return Collections.emptySet();
		} else {
			return rowReferences;
		}
	}
}
