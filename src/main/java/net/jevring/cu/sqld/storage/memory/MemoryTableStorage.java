/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.storage.memory;

import net.jevring.cu.sqld.statement.conditions.indexed.IndexedConditionTree;
import net.jevring.cu.sqld.statement.conditions.indexed.IndexedConditionTreeMatcher;
import net.jevring.cu.sqld.storage.Row;
import net.jevring.cu.sqld.storage.RowReference;
import net.jevring.cu.sqld.storage.TableStorage;
import net.jevring.cu.sqld.storage.index.TableIndexes;
import net.jevring.cu.sqld.structure.Table;
import net.jevring.cu.sqld.structure.index.Index;
import net.jevring.cu.sqld.structure.index.IndexKey;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Table storage that stores table data in memory only.
 *
 * @author markus@jevring.net
 */
public class MemoryTableStorage implements TableStorage {
	private final TableIndexes tableIndexes = new TableIndexes();
	// Implementation note: a micro-optimization might be to use AtomicLong for ints, but that's likely a very small speed change.
	// todo: this will have problem with wrapping, though, which might be an issue. Deal with that later
	private final AtomicLong autoIncrementLong = new AtomicLong();

	private final Map<RowReference, Row> rows = new HashMap<>();

	@Override
	public void dispose() {
		// does nothing as the gc takes care of things
	}

	@Override
	public void start() throws SQLException {
		// does nothing, as there's nothing to read from
	}

	@Override
	public Set<RowReference> getRows(Index index, IndexKey key) throws SQLException {
		return new HashSet<>(tableIndexes.getRows(index, key));
	}


	@Override
	public Set<RowReference> getRows() {
		return new HashSet<>(rows.keySet());
	}

	@Override
	public Set<RowReference> getRows(IndexedConditionTree conditions) throws SQLException {
		// this was just too easy and too beautiful.
		// since we throw exceptions, this can't be allowed.
		// well, fuck...
		//return rows.parallelStream().filter(matcher::matches).collect(Collectors.toList());

		IndexedConditionTreeMatcher matcher = new IndexedConditionTreeMatcher(conditions);

		Set<RowReference> matchingRows = new HashSet<>();
		for (Map.Entry<RowReference, Row> entry : rows.entrySet()) {
			RowReference rowReference = entry.getKey();
			Row row = entry.getValue();
			if (matcher.matches(row)) {
				matchingRows.add(rowReference);
			}
		}
		return matchingRows;
	}

	@Override
	public List<RowReference> addRows(List<Row> rows) throws SQLException {
		List<RowReference> newRowReferences = new ArrayList<>();
		for (Row row : rows) {
			MemoryRowReference rowReference = new MemoryRowReference(this);
			newRowReferences.add(rowReference);
			this.rows.put(rowReference, row);
			tableIndexes.addRow(row, rowReference);
		}
		return newRowReferences;
	}


	@Override
	public void updateRows(List<Row> changedRows, List<RowReference> changedRowReferences) throws SQLException {
		for (int i = 0; i < changedRows.size(); i++) {
			Row changedRow = changedRows.get(i);
			RowReference changedRowReference = changedRowReferences.get(i);
			Row originalRow = rows.put(changedRowReference, changedRow);
			tableIndexes.updateRows(changedRow, changedRowReference, originalRow, changedRowReference);
		}
	}

	@Override
	public void deleteRows(Set<RowReference> rows) {
		for (RowReference row : rows) {
			Row removedRow = this.rows.remove(row);
			tableIndexes.deleteRow(row, removedRow);
		}
	}

	@Override
	public long autoIncrement(int autoIncrementStep) {
		return autoIncrementLong.addAndGet(autoIncrementStep);
	}

	@Override
	public void addIndex(Index index) throws SQLException {
		tableIndexes.addIndex(index, rows.keySet());
	}

	@Override
	public void setTable(Table table) {
		// does nothing, as this is not required for this storage engine
	}

	Row getRow(MemoryRowReference rowReference) {
		return rows.get(rowReference);
	}
}
