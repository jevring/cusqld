/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.storage.disk.variable;

import net.jevring.cu.sqld.statement.conditions.indexed.IndexedConditionTree;
import net.jevring.cu.sqld.statement.conditions.indexed.IndexedConditionTreeMatcher;
import net.jevring.cu.sqld.storage.Row;
import net.jevring.cu.sqld.storage.RowReference;
import net.jevring.cu.sqld.storage.TableStorage;
import net.jevring.cu.sqld.storage.disk.FileOffsetRowProvider;
import net.jevring.cu.sqld.storage.disk.FileOffsetRowReference;
import net.jevring.cu.sqld.storage.index.TableIndexes;
import net.jevring.cu.sqld.storage.memory.MemoryRow;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.structure.Table;
import net.jevring.cu.sqld.structure.TableId;
import net.jevring.cu.sqld.structure.column.Column;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.column.types.*;
import net.jevring.cu.sqld.structure.index.Index;
import net.jevring.cu.sqld.structure.index.IndexKey;

import java.io.*;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Table storage for a table that keeps each table in a separate file on disk.
 * BLOBs, CLOBs and TEXTs get their own files.
 *
 * @author markus@jevring.net
 */
public class VariableWidthTablePerFileTableStorage implements TableStorage, FileOffsetRowProvider {
	private static final long numberOfRecordsOffset = 8;
	private static final long recordsStart = numberOfRecordsOffset + 8;
	private static final int VARIABLE_WIDTH_TABLE_PER_FILE_FORMAT = 2;

	private static final int ROW_HEADER_RECORD_SIZE = 4;
	private static final int ROW_HEADER_DELETED_SIZE = 1;
	/**
	 * Row header:<br>
	 * 4 bytes: length<br>
	 * 1 byte:  deleted<br>
	 */
	private static final int ROW_HEADER_SIZE = ROW_HEADER_RECORD_SIZE + ROW_HEADER_DELETED_SIZE;

	private final TableIndexes tableIndexes = new TableIndexes();
	private final Set<RowReference> rowReferences = new HashSet<>();
	// Implementation note: a micro-optimization might be to use AtomicLong for ints, but that's likely a very small speed change.
	// todo: this will have problem with wrapping, though, which might be an issue. Deal with that later
	private final AtomicLong autoIncrementLong = new AtomicLong();
	private final FreeSpaceManager freeSpaceManager;
	private final RandomAccessFile data;
	private final Charset charset;
	private final File file;
	private Table table;

	public VariableWidthTablePerFileTableStorage(File dataDirectory, SchemaId schemaId, TableId tableId, Charset charset) throws
	                                                                                                                      SQLException {
		Objects.requireNonNull(dataDirectory, "Data directory can't be null");
		Objects.requireNonNull(schemaId, "Schema can't be null");
		Objects.requireNonNull(tableId, "Table can't be null");
		Objects.requireNonNull(charset, "Charset can't be null");
		this.charset = charset;
		// todo: have a better location
		// todo: take file locks etc
		this.file = new File(dataDirectory, String.format("%s/%s/rows.bin", schemaId, tableId));
		try {
			boolean newFile = !file.exists();
			if (newFile) {
				if (!file.getParentFile().exists()) {
					if (!file.getParentFile().mkdirs()) {
						throw new SQLException("Could not create directories (some may already have been created): " + file.getParentFile()
						                                                                                                   .getAbsolutePath());
					}
				}
			}
			this.data = new RandomAccessFile(file, "rws");
			this.freeSpaceManager = new FreeSpaceManager(ROW_HEADER_RECORD_SIZE, this.data, ROW_HEADER_SIZE);
			if (newFile) {
				writeHeader();
			} else {
				validateHeader();
			}
		} catch (IOException e) {
			throw new SQLException(e);
		}
	}

	@Override
	public void start() throws SQLException {
		try {
			readRowReferences();
		} catch (IOException e) {
			throw new SQLException("Could not read rows from disk", e);
		}
	}

	/**
	 * Traverses the file and acquires the row references.
	 * This way we can know where to look for things without having to traverse
	 * the file for each query.
	 */
	private void readRowReferences() throws IOException, SQLException {
		// todo: should we even track the number of rows in the file? If we do, it's multiple places to seek to and write to for each operation

		// todo: use a stream for this instead of a random access file
		data.seek(numberOfRecordsOffset);
		long totalRows = data.readLong();
		data.seek(recordsStart); // should be a noop
		for (int row = 0; row < totalRows; row++) {
			long fileOffset = data.getFilePointer();
			int recordLength = data.readInt();
			boolean deleted = data.readBoolean(); // todo: we should likely not waste a whole boolean of this
			if (deleted) {
				freeSpaceManager.free(recordLength, fileOffset, false);
			} else {
				FileOffsetRowReference rowReference = new FileOffsetRowReference(this, fileOffset, recordLength);
				rowReferences.add(rowReference);
				Row r = readRowAtCurrentPosition(rowReference.getRecordLength());
				// todo: is this the best way to do it? i.e. not storing the index itself
				tableIndexes.addRow(r, rowReference);
			}
			// since we're reading the row at the current position, we'll be in position anyway, so there's no need to this.
			// todo: if we change to a file-based storage for the index, this will have to change
			//data.skipBytes(recordLength);
		}
	}

	private void validateHeader() {
		// todo: validate the header
	}

	private void writeHeader() throws IOException {
		/*
		4 bytes: fourCC
		1 byte: storage format id
		1 byte: storage format version
		2 byte: filler
		8 bytes: number of records
		 */
		data.writeBytes("CUTS"); // fourCC "cusql table storage"
		data.writeByte(VARIABLE_WIDTH_TABLE_PER_FILE_FORMAT); // storage format id
		data.writeByte(1); // storage format version
		data.write(new byte[2]); // filler
		data.writeLong(0L); // number of records
	}

	@Override
	public void dispose() {
		boolean deleted = file.delete();
		if (!deleted) {
			throw new IllegalStateException("Could not delete table storage files in " + file.getAbsolutePath());
		}
	}

	@Override
	public Set<RowReference> getRows(IndexedConditionTree conditions) throws SQLException {
		IndexedConditionTreeMatcher matcher = new IndexedConditionTreeMatcher(conditions);

		Set<RowReference> matchingRows = new HashSet<>();
		for (RowReference rowReference : rowReferences) {
			if (matcher.matches(rowReference.getRow())) {
				matchingRows.add(rowReference);
			}
		}
		return matchingRows;
	}

	@Override
	public Set<RowReference> getRows(Index index, IndexKey key) throws SQLException {
		return tableIndexes.getRows(index, key);
	}

	@Override
	public Set<RowReference> getRows() throws SQLException {
		return new HashSet<>(rowReferences);
	}

	@Override
	public List<RowReference> addRows(List<Row> rows) throws SQLException {
		try {
			List<RowReference> newRowReferences = new ArrayList<>();
			for (Row row : rows) {
				byte[] bytes = getBytes(row);
				long nextFreeLocation = freeSpaceManager.allocate(bytes.length);
				FileOffsetRowReference rowReference = new FileOffsetRowReference(this, nextFreeLocation, bytes.length);
				newRowReferences.add(rowReference);
				rowReferences.add(rowReference);
				data.seek(nextFreeLocation);
				data.writeInt(bytes.length); // 4 bytes: record length
				data.writeBoolean(false); // false, since the record is NOT deleted
				data.write(bytes);
				// todo: handle blob/clob in separate files as well.
				// this will make them easier to read and handle, since we can just stream to/from those files.
			}
			data.seek(numberOfRecordsOffset);
			data.writeLong(rowReferences.size());
			return newRowReferences;
		} catch (IOException e) {
			throw new SQLException("Could not add rows", e);
		}
	}

	@Override
	public void updateRows(List<Row> changedRows, List<RowReference> changedRowReferences) throws SQLException {
		List<RowReference> newRowReferences = addRows(changedRows);
		deleteRows(new HashSet<>(changedRowReferences));

		for (int i = 0; i < changedRowReferences.size(); i++) {
			RowReference newRowReference = newRowReferences.get(i);
			RowReference changedRowReference = changedRowReferences.get(i);
			Row originalRow = changedRowReference.getRow();
			Row changedRow = changedRows.get(i);
			// remove it because we'll create a new one that points to the new data
			rowReferences.remove(changedRowReference);
			tableIndexes.updateRows(changedRow, newRowReference, originalRow, changedRowReference);
		}
	}

	@Override
	public void deleteRows(Set<RowReference> rows) throws SQLException {
		try {
			for (RowReference row : rows) {
				rowReferences.remove(row);
				FileOffsetRowReference forr = (FileOffsetRowReference) row;
				freeSpaceManager.free(forr.getRecordLength(), forr.getOffset(), true);
				tableIndexes.deleteRow(row, row.getRow());
			}
		} catch (IOException e) {
			throw new SQLException("Could not delete rows", e);
		}
	}

	@Override
	public long autoIncrement(int autoIncrementStep) {
		return autoIncrementLong.addAndGet(autoIncrementStep);
	}

	@Override
	public void addIndex(Index index) throws SQLException {
		tableIndexes.addIndex(index, rowReferences);
	}

	@Override
	public void setTable(Table table) {
		Objects.requireNonNull(table, "Table can't be null");
		// todo: this is a bit hacky, but since this table storage needs to know the structure of the row, it's required
		this.table = table;
	}

	private byte[] getBytes(Row row) throws SQLException, IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);

		StorageWriter storageWriter = new DataOutputStreamStorageWriter(dos, charset);

		writeBitSet(row, table.getNullableColumns(), dos, (o, flags, flagIndex) -> {
			if (o == null) {
				flags.set(flagIndex);
			}
		});

		writeBitSet(row, table.getBooleanColumns(), dos, (o, flags, flagIndex) -> {
			if (o != null) {
				flags.set(flagIndex, (Boolean) o);
			} // else it's been handled by the null handler above
		});

		List<ColumnId> columnsInOrder = table.getColumnsInOrder();
		for (int i = 0; i < columnsInOrder.size(); i++) {
			ColumnId columnId = columnsInOrder.get(i);
			Column<Object> column = table.getColumn(columnId);
			Object o = row.getValueAt(i);
			column.getType().writeDataToStorage(storageWriter, o);
		}
		return baos.toByteArray();
	}


	@Override
	public Row readRow(FileOffsetRowReference rowReference) throws IOException, SQLException {
		// todo: read the record into memory (ideally via a cache in the future)
		data.seek(rowReference.getOffset() + ROW_HEADER_SIZE);
		return readRowAtCurrentPosition(rowReference.getRecordLength());
	}

	private Row readRowAtCurrentPosition(int recordLength) throws IOException, SQLException {
		// since we know exactly how long the record is, just batch-read it from disk and then 
		// we can do read things from the bytes in memory
		byte[] bytes = new byte[recordLength];
		data.read(bytes);

		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(bytes));


		// todo: unit tests for booleans and null values.
		// todo: in general, write more unit tests!

		BitSet nullableFlags = readBitSet(dis);
		BitSet booleanFlags = readBitSet(dis);

		// todo: if we had the column info, rather than just the column type, we could put the nullable flags into the storage reader as well.
		// should we move the methods ot the column, rather than the column type?
		// seems a bit overkill for just this use-case
		StorageReader storageReader = new DataInputStreamStorageReader(dis, charset, booleanFlags);

		Object[] values = new Object[table.getNumberOfColumns()];
		List<ColumnId> columnsInOrder = table.getColumnsInOrder();
		int nullableColumn = 0;
		for (int i = 0; i < columnsInOrder.size(); i++) {
			ColumnId columnId = columnsInOrder.get(i);
			Column<?> column = table.getColumn(columnId);
			if (!column.isNotNull() && nullableFlags.get(nullableColumn++)) {
				// NOTE: don't flip these two conditions. 
				// if we do, we'll increment nullableColumn too often
				values[i] = null;
			} else {
				values[i] = column.getType().readDataFromStorage(storageReader);
			}
		}
		return new MemoryRow(values);
	}

	/**
	 * Reads a bit set from the stream. The bitset is specified as a byte for the length,
	 * followed by that many bytes.
	 */
	private BitSet readBitSet(DataInputStream dis) throws IOException {
		byte length = dis.readByte();
		byte[] bytes = new byte[length];
		dis.readFully(bytes);
		return BitSet.valueOf(bytes);
	}

	private void writeBitSet(Row row, List<ColumnId> columns, DataOutputStream dos, BitSetFlagHandler handler) throws
	                                                                                                           SQLException,
	                                                                                                           IOException {
		BitSet flags = new BitSet(columns.size());
		for (int columnIndex = 0; columnIndex < columns.size(); columnIndex++) {
			ColumnId column = columns.get(columnIndex);
			int rowIndex = table.indexOf(column);
			Object o = row.getValueAt(rowIndex);
			handler.handle(o, flags, columnIndex);
		}
		byte[] bytes = flags.toByteArray();
		dos.writeByte(bytes.length);
		dos.write(bytes);
	}

	private static interface BitSetFlagHandler {
		public void handle(Object o, BitSet flags, int flagIndex);
	}

	private static final class DataInputStreamStorageReader implements StorageReader {
		private final DataInputStream dis;
		private final Charset charset;
		private final BitSet booleanFlags;
		private int booleanColumn = 0;

		public DataInputStreamStorageReader(DataInputStream dis, Charset charset, BitSet booleanFlags) {
			this.dis = dis;
			this.charset = charset;
			this.booleanFlags = booleanFlags;
		}

		@Override
		public Double readDouble(DoubleColumnType columnType) throws IOException {
			return dis.readDouble();
		}

		@Override
		public Float readFloat(FloatColumnType columnType) throws IOException {
			return dis.readFloat();
		}

		@Override
		public Byte readByte(ByteColumnType columnType) throws IOException {
			return dis.readByte();
		}

		@Override
		public Short readShort(ShortColumnType columnType) throws IOException {
			return dis.readShort();
		}

		@Override
		public Integer readInteger(IntegerColumnType columnType) throws IOException {
			return dis.readInt();
		}

		@Override
		public Long readLong(LongColumnType columnType) throws IOException {
			return dis.readLong();
		}

		@Override
		public BigDecimal readBigDecimal(BigDecimalColumnType columnType) throws IOException {
			return new BigDecimal(readSizePrefixedString());
		}

		@Override
		public String readChar(CharColumnType columnType) throws IOException {
			CharsetEncoder charsetEncoder = charset.newEncoder();
			// why maxBytesPerChar() is a float instead of an int, I will never know...
			int length = (int) Math.ceil((double) charsetEncoder.maxBytesPerChar() * columnType.getLength());
			byte[] bytes = new byte[length];
			dis.readFully(bytes);
			// we have to substring this, as we might have a byte array
			// that is much too long, to handle variable length encoders 
			// like UTF-8. If we do, we get trailing garbage, which needs
			// to be handled.
			return new String(bytes, charset).substring(0, columnType.getLength());
		}

		@Override
		public String readVarChar(VarCharColumnType columnType) throws IOException {
			return readSizePrefixedString();
		}

		@Override
		public Boolean readBoolean(BooleanColumnType columnType) throws IOException {
			return booleanFlags.get(booleanColumn++);
		}

		@Override
		public String readText(TextColumnType columnType) throws IOException {
			// todo: read this from a separate file

			// todo: implement DataInputStreamStorageReader.readText()
			throw new UnsupportedOperationException("DataInputStreamStorageReader.readText() not yet implemented");
		}

		private String readSizePrefixedString() throws IOException {
			// if this is something like UTF-16, then we'll get a length that is longer than a byte,
			// which is why we have to use a short instead of a byte.
			short length = dis.readShort();
			byte[] bytes = new byte[length];
			dis.readFully(bytes);
			return new String(bytes, charset);
		}
	}

	private static final class DataOutputStreamStorageWriter implements StorageWriter {
		private final DataOutputStream dos;
		private final Charset charset;

		public DataOutputStreamStorageWriter(DataOutputStream dos, Charset charset) {
			this.dos = dos;
			this.charset = charset;
		}

		// todo: support all the others with special boolean and blob handling
		@Override
		public void writeChar(String value, CharColumnType columnType) throws IOException, SQLException {
			String padded = columnType.convertAndAdjust(value);
			CharsetEncoder charsetEncoder = charset.newEncoder();
			// why maxBytesPerChar() is a float instead of an int, I will never know...
			// see StringEncoder.scale() for why we use a double here
			int length = (int) Math.ceil((double) charsetEncoder.maxBytesPerChar() * columnType.getLength());
			byte[] bytes = new byte[length];
			CoderResult coderResult = charsetEncoder.encode(CharBuffer.wrap(padded), ByteBuffer.wrap(bytes), true);
			if (coderResult.isError() || coderResult.isUnmappable()) {
				coderResult.throwException();
			}
			dos.write(bytes);
		}

		@Override
		public void writeVarChar(String value, VarCharColumnType columnType) throws IOException {
			writeSizePrefixedString(value);
		}

		@Override
		public void writeInteger(Integer value) throws IOException {
			dos.writeInt(value);
		}

		@Override
		public void writeLong(Long value) throws IOException {
			dos.writeLong(value);
		}

		@Override
		public void writeByte(Byte value) throws IOException {
			dos.writeByte(value);
		}

		@Override
		public void writeShort(Short value) throws IOException {
			dos.writeShort(value);
		}

		@Override
		public void writeDouble(Double value) throws IOException {
			dos.writeDouble(value);
		}

		@Override
		public void writeFloat(Float value) throws IOException {
			dos.writeFloat(value);
		}

		@Override
		public void writeBigDecimal(BigDecimal value) throws IOException {
			writeSizePrefixedString(value.toPlainString());
		}

		@Override
		public void writeBoolean(Boolean value) throws IOException, SQLException {
			// do nothing. boolean columns have received special handling already
		}

		@Override
		public void writeText(String value, TextColumnType textColumnType) throws IOException, SQLException {
			// todo: implement DataOutputStreamStorageWriter.writeText()
			throw new UnsupportedOperationException("DataOutputStreamStorageWriter.writeText() not yet implemented");
		}

		private void writeSizePrefixedString(String value) throws IOException {
			// if this is something like UTF-16, then we'll get a length that is longer than a byte,
			// which is why we have to use a short instead of a byte.
			byte[] bytes = value.getBytes(charset);
			dos.writeShort(bytes.length);
			dos.write(bytes);
		}
	}
}
