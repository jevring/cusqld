/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.storage.disk.variable;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;

/**
 * Tracks the free space in a file.
 *
 * @author markus@jevring.net
 */
public class FreeSpaceManager {
	/**
	 * A map keyed on the available size, which points to a list with offsets.
	 * <p>The sizes in this map are for <b>content</b> only, i.e. <b>without</b>
	 * any consideration for the row headers, which are always the same.
	 * <p>The offsets in this map are for the <b>whole</b> record, including headers.
	 */
	private final NavigableMap<Integer, List<Long>> freeSpacePointers = new TreeMap<>();
	private final int recordHeaderOffsetForDeletedFlag;
	private final RandomAccessFile data;
	private final int totalHeaderSize;

	public FreeSpaceManager(int recordHeaderOffsetForDeletedFlag, RandomAccessFile data, int totalHeaderSize) {
		this.recordHeaderOffsetForDeletedFlag = recordHeaderOffsetForDeletedFlag;
		this.data = data;
		this.totalHeaderSize = totalHeaderSize;
	}

	/**
	 * Looks for free space in the file that will hold the specified number of DATA (i.e. without header)
	 * bytes. If no free space can be found in the middle of the file, free space can always be found
	 * at the end of the file.
	 *
	 * @param length the least amount of space we need
	 * @return a position into which we can write this data <b>including the header</b>.
	 * @throws IOException if we couldn't query the file for its length
	 */
	public long allocate(int length) throws IOException {
		// the first key is the size of the first space that is larger than or equal to the space we need.
		Map.Entry<Integer, List<Long>> entry = freeSpacePointers.ceilingEntry(length);
		if (entry != null) {
			Integer nearestAmountOfSpace = entry.getKey();
			List<Long> pointers = entry.getValue();
			if (!pointers.isEmpty()) {
				Long pointer = pointers.remove(0);
				int remainingSpace = nearestAmountOfSpace - length;
				if (remainingSpace >= totalHeaderSize) {
					// as the pointer points to the start of the record, including the header, 
					// we need to add the header size to the pointer here
					free(remainingSpace, pointer + length + totalHeaderSize, true);
				}
				System.out.printf("Found %d bytes of free space @ %d%n", length, pointer);
				return pointer;
			}
		}
		System.out.printf("Found no free space for %d bytes, returning the end of the file: %d%n", length, data.length());
		// there's no free space. Add to the end.
		return data.length();
	}

	/**
	 * Registers the record as deleted and tracks the empty space. Empty space can later be used for new rows.
	 *
	 * @param length      the amount of space to relinquish. This is <b>without</b> the header
	 * @param offset      the file offset at which the record <b>including header</b> starts
	 * @param writeToDisk whether to write the data to disk. This is false when reading from disk at startup
	 * @throws IOException
	 */
	public void free(int length, long offset, boolean writeToDisk) throws IOException {
		// todo: coalesce adjacent spaces.
		List<Long> pointers = freeSpacePointers.get(length);
		if (pointers == null) {
			pointers = new ArrayList<>();
			freeSpacePointers.put(length, pointers);
		}
		pointers.add(offset);
		System.out.printf("Put free space: %d @ %d%n", length, offset);
		if (writeToDisk) {
			// since the offset is where the DATA starts, we have to go back to the boolean that indicates that the row is deleted
			data.seek(offset + recordHeaderOffsetForDeletedFlag);
			data.writeBoolean(true);
		}
	}
}
