/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement;

import net.jevring.cu.sqld.SqlDaemon;
import net.jevring.cu.sqld.connection.ConnectionContext;
import net.jevring.cu.sqld.connection.UserId;
import net.jevring.cu.sqld.statement.parser.Parser;
import net.jevring.cu.sqld.statement.parser.antlr4.AntlrSqlParserWrapper;
import net.jevring.cu.sqld.storage.ResultSet;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.util.Util;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests "outer" joins
 *
 * @author markus@jevring.net
 */
public class OuterJoinTest {
	private static final Parser parser = new AntlrSqlParserWrapper();
	private static final ConnectionContext connectionContext =
			new ConnectionContext(new UserId("user"), new SchemaId("test_schema"));
	private static final File DATA_DIRECTORY = new File("data/test/" + OuterJoinTest.class.getSimpleName());
	private static final SqlDaemon daemon = new SqlDaemon(DATA_DIRECTORY);

	@BeforeClass
	public static void setUp() throws Exception {
		Util.recursivelyWipeDirectory(DATA_DIRECTORY);
		CreateSchema create = parser.parse("CREATE SCHEMA test_schema");
		daemon.executeStructureChange(create, connectionContext);
		assertTrue(daemon.getSchemas().contains(new SchemaId("test_schema")));

		CreateTable createPerson = parser.parse("create table Person (name varchar(30), Age int, EmployerId int, SchoolId int)");
		daemon.executeStructureChange(createPerson, connectionContext);

		CreateTable createEmployer = parser.parse("create table Employer (Id int, Name varchar(30))");
		daemon.executeStructureChange(createEmployer, connectionContext);

		CreateTable createSchool = parser.parse("create table School (Id int, Name varchar(30))");
		daemon.executeStructureChange(createSchool, connectionContext);

		Insert insertPeople = parser.parse("insert into person values " +
				                                   "('Mike', 24, 3, 1),\n" +
				                                   "('Bob', 25, 4, 2),\n" +
				                                   "('Sue', 26, 5, 2),\n" +
				                                   "('Caroline', 27, 5, 3),\n" +
				                                   "('Rick', 28, null, 3),\n" +
				                                   "('Deckard', 29, 3, null)");
		daemon.executeUpdate(insertPeople, connectionContext);

		Insert insertEmployers = parser.parse("insert into employer values " +
				                                      "(1, 'Alpha Corp'),\n" +
				                                      "(2, 'Beta Inc'),\n" +
				                                      "(3, 'Gamma GmbH'),\n" +
				                                      "(4, 'Delta Services'),\n" +
				                                      "(5, 'Gas guzzlers R us'),\n" +
				                                      "(6, 'Universal exports'),\n" +
				                                      "(7, 'Mars airlines')");
		daemon.executeUpdate(insertEmployers, connectionContext);

		Insert insertSchools = parser.parse("insert into school values " +
				                                    "(1, 'Grammar school'),\n" +
				                                    "(2, 'High school'),\n" +
				                                    "(3, 'University')");
		daemon.executeUpdate(insertSchools, connectionContext);
	}

	@Test
	public void testLeftJoin() throws Exception {
		Select select = parser.parse("select * from Person p left outer join Employer e on p.EmployerId = e.Id order by age asc");
		ResultSet rs = daemon.executeQuery(select, connectionContext);

		List<List<Object>> expectedResults = Arrays.asList(Arrays.asList("Mike", 24, "Gamma GmbH"),
		                                                   Arrays.asList("Bob", 25, "Delta Services"),
		                                                   Arrays.asList("Sue", 26, "Gas guzzlers R us"),
		                                                   Arrays.asList("Caroline", 27, "Gas guzzlers R us"),
		                                                   Arrays.asList("Rick", 28, null),
		                                                   Arrays.asList("Deckard", 29, "Gamma GmbH"));

		int i = 0;
		while (rs.next()) {
			String personName = rs.getString("p.Name");
			Integer personAge = rs.getInteger("p.Age");
			String employerName = rs.getString("e.Name");
			System.out.printf("%10s is %d years old works at %s%n", personName, personAge, employerName);

			List<Object> expectedResult = expectedResults.get(i++);
			assertEquals(expectedResult.get(0), rs.getString("p.name"));
			assertEquals(expectedResult.get(1), rs.getInteger("p.Age"));
			assertEquals(expectedResult.get(2), rs.getString("e.name"));

		}
		assertEquals(expectedResults.size(), i);
	}

	@Test
	public void testRightJoin() throws Exception {
		Select select =
				parser.parse("select * from Person p right join Employer e on p.EmployerId = e.Id order by age desc, e.name Asc");
		ResultSet rs = daemon.executeQuery(select, connectionContext);

		List<List<Object>> expectedResults = Arrays.asList(Arrays.asList("Deckard", 29, "Gamma GmbH"),
		                                                   Arrays.asList("Caroline", 27, "Gas guzzlers R us"),
		                                                   Arrays.asList("Sue", 26, "Gas guzzlers R us"),
		                                                   Arrays.asList("Bob", 25, "Delta Services"),
		                                                   Arrays.asList("Mike", 24, "Gamma GmbH"),
		                                                   Arrays.asList(null, null, "Alpha Corp"),
		                                                   Arrays.asList(null, null, "Beta Inc"),
		                                                   Arrays.asList(null, null, "Mars airlines"),
		                                                   Arrays.asList(null, null, "Universal exports"));

		int i = 0;
		while (rs.next()) {
			String personName = rs.getString("p.Name");
			Integer personAge = rs.getInteger("p.Age");
			String employerName = rs.getString("e.Name");
			System.out.printf("%10s is %d years old works at %s%n", personName, personAge, employerName);

			List<Object> expectedResult = expectedResults.get(i++);
			assertEquals(expectedResult.get(0), rs.getString("p.name"));
			assertEquals(expectedResult.get(1), rs.getInteger("p.Age"));
			assertEquals(expectedResult.get(2), rs.getString("e.name"));
		}
		assertEquals(expectedResults.size(), i);
	}

	@Test
	public void testFullJoin() throws Exception {
		Select select =
				parser.parse("select * from Person p full join Employer e on p.EmployerId = e.Id order by age desc, e.name Asc");
		ResultSet rs = daemon.executeQuery(select, connectionContext);

		List<List<Object>> expectedResults = Arrays.asList(Arrays.asList("Deckard", 29, "Gamma GmbH"),
		                                                   Arrays.asList("Rick", 28, null),
		                                                   Arrays.asList("Caroline", 27, "Gas guzzlers R us"),
		                                                   Arrays.asList("Sue", 26, "Gas guzzlers R us"),
		                                                   Arrays.asList("Bob", 25, "Delta Services"),
		                                                   Arrays.asList("Mike", 24, "Gamma GmbH"),
		                                                   Arrays.asList(null, null, "Alpha Corp"),
		                                                   Arrays.asList(null, null, "Beta Inc"),
		                                                   Arrays.asList(null, null, "Mars airlines"),
		                                                   Arrays.asList(null, null, "Universal exports"));

		int i = 0;
		while (rs.next()) {
			String personName = rs.getString("p.Name");
			Integer personAge = rs.getInteger("p.Age");
			String employerName = rs.getString("e.Name");
			System.out.printf("%10s is %d years old works at %s%n", personName, personAge, employerName);

			List<Object> expectedResult = expectedResults.get(i++);
			assertEquals(expectedResult.get(0), rs.getString("p.name"));
			assertEquals(expectedResult.get(1), rs.getInteger("p.Age"));
			assertEquals(expectedResult.get(2), rs.getString("e.name"));
		}
		assertEquals(expectedResults.size(), i);
	}

	// todo: test multiple tables
}
