/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.structure;

import net.jevring.cu.sqld.SqlDaemon;
import net.jevring.cu.sqld.connection.ConnectionContext;
import net.jevring.cu.sqld.connection.UserId;
import net.jevring.cu.sqld.statement.CreateSchema;
import net.jevring.cu.sqld.statement.CreateTable;
import net.jevring.cu.sqld.statement.DropSchema;
import net.jevring.cu.sqld.statement.parser.Parser;
import net.jevring.cu.sqld.statement.parser.antlr4.AntlrSqlParserWrapper;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.util.Util;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 */
public class CreateAndDropSchemaTest {
	private static final Parser parser = new AntlrSqlParserWrapper();
	private static final ConnectionContext connectionContext =
			new ConnectionContext(new UserId("user"), new SchemaId("test_schema"));
	private static final File DATA_DIRECTORY = new File("data/test/" + CreateAndDropSchemaTest.class.getSimpleName());
	private static final SqlDaemon daemon = new SqlDaemon(DATA_DIRECTORY);

	@BeforeClass
	public static void setUp() throws Exception {
		Util.recursivelyWipeDirectory(DATA_DIRECTORY);
	}

	@Test
	public void testCreate() throws Exception {
		daemon.executeStructureChange(parser.<CreateSchema>parse("Create schema test_schema"), connectionContext);
		daemon.executeStructureChange(parser.<CreateTable>parse("Create table test (value int)"), connectionContext);
		assertNotNull(DATA_DIRECTORY.listFiles());
	}

	@Test
	public void testDelete() throws Exception {
		daemon.executeStructureChange(parser.<DropSchema>parse("drop schema test_schema"), connectionContext);
		assertNotNull(DATA_DIRECTORY.listFiles());
		//noinspection ConstantConditions
		assertEquals(0, DATA_DIRECTORY.listFiles().length);
	}
}
