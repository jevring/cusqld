/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.structure;

import net.jevring.cu.sqld.SqlDaemon;
import net.jevring.cu.sqld.connection.ConnectionContext;
import net.jevring.cu.sqld.connection.UserId;
import net.jevring.cu.sqld.statement.CreateSchema;
import net.jevring.cu.sqld.statement.CreateTable;
import net.jevring.cu.sqld.statement.Insert;
import net.jevring.cu.sqld.statement.Select;
import net.jevring.cu.sqld.statement.parser.Parser;
import net.jevring.cu.sqld.statement.parser.antlr4.AntlrSqlParserWrapper;
import net.jevring.cu.sqld.storage.ResultSet;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.structure.Table;
import net.jevring.cu.sqld.structure.TableId;
import net.jevring.cu.sqld.structure.column.Column;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.column.types.IntegerColumnType;
import net.jevring.cu.sqld.util.Util;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests columns with 'auto_increment' constraints.
 *
 * @author markus@jevring.net
 */
public class AutoIncrementTest {
	private static final Parser parser = new AntlrSqlParserWrapper();
	private static final ConnectionContext connectionContext =
			new ConnectionContext(new UserId("user"), new SchemaId("test_schema"));
	private static final File DATA_DIRECTORY = new File("data/test/" + AutoIncrementTest.class.getSimpleName());
	private static final SqlDaemon daemon = new SqlDaemon(DATA_DIRECTORY);

	@BeforeClass
	public static void setUp() throws Exception {
		Util.recursivelyWipeDirectory(DATA_DIRECTORY);
		CreateSchema createSchema = parser.parse("create schema test_schema");
		daemon.executeStructureChange(createSchema, connectionContext);
		CreateTable createTable = parser.parse("create table ids (id int auto_increment 12, name varchar(30))");
		daemon.executeStructureChange(createTable, connectionContext);
		Table table = daemon.getTable(new SchemaId("test_schema"), new TableId("ids"));
		assertEquals(2, table.getNumberOfColumns());
		assertTrue(table.getColumns().containsKey(new ColumnId("id")));
		assertTrue(table.getColumns().containsKey(new ColumnId("name")));
		Column<?> idColumn = table.getColumns().get(new ColumnId("id"));
		assertEquals(12.0, idColumn.getAutoIncrementStep(), 0);
		assertTrue(idColumn.getType() instanceof IntegerColumnType);
	}

	@Test
	public void testAutoIncrement() throws Exception {
		Insert insert = parser.parse("insert into ids values ('bob')");
		daemon.executeUpdate(insert, connectionContext);
		assertEquals(12L, connectionContext.getLastGeneratedId());

		Select select = parser.parse("select last_insert_ID()");
		ResultSet rs = daemon.executeQuery(select, connectionContext);
		if (rs.next()) {
			assertEquals(12L, rs.getValue("anything goes!"));
			assertEquals(12L, rs.getValue("last_insert_id()"));
			assertEquals(12L, rs.getValue("last_insert_id"));
			assertEquals(12L, rs.getValue(0));
		}
	}
}
