/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement;

import net.jevring.cu.sqld.SqlDaemon;
import net.jevring.cu.sqld.connection.ConnectionContext;
import net.jevring.cu.sqld.connection.UserId;
import net.jevring.cu.sqld.statement.parser.Parser;
import net.jevring.cu.sqld.statement.parser.antlr4.AntlrSqlParserWrapper;
import net.jevring.cu.sqld.storage.ResultSet;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.util.Util;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests selects and joins over multiple tables.
 * Note that the order in which the fields are returned will depend on
 * the implementation. If the implementation is changed, the order here
 * might change. This is perhaps not ideal, but it makes for easier testing.
 * This should not be construed as the order being specified here is the "correct"
 * order. It's just the current order. The content, however, should remain the same.
 *
 * @author markus@jevring.net
 */
public class SelectTest {
	private static final Parser parser = new AntlrSqlParserWrapper();
	private static final ConnectionContext connectionContext =
			new ConnectionContext(new UserId("user"), new SchemaId("test_schema"));
	private static final File DATA_DIRECTORY = new File("data/test/" + SelectTest.class.getSimpleName());
	private static final SqlDaemon daemon = new SqlDaemon(DATA_DIRECTORY);

	@BeforeClass
	public static void setUp() throws Exception {
		Util.recursivelyWipeDirectory(DATA_DIRECTORY);
		CreateSchema create = parser.parse("CREATE SCHEMA test_schema");
		daemon.executeStructureChange(create, connectionContext);
		assertTrue(daemon.getSchemas().contains(new SchemaId("test_schema")));

		CreateTable createPerson =
				parser.parse("create table Person (name varchar(30), AgeId int, HeightId int, DeliveryAddressId int)");
		daemon.executeStructureChange(createPerson, connectionContext);

		CreateTable createAge = parser.parse("create table Age (Id int, Age int)");
		daemon.executeStructureChange(createAge, connectionContext);

		CreateTable createHeight = parser.parse("create table Height (Id int, Height int)");
		daemon.executeStructureChange(createHeight, connectionContext);


		// populate the database
		Insert insertPeople = parser.parse("insert into Person values ('Mike', 1, 1, 1), ('Bob', 2, 2, 1), ('Sue', 1, 3, 1)");
		daemon.executeUpdate(insertPeople, connectionContext);

		Insert insertAges = parser.parse("insert into AGE values (1, 24), (2,32)");
		daemon.executeUpdate(insertAges, connectionContext);

		Insert insertHeights = parser.parse("insert into Height values (1, 176), (2, 180), (3, 168)");
		daemon.executeUpdate(insertHeights, connectionContext);

		CreateSchema createDeliverySchema = parser.parse("CREATE SCHEMA DeliverySchema");
		daemon.executeStructureChange(createDeliverySchema, connectionContext);

		CreateTable createAddress = parser.parse("create table DeliverySchema.Address (Id int, Description varchar(30))");
		daemon.executeStructureChange(createAddress, connectionContext);

		Insert insertDeliverAddresses = parser.parse("insert into DeliverySchema.Address values (1, 'The post office')");
		daemon.executeUpdate(insertDeliverAddresses, connectionContext);
	}

	@Test
	public void testLike() throws Exception {
		Select query = parser.parse("select * from Person where Name like '%e%' order by name asc");
		ResultSet rs = daemon.executeQuery(query, connectionContext);
		List<String> expectedResults = Arrays.asList("Mike", "Sue");

		int i = 0;
		while (rs.next()) {
			String expectedValue = expectedResults.get(i++);
			assertEquals(expectedValue, rs.getString("name"));
			System.out.printf("Person %5s%n", rs.getString("Name"));
		}
		assertEquals(expectedResults.size(), i);

	}

	@Test
	public void testOneTable() throws Exception {
		Select query = parser.parse("select * from Person order by name asc");
		ResultSet rs = daemon.executeQuery(query, connectionContext);
		List<String> expectedResults = Arrays.asList("Bob", "Mike", "Sue");

		int i = 0;
		while (rs.next()) {
			String expectedValue = expectedResults.get(i++);
			assertEquals(expectedValue, rs.getString("name"));
			System.out.printf("Person %5s%n", rs.getString("Name"));
		}
		assertEquals(expectedResults.size(), i);
	}

	@Test
	public void testTwoTables() throws Exception {
		Select query = parser.parse("select * from Person p, Age a order by name asc, age asc");
		ResultSet rs = daemon.executeQuery(query, connectionContext);
		List<List<Object>> expectedResults = Arrays.asList(Arrays.asList("Bob", 24),
		                                                   Arrays.asList("Bob", 32),
		                                                   Arrays.asList("Mike", 24),
		                                                   Arrays.asList("Mike", 32),
		                                                   Arrays.asList("Sue", 24),
		                                                   Arrays.asList("Sue", 32));

		int i = 0;
		while (rs.next()) {
			List<Object> expectedResult = expectedResults.get(i++);
			assertEquals(expectedResult.get(0), rs.getString("name"));
			assertEquals(expectedResult.get(1), rs.getInteger("Age"));
			System.out.printf("Person %5s is %d years old%n", rs.getString("Name"), rs.getInteger("Age"));
		}
		assertEquals(expectedResults.size(), i);
	}

	@Test
	public void testTwoTablesWithJoinCondition() throws Exception {
		Select query = parser.parse("select * from Person p, Age a where a.id = p.AgeId order by name asc");
		ResultSet rs = daemon.executeQuery(query, connectionContext);
		List<List<Object>> expectedResults =
				Arrays.asList(Arrays.asList("Bob", 32), Arrays.asList("Mike", 24), Arrays.asList("Sue", 24));
		int i = 0;
		while (rs.next()) {
			List<Object> expectedResult = expectedResults.get(i++);
			assertEquals(expectedResult.get(0), rs.getString("name"));
			assertEquals(expectedResult.get(1), rs.getInteger("Age"));

			System.out.printf("Person %5s is %d years old%n", rs.getString("Name"), rs.getInteger("Age"));
		}
		assertEquals(expectedResults.size(), i);
	}

	@Test
	public void testThreeTables() throws Exception {
		Select query = parser.parse("select * from Person p, Age a, Height h order by name asc, age asc, height asc");
		ResultSet rs = daemon.executeQuery(query, connectionContext);

		List<List<Object>> expectedResults = Arrays.asList(Arrays.asList("Bob", 24, 168),
		                                                   Arrays.asList("Bob", 24, 176),
		                                                   Arrays.asList("Bob", 24, 180),
		                                                   Arrays.asList("Bob", 32, 168),
		                                                   Arrays.asList("Bob", 32, 176),
		                                                   Arrays.asList("Bob", 32, 180),
		                                                   Arrays.asList("Mike", 24, 168),
		                                                   Arrays.asList("Mike", 24, 176),
		                                                   Arrays.asList("Mike", 24, 180),
		                                                   Arrays.asList("Mike", 32, 168),
		                                                   Arrays.asList("Mike", 32, 176),
		                                                   Arrays.asList("Mike", 32, 180),
		                                                   Arrays.asList("Sue", 24, 168),
		                                                   Arrays.asList("Sue", 24, 176),
		                                                   Arrays.asList("Sue", 24, 180),
		                                                   Arrays.asList("Sue", 32, 168),
		                                                   Arrays.asList("Sue", 32, 176),
		                                                   Arrays.asList("Sue", 32, 180));
		int i = 0;
		while (rs.next()) {
			List<Object> expectedResult = expectedResults.get(i++);
			assertEquals(expectedResult.get(0), rs.getString("name"));
			assertEquals(expectedResult.get(1), rs.getInteger("Age"));
			assertEquals(expectedResult.get(2), rs.getInteger("height"));
			System.out.printf("Person %5s is %d years old and %d cm tall%n",
			                  rs.getString("Name"),
			                  rs.getInteger("Age"),
			                  rs.getInteger("Height"));
		}
		assertEquals(expectedResults.size(), i);
	}

	@Test
	public void testThreeTablesWithPartialJoinConditions() throws Exception {
		Select query = parser.parse("select * from Person p, Age a, Height h where a.id = p.AgeId order by name asc, height asc");
		List<List<Object>> expectedResults = Arrays.asList(Arrays.asList("Bob", 32, 168),
		                                                   Arrays.asList("Bob", 32, 176),
		                                                   Arrays.asList("Bob", 32, 180),
		                                                   Arrays.asList("Mike", 24, 168),
		                                                   Arrays.asList("Mike", 24, 176),
		                                                   Arrays.asList("Mike", 24, 180),
		                                                   Arrays.asList("Sue", 24, 168),
		                                                   Arrays.asList("Sue", 24, 176),
		                                                   Arrays.asList("Sue", 24, 180));
		ResultSet rs = daemon.executeQuery(query, connectionContext);
		int i = 0;
		while (rs.next()) {
			List<Object> expectedResult = expectedResults.get(i++);
			assertEquals(expectedResult.get(0), rs.getString("name"));
			assertEquals(expectedResult.get(1), rs.getInteger("Age"));
			assertEquals(expectedResult.get(2), rs.getInteger("height"));

			System.out.printf("Person %5s is %d years old and %d cm tall%n",
			                  rs.getString("Name"),
			                  rs.getInteger("Age"),
			                  rs.getInteger("Height"));
		}
		assertEquals(expectedResults.size(), i);
	}

	@Test
	public void testThreeTablesWithFullJoinConditions() throws Exception {
		Select query = parser.parse(
				"select * from Person p, age a, Height h where a.id = p.ageId and h.Id = p.HeightId order by name asc");
		ResultSet rs = daemon.executeQuery(query, connectionContext);
		List<List<Object>> expectedResults =
				Arrays.asList(Arrays.asList("Bob", 32, 180), Arrays.asList("Mike", 24, 176), Arrays.asList("Sue", 24, 168));
		int i = 0;
		while (rs.next()) {
			List<Object> expectedResult = expectedResults.get(i++);
			assertEquals(expectedResult.get(0), rs.getString("name"));
			assertEquals(expectedResult.get(1), rs.getInteger("Age"));
			assertEquals(expectedResult.get(2), rs.getInteger("height"));

			System.out.printf("Person %5s is %d years old and %d cm tall%n",
			                  rs.getString("Name"),
			                  rs.getInteger("Age"),
			                  rs.getInteger("Height"));
		}
		assertEquals(expectedResults.size(), i);
	}

	@Test
	public void testThreeTablesWithFullJoinConditionsWithColumnAliases() throws Exception {
		Select query = parser.parse(
				"select p.Name as WhatIsYourName, Age.Age aS HowOldAreYou, Height AS HowTallAreYou from Person p, age A, height h where a.id = p.ageId and h.id = p.heightId order by name asc");
		ResultSet rs = daemon.executeQuery(query, connectionContext);
		List<List<Object>> expectedResults =
				Arrays.asList(Arrays.asList("Bob", 32, 180), Arrays.asList("Mike", 24, 176), Arrays.asList("Sue", 24, 168));
		int i = 0;
		while (rs.next()) {
			List<Object> expectedResult = expectedResults.get(i++);
			assertEquals(expectedResult.get(0), rs.getString("WhatIsYourName"));
			assertEquals(expectedResult.get(1), rs.getInteger("HowOldAreYou"));
			assertEquals(expectedResult.get(2), rs.getInteger("HowTallAreYou"));

			System.out.printf("Person %5s is %d years old and %d cm tall%n",
			                  rs.getString("WhatIsYourName"),
			                  rs.getInteger("HowOldAreYou"),
			                  rs.getInteger("HowTallAreYou"));
		}
		assertEquals(expectedResults.size(), i);
	}

	@Test
	public void testCrossJoin() throws Exception {
		// todo: add tests for all the types of joins we have (when we actually have them implemented)
		Select query = parser.parse(
				"select * from Person p cross join DeliverySchema.Address a on p.DeliveryAddressId = a.Id order by name asc");

		ResultSet rs = daemon.executeQuery(query, connectionContext);
		List<List<Object>> expectedResults =
				Arrays.asList(Arrays.asList("Bob", "The post office"), Arrays.asList("Mike", "The post office"),

				              Arrays.asList("Sue", "The post office"));
		int i = 0;
		while (rs.next()) {
			List<Object> expectedResult = expectedResults.get(i++);
			assertEquals(expectedResult.get(0), rs.getString("p.Name"));
			assertEquals(expectedResult.get(1), rs.getString("a.Description"));

			System.out.printf("Person %5s gets goods delivered to %s%n", rs.getString("p.name"), rs.getString("a.description"));
		}
		assertEquals(expectedResults.size(), i);
	}
}
