/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.structure.index;

import net.jevring.cu.sqld.SqlDaemon;
import net.jevring.cu.sqld.connection.ConnectionContext;
import net.jevring.cu.sqld.connection.UserId;
import net.jevring.cu.sqld.statement.*;
import net.jevring.cu.sqld.statement.parser.Parser;
import net.jevring.cu.sqld.statement.parser.antlr4.AntlrSqlParserWrapper;
import net.jevring.cu.sqld.storage.ResultSet;
import net.jevring.cu.sqld.storage.memory.MemoryRow;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.structure.Table;
import net.jevring.cu.sqld.structure.TableId;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.index.Index;
import net.jevring.cu.sqld.structure.index.IndexKey;
import net.jevring.cu.sqld.util.Util;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

/**
 * Tests indexes in tables.
 *
 * @author markus@jevring.net
 */
public class UniqueIndexTest {

	private static final Parser parser = new AntlrSqlParserWrapper();
	private static final ConnectionContext connectionContext =
			new ConnectionContext(new UserId("user"), new SchemaId("test_schema"));
	private static final File DATA_DIRECTORY = new File("data/test/" + UniqueIndexTest.class.getSimpleName());
	private static final SqlDaemon daemon = new SqlDaemon(DATA_DIRECTORY);
	private static final MemoryRow SAMPLE_ROW = new MemoryRow(new Object[]{"a", "b", "c", "24"});

	@BeforeClass
	public static void setUp() throws Exception {
		Util.recursivelyWipeDirectory(DATA_DIRECTORY);
		CreateSchema create = parser.parse("CREATE SCHEMA test_schema");
		daemon.executeStructureChange(create, connectionContext);
		assertTrue(daemon.getSchemas().contains(new SchemaId("test_schema")));

		// since we can't set up test dependencies, we're going to have to carry out the test here in the setup.
		CreateTable createTable = parser.parse("create table Names (" +
				                                       "first_name varchar(120), " +
				                                       "middle_name varchar(120), " +
				                                       "last_name varchar(120), " +
				                                       "age int default 0, " +
				                                       "unique(first_name, last_name)," +
				                                       "unique(middle_name), " +
				                                       "unique(first_name)" +
				                                       ")");
		daemon.executeStructureChange(createTable, connectionContext);
		SchemaId schemaId = new SchemaId("test_schema");
		TableId tableId = new TableId("Names");
		Table table = daemon.getTable(schemaId, tableId);
		assertEquals(3, table.getIndexes().size());

		// first+last
		Index index = table.getIndexes().get(0);
		assertEquals(Index.Type.Unique, index.getType());
		assertEquals(Arrays.asList(new ColumnId("first_name"), new ColumnId("last_name")), index.getColumns());
		IndexKey key = index.getMapping().convertToIndexKey(SAMPLE_ROW);
		assertEquals(new IndexKey(new Object[]{"a", "c"}), key);


		// middle
		Index middleNameIndex = table.getIndexes().get(1);
		assertEquals(Index.Type.Unique, middleNameIndex.getType());
		assertEquals(Collections.singletonList(new ColumnId("middle_name")), middleNameIndex.getColumns());
		IndexKey middleNameKey = middleNameIndex.getMapping().convertToIndexKey(SAMPLE_ROW);
		assertEquals(new IndexKey(new Object[]{"b"}), middleNameKey);

		// first
		Index firstNameIndex = table.getIndexes().get(2);
		assertEquals(Index.Type.Unique, firstNameIndex.getType());
		assertEquals(Collections.singletonList(new ColumnId("first_name")), firstNameIndex.getColumns());
		IndexKey firstNameKey = firstNameIndex.getMapping().convertToIndexKey(SAMPLE_ROW);
		assertEquals(new IndexKey(new Object[]{"a"}), firstNameKey);
	}

	@Test
	public void testIndexInsertRejection() throws Exception {
		Insert i1 = parser.parse("insert into names (first_name, middle_name, last_name) values ('a_f', 'a_m', 'a_l')");
		daemon.executeUpdate(i1, connectionContext);
		// i1 should succeed

		try {
			Insert i2 = parser.parse(
					"insert into names (first_name, middle_name, last_name) values ('a_f', 'different middle', 'a_l')");
			daemon.executeUpdate(i2, connectionContext);
			// i2 should fail, as the first and last (but not middle) names are the same
			fail();
		} catch (SQLException ignored) {
			// we expect to end up here
		}
	}

	@Test
	public void testDeleteAndReinsert() throws Exception {
		Insert i1 = parser.parse("insert into names (first_name, middle_name, last_name) values ('b_f', 'b_m', 'b_l')");
		daemon.executeUpdate(i1, connectionContext);
		// i1 should succeed

		Delete d1 = parser.parse("delete from names where first_name = 'b_f'");
		daemon.executeUpdate(d1, connectionContext);

		// now we should be able to insert again as we've removed the offending key in the index.
		// had we not deleted, this would fail as in testIndexInsertRejection()
		daemon.executeUpdate(i1, connectionContext);
		// i1 should succeed
	}

	@Test
	public void testUpdateAndReinsert() throws Exception {
		Insert i1 = parser.parse("insert into names (first_name, middle_name, last_name) values ('c_f', 'c_m', 'c_l')");
		daemon.executeUpdate(i1, connectionContext);
		// i1 should succeed

		Update u1 = parser.parse("update names set first_name = 'd_f', middle_name='d_m' where first_name = 'c_f'");
		daemon.executeUpdate(u1, connectionContext);

		// now we should be able to insert again as we've removed the offending key in the index.
		// had we not updated, this would fail as in testIndexInsertRejection()
		daemon.executeUpdate(i1, connectionContext);
		// i1 should succeed
	}

	@Test
	public void testSelect() throws Exception {
		Select select = parser.parse("select * from names where " +
				                             "(first_name = 'e_f' and last_name = 'e_l' and middle_name = 'e_m') or " +
				                             "(first_name = 'f_f' and (last_name = 'f_l' or last_name = 'g_l')) or " +
				                             "middle_name='c_m' or " +
				                             "(first_name = 'f_f' and (middle_name = 'f_m' or middle_name = 'g_m'))");
		// todo: change this to just handle the planning
		ResultSet rs = daemon.executeQuery(select, connectionContext);
		// todo: add some assertions

	}

	@Test
	public void testSelect2() throws Exception {
		Insert insert = parser.parse("insert into names values ('h_f', 'h_m', 'h_l', 15)");
		daemon.executeUpdate(insert, connectionContext);
		Select select = parser.parse(
				"select * from names where (first_name = 'h_f' and age > 5 and middle_name = 'h_m' and last_name = 'h_l' and age < 20)");

		// todo: should we assert the structure of the tree here?
		// or is the result all that matters?
		// todo: we really should support EXPLAIN here or something, so we can look at the result from the eye of the sql connection

		// todo: change this to just handle the planning
		ResultSet rs = daemon.executeQuery(select, connectionContext);
		// todo: add some assertions

	}
}
