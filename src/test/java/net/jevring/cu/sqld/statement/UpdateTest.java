/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement;

import net.jevring.cu.sqld.SqlDaemon;
import net.jevring.cu.sqld.connection.ConnectionContext;
import net.jevring.cu.sqld.connection.UserId;
import net.jevring.cu.sqld.statement.parser.Parser;
import net.jevring.cu.sqld.statement.parser.antlr4.AntlrSqlParserWrapper;
import net.jevring.cu.sqld.storage.ResultSet;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.util.Util;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

/**
 * Tests the UPDATE statement.
 *
 * @author markus@jevring.net
 */
public class UpdateTest {
	private static final Parser parser = new AntlrSqlParserWrapper();
	private static final ConnectionContext connectionContext =
			new ConnectionContext(new UserId("user"), new SchemaId("test_schema"));
	private static final File DATA_DIRECTORY = new File("data/test/" + UpdateTest.class.getSimpleName());
	private static final SqlDaemon daemon = new SqlDaemon(DATA_DIRECTORY);

	@BeforeClass
	public static void setUp() throws Exception {
		Util.recursivelyWipeDirectory(DATA_DIRECTORY);
		CreateSchema create = parser.parse("CREATE SCHEMA test_schema");
		daemon.executeStructureChange(create, connectionContext);
		assertTrue(daemon.getSchemas().contains(new SchemaId("test_schema")));

		CreateTable createTable = parser.parse("create table Person (name varchar(30), age int)");
		daemon.executeStructureChange(createTable, connectionContext);

		Insert insertPeople = parser.parse("insert into Person values ('Mike', 24), ('Bob', 32), ('Sue', 38)");
		daemon.executeUpdate(insertPeople, connectionContext);
	}

	@Test
	public void testSingleFieldUpdate() throws Exception {
		Select findMike = parser.parse("select * from Person where name = 'Mike'");
		assertTrue(daemon.executeQuery(findMike, connectionContext).next());

		Update update = parser.parse("update person set name = 'Sven' where name = 'Mike'");
		int affectedRows = daemon.executeUpdate(update, connectionContext);
		assertEquals(1, affectedRows);
		// Mike is now gone
		assertFalse(daemon.executeQuery(findMike, connectionContext).next());
		// and now Sven is here instead
		Select findSven = parser.parse("select * from Person where name = 'Sven'");
		assertTrue(daemon.executeQuery(findSven, connectionContext).next());
	}

	@Test
	public void testDualFieldUpdate() throws Exception {
		Select findPeopleOver30 = parser.parse("select * from person where age > 30");
		ResultSet peopleOver30 = daemon.executeQuery(findPeopleOver30, connectionContext);
		// 2 hits, Bob and Sue
		assertTrue(peopleOver30.next());
		assertTrue(peopleOver30.next());

		Update update = parser.parse("update person set age = 20 where age > 30");
		int affectedRows = daemon.executeUpdate(update, connectionContext);
		assertEquals(2, affectedRows);

		// nobody left over 30 anymore
		assertFalse(daemon.executeQuery(findPeopleOver30, connectionContext).next());
	}
}
