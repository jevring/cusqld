/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.statement.structure;

import net.jevring.cu.sqld.SqlDaemon;
import net.jevring.cu.sqld.connection.ConnectionContext;
import net.jevring.cu.sqld.connection.UserId;
import net.jevring.cu.sqld.statement.CreateSchema;
import net.jevring.cu.sqld.statement.CreateTable;
import net.jevring.cu.sqld.statement.DropTable;
import net.jevring.cu.sqld.statement.parser.Parser;
import net.jevring.cu.sqld.statement.parser.antlr4.AntlrSqlParserWrapper;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.structure.Table;
import net.jevring.cu.sqld.structure.TableId;
import net.jevring.cu.sqld.structure.column.Column;
import net.jevring.cu.sqld.structure.column.ColumnId;
import net.jevring.cu.sqld.structure.column.types.*;
import net.jevring.cu.sqld.util.Util;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Tests that the {@code CREATE TABLE} DDL statements work.
 *
 * @author markus@jevring.net
 */
public class CreateTableTest {
	// note: we can't just use the schema name 'schema' here, as it's a reserved word...

	private static final Parser parser = new AntlrSqlParserWrapper();
	private static final ConnectionContext connectionContext =
			new ConnectionContext(new UserId("user"), new SchemaId("test_schema"));
	private static final File DATA_DIRECTORY = new File("data/test/" + CreateTableTest.class.getSimpleName());
	private static final SqlDaemon daemon = new SqlDaemon(DATA_DIRECTORY);

	@BeforeClass
	public static void setUp() throws Exception {
		Util.recursivelyWipeDirectory(DATA_DIRECTORY);
		CreateSchema create = parser.parse("CREATE SCHEMA test_schema");
		daemon.executeStructureChange(create, connectionContext);
		assertTrue(daemon.getSchemas().contains(new SchemaId("test_schema")));
	}

	@Test
	public void testCreateTable() throws Exception {
		CreateTable create = parser.parse("create table person (" +
				                                  "name varchar(120), " +
				                                  "age int," +
				                                  "some_text text," +
				                                  "some_enum enum('a', 'b')," +
				                                  "some_set set('a', 'b')," +
				                                  "some_float float(5, 10)" +
				                                  ")");
		daemon.executeStructureChange(create, connectionContext);
		SchemaId schemaId = new SchemaId("test_schema");
		TableId tableId = new TableId("Person");
		Set<TableId> tables = daemon.getTables(schemaId);
		assertTrue(tables.contains(tableId));
		Table table = daemon.getTable(schemaId, tableId);

		Column<String> nameColumn = table.getColumn(new ColumnId("name"));
		assertNotNull(nameColumn);
		assertTrue(nameColumn.getType() instanceof VarCharColumnType);
		VarCharColumnType nameColumnType = (VarCharColumnType) nameColumn.getType();
		assertEquals(120, nameColumnType.getLength());

		Column<Integer> ageColumn = table.getColumn(new ColumnId("age"));
		assertNotNull(ageColumn);
		assertTrue(ageColumn.getType() instanceof IntegerColumnType);

		Column<String> textColumn = table.getColumn(new ColumnId("some_text"));
		assertNotNull(textColumn);
		assertTrue(textColumn.getType() instanceof TextColumnType);

		Column<Object> enumColumn = table.getColumn(new ColumnId("some_enum"));
		assertNotNull(enumColumn);
		assertTrue(enumColumn.getType() instanceof EnumColumnType);
		EnumColumnType enumColumnType = (EnumColumnType) enumColumn.getType();
		assertEquals(new HashSet<>(Arrays.asList("a", "b")), enumColumnType.getAllowedValues());

		Column<Object> setColumn = table.getColumn(new ColumnId("some_set"));
		assertNotNull(setColumn);
		assertTrue(setColumn.getType() instanceof SetColumnType);
		SetColumnType setColumnType = (SetColumnType) setColumn.getType();
		assertEquals(new HashSet<>(Arrays.asList("a", "b")), setColumnType.getAllowedValues());

		Column<Float> floatColumn = table.getColumn(new ColumnId("some_float"));
		assertNotNull(floatColumn);
		assertTrue(floatColumn.getType() instanceof FloatColumnType);
		FloatColumnType doubleColumnType = (FloatColumnType) floatColumn.getType();
		assertEquals(5, doubleColumnType.getTotalLength());
		assertEquals(10, doubleColumnType.getDecimalLength());

		DropTable dropTable = parser.parse("drop table Person");
		daemon.executeStructureChange(dropTable, connectionContext);
		assertFalse(daemon.getTables(schemaId).contains(tableId));
		//noinspection ConstantConditions
		assertEquals(1, DATA_DIRECTORY.listFiles().length);
		assertEquals(new File("data/test/CreateTableTest/test_schema"), DATA_DIRECTORY.listFiles()[0]);
	}
}
