/*
 * Copyright (c) 2015, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 *     Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer.
 *     Redistributions in binary form must reproduce the above copyright notice, this 
 * list of conditions and the following disclaimer in the documentation and/or other
 *  materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE.
 */

package net.jevring.cu.sqld.storage.disk;

import net.jevring.cu.sqld.SqlDaemon;
import net.jevring.cu.sqld.connection.ConnectionContext;
import net.jevring.cu.sqld.connection.UserId;
import net.jevring.cu.sqld.statement.*;
import net.jevring.cu.sqld.statement.parser.Parser;
import net.jevring.cu.sqld.statement.parser.antlr4.AntlrSqlParserWrapper;
import net.jevring.cu.sqld.storage.ResultSet;
import net.jevring.cu.sqld.structure.SchemaId;
import net.jevring.cu.sqld.util.Util;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test for the variable width record disk-based table storage.
 *
 * @author markus@jevring.net
 */
public class VariableWidthTablePerFileTableStorageTest {
	private static final Parser parser = new AntlrSqlParserWrapper();
	private static final ConnectionContext connectionContext =
			new ConnectionContext(new UserId("user"), new SchemaId("test_schema"));
	private static final File DATA_DIRECTORY =
			new File("data/test/" + VariableWidthTablePerFileTableStorageTest.class.getSimpleName());
	private static final SqlDaemon daemon = new SqlDaemon(DATA_DIRECTORY);

	@BeforeClass
	public static void setUp() throws Exception {
		Util.recursivelyWipeDirectory(DATA_DIRECTORY);
		CreateSchema createSchema = parser.parse("create schema test_schema");
		daemon.executeStructureChange(createSchema, connectionContext);

		CreateTable createTable =
				parser.parse("create table Person (first_name char(10), last_name varchar(25), age int) engine TablePerFile");
		daemon.executeStructureChange(createTable, connectionContext);
	}

	@Test
	public void testInsertAndSelect() throws Exception {

		Insert insertBobAndMike = parser.parse(
				"insert into person values ('b\u20AC\u20AC\u20AC\u20AC\u20ACb', 'smith', 24), ('mike', 'rollins', 38)");
		daemon.executeUpdate(insertBobAndMike, connectionContext);

		Select select = parser.parse("select * from person where age < 35");
		ResultSet rs = daemon.executeQuery(select, connectionContext);
		assertTrue(rs.next());
		assertEquals("b€€€€€b   ", rs.getString("first_name"));
		assertEquals("smith", rs.getString("last_name"));
		assertEquals(24, rs.getInteger("age").intValue());
		assertFalse(rs.next());
	}

	@Test
	public void testInsertUpdateAndSelect() throws Exception {
		// NOTE: Ensure we don't just modify things at the start or at the end
		// clear everything
		daemon.executeUpdate(parser.<Delete>parse("delete from person"), connectionContext);

		Insert insert = parser.parse(
				"insert into person values ('b\u20AC\u20AC\u20AC\u20AC\u20ACb', 'smith', 24), ('mike', 'rollins', 30), ('sarah', 's', 40), ('lily', 's', 50), ('jack', 'stephenson', 60)");
		daemon.executeUpdate(insert, connectionContext);

		Update update = parser.parse("update person set age = 100 where age < 42 and age > 28");
		int updates = daemon.executeUpdate(update, connectionContext);
		assertEquals(2, updates);

		// there should be nobody left this young
		Select selectYoung = parser.parse("select * from person where age < 35");
		ResultSet rsYoung = daemon.executeQuery(selectYoung, connectionContext);
		assertTrue(rsYoung.next());
		assertEquals("b€€€€€b   ", rsYoung.getString("first_name"));
		assertEquals("smith", rsYoung.getString("last_name"));
		assertEquals(24, rsYoung.getInteger("age").intValue());
		assertFalse(rsYoung.next());
	}

	@Test
	public void testInsertDeleteAndSelect() throws Exception {
		// NOTE: Ensure we don't just modify things at the start or at the end

		// clear everything
		daemon.executeUpdate(parser.<Delete>parse("delete from person"), connectionContext);

		Insert insert = parser.parse(
				"insert into person values ('b\u20AC\u20AC\u20AC\u20AC\u20ACb', 'smith', 24), ('mike', 'rollins', 30), ('sarah', 's', 40), ('lily', 's', 50), ('jack', 'stephenson', 60)");
		daemon.executeUpdate(insert, connectionContext);

		Delete delete = parser.parse("delete from person where age < 42 and age > 28");
		int updates = daemon.executeUpdate(delete, connectionContext);
		assertEquals(2, updates);

		Select selectYoung = parser.parse("select * from person where age < 35");
		ResultSet rsYoung = daemon.executeQuery(selectYoung, connectionContext);
		assertTrue(rsYoung.next());
		assertEquals("b€€€€€b   ", rsYoung.getString("first_name"));
		assertEquals("smith", rsYoung.getString("last_name"));
		assertEquals(24, rsYoung.getInteger("age").intValue());
		assertFalse(rsYoung.next());

		Select selectOld = parser.parse("select * from person where age > 35 and age < 59");
		ResultSet rsOld = daemon.executeQuery(selectOld, connectionContext);
		assertTrue(rsOld.next());
		assertEquals("lily      ", rsOld.getString("first_name"));
		assertEquals("s", rsOld.getString("last_name"));
		assertEquals(50, rsOld.getInteger("age").intValue());
		assertFalse(rsOld.next());
	}

	@Test
	public void testInsertDeleteAndReinsert() throws Exception {
		// clear everything
		daemon.executeUpdate(parser.<Delete>parse("delete from person"), connectionContext);

		Insert insert = parser.parse(
				"insert into person values ('b\u20AC\u20AC\u20AC\u20AC\u20ACb', 'smith', 24), ('mike', 'rollins', 30), ('sarah', 's', 40), ('lily', 's', 50), ('jack', 'stephenson', 60)");
		daemon.executeUpdate(insert, connectionContext);

		Delete delete = parser.parse("delete from person where age < 52 and age > 28");
		int updates = daemon.executeUpdate(delete, connectionContext);
		assertEquals(3, updates);

		/*
		Select select = parser.parse("select * from person");
		ResultSet rs = daemon.executeQuery(select, connectionContext);
		while (rs.next()) {
			System.out.println("rs.getString(\"first_name\") = " + rs.getString("first_name"));
			System.out.println("rs.getString(\"last_name\") = " + rs.getString("last_name"));
			System.out.println("rs.getInteger(\"age\") = " + rs.getInteger("age"));
		}
		*/

		// if we're replacing data in space that's already used, we shouldn't use any more space than we already have.
		int currentFileLength = (int) new File("data/test_schema/Person/rows.bin").length();
		System.out.println("currentFileLength = " + currentFileLength);

		// we lost mike and sarah. Lets put sorta-mike back in, but with a new age
		Insert reinsertMike = parser.parse("insert into person values ('MIKE', 'ROLLINS', 120)");
		daemon.executeUpdate(reinsertMike, connectionContext);
		int currentFileLengthAfterReinsert = (int) new File("data/test_schema/Person/rows.bin").length();
		assertEquals(currentFileLength, currentFileLengthAfterReinsert);
	}

	@Test
	public void testLoadFromDisk() throws Exception {
		// clear everything
		daemon.executeUpdate(parser.<Delete>parse("delete from person"), connectionContext);

		Insert insert = parser.parse(
				"insert into person values ('b\u20AC\u20AC\u20AC\u20AC\u20ACb', 'smith', 24), ('mike', 'rollins', 30), ('sarah', 's', 40), ('lily', 's', 50), ('jack', 'stephenson', 60)");
		daemon.executeUpdate(insert, connectionContext);

		SqlDaemon newSqlDaemon = new SqlDaemon(DATA_DIRECTORY);
		newSqlDaemon.readSchemasAndTablesFromDisk();
		Select selectYoung = parser.parse("select * from person where age = 24");

		ResultSet rsYoung = newSqlDaemon.executeQuery(selectYoung, connectionContext);
		assertTrue(rsYoung.next());
		assertEquals("b€€€€€b   ", rsYoung.getString("first_name"));
		assertEquals("smith", rsYoung.getString("last_name"));
		assertEquals(24, rsYoung.getInteger("age").intValue());
		assertFalse(rsYoung.next());
	}
}