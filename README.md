# README #

I'm writing an SQL server in Java, to learn more about SQL and databases. If you want to use this as an actual database, please do so.

Build with maven: 
```
#!shell

mvn clean install
```


If you want to talk to me, email me at [markus@jevring.net](mailto:markus@jevring.net)

We now have CI as well: [![Build Status](https://drone.io/bitbucket.org/jevring/cusqld/status.png)](https://drone.io/bitbucket.org/jevring/cusqld/latest)